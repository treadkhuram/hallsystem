Vendors:
	id, name, phone, city, address, active
Raw material:
	id, name, unit, purchase price, qty, alert qty, active
Unit: 
	id, measure_unit, recipe_unt, active
Dishes:
	id, name, sale_price, qty, active
Booking:
	id, customer_name, booking_date, function_date, time(range), phone, function_name, booking_persons, confirm_persons, 
	hall, notes, status, total_amount, tax, discount, active
booking_transaction:
	id, referance, amount, detail, date, booking_id
booking_menu
	id, dish_id, booking_id

vendors_supply
	id, bill_book_no, detail, date, credit, debit=0, total_amount, vendor_id
vendor_supply_data
	id, product_id, qty, price, vendors_supply_id,
cash
	id, account_type(vendor, employee), amount, date, detail, voucher_no, bank_account_id, paid_person_id
bank
	id, name, account_no, detail, active
bank_account
	id, date, detail, debit, credit, bank_id


DB: update
ALTER TABLE booking ADD COLUMN serial_no text AFTER customer_name;
ALTER TABLE booking ADD COLUMN hall_charges int DEFAULT 0 AFTER confirm_persons;
ALTER TABLE booking ADD COLUMN hall_labour_cost int DEFAULT 0 AFTER hall_charges;
ALTER TABLE booking ADD COLUMN hall_tax int DEFAULT 0 AFTER hall_labour_cost;
ALTER TABLE booking ADD COLUMN function_cost DOUBLE DEFAULT 0 AFTER hall_tax;

CREATE TABLE settings(id INT PRIMARY KEY AUTO_INCREMENT, setting_name TEXT, detail TEXT, table_id INT)





