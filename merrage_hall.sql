-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 13, 2021 at 12:40 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 7.2.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `merrage_hall`
--

-- --------------------------------------------------------

--
-- Table structure for table `bank`
--

CREATE TABLE `bank` (
  `id` int(11) NOT NULL,
  `name` text,
  `detail` text,
  `active` int(11) DEFAULT NULL COMMENT '1=active, 0=delete'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bank`
--

INSERT INTO `bank` (`id`, `name`, `detail`, `active`) VALUES
(1, 'shop', 'account for transaction', 1),
(3, 'HBL', 'HBL account', 1);

-- --------------------------------------------------------

--
-- Table structure for table `bank_account`
--

CREATE TABLE `bank_account` (
  `id` int(11) NOT NULL,
  `date` date DEFAULT NULL,
  `detail` text,
  `debit` double DEFAULT NULL,
  `credit` double DEFAULT NULL,
  `bank_id` int(11) DEFAULT NULL,
  `cash_id` int(11) DEFAULT '-1',
  `booking_transaction_id` int(11) DEFAULT '-1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bank_account`
--

INSERT INTO `bank_account` (`id`, `date`, `detail`, `debit`, `credit`, `bank_id`, `cash_id`, `booking_transaction_id`) VALUES
(1, '2021-03-09', '(Pay) hello', 0, 12, 1, 7, -1),
(2, '2021-03-09', '(Receive) cash receive', 800, 0, 1, 8, -1),
(3, '2021-03-09', 'Booking Advance Received', 12000, 0, 1, -1, 9),
(6, '2021-03-09', 'Booking return COVID reason', 0, 500, 1, -1, 12),
(7, '2021-03-10', 'Cash in hand updated', 1000, 0, 1, -1, -1),
(8, '2021-03-11', 'Cash in hand updated', 1000, 0, 1, -1, -1),
(11, '2021-03-12', '(Pay) cash pay', 0, 4000, 1, 10, -1),
(14, '2021-03-12', 'Booking Advance Received', 3000, 0, 1, -1, 13),
(15, '2021-03-12', '0', 0, 0, 1, -1, 14),
(16, '2021-03-12', '0', 0, 0, 1, -1, 15),
(17, '2021-03-12', 'Booking Amount receive on event close', 0, 0, 1, -1, 16),
(18, '2021-03-12', 'Booking Advance Received', 12000, 0, 1, -1, 17),
(19, '2021-03-12', 'Booking Amount receive on event close', 0, 0, 1, -1, 18),
(20, '2021-03-13', 'Booking Advance Received', 14000, 0, 1, -1, 19);

-- --------------------------------------------------------

--
-- Table structure for table `booking`
--

CREATE TABLE `booking` (
  `id` int(11) NOT NULL,
  `customer_name` text NOT NULL,
  `booking_date` text NOT NULL,
  `function_date` text NOT NULL,
  `fun_st_time` text NOT NULL,
  `fun_end_time` text NOT NULL,
  `phone` text NOT NULL,
  `function_name` text NOT NULL,
  `booking_persons` int(11) NOT NULL,
  `confirm_persons` int(11) NOT NULL,
  `hall` text NOT NULL,
  `notes` text NOT NULL,
  `active` int(11) NOT NULL,
  `status` int(11) DEFAULT '0' COMMENT '0=pending, 1=complete, -1=cancel',
  `total_amount` double DEFAULT NULL,
  `tax` double DEFAULT '0',
  `discount` double DEFAULT '0',
  `menu_id` int(11) DEFAULT NULL,
  `per_person_rate` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `booking`
--

INSERT INTO `booking` (`id`, `customer_name`, `booking_date`, `function_date`, `fun_st_time`, `fun_end_time`, `phone`, `function_name`, `booking_persons`, `confirm_persons`, `hall`, `notes`, `active`, `status`, `total_amount`, `tax`, `discount`, `menu_id`, `per_person_rate`) VALUES
(2, 'khuram shahza', '2021-03-05', '2021-03-06', '12:00', '15:00', '2147483647', 'shadi', 120, 120, '1', 'hello', 1, 1, 144000, 0, 0, 1, 1100),
(3, 'Usama', '2021-03-06', '2021-03-13', '12:00', '15:00', '03214578789', 'shadi', 70, 70, '1', '', 1, 0, 144000, 0, 0, 1, 1200),
(4, 'khuram shahzad', '2021-03-06', '2021-03-12', '12:00', '15:00', '03123385888', 'shadi', 120, 120, '1', '', 1, 1, 144000, 5000, 2000, 1, 1200),
(5, 'Adnan Shafique', '2021-03-09', '2021-03-18', '12:00', '15:00', '55899932', 'shadi', 300, 300, '1', 'Canceled due to COVID reason', 1, -1, 144000, 0, 0, 1, 1200),
(6, 'ramzan', '2021-03-12', '2021-03-20', '12:00', '15:00', '980008798', 'shadi', 120, 120, '1', '', 1, 1, 144000, 0, 0, 1, 1200),
(7, 'ahsan', '2021-03-12', '2021-03-13', '12:00', '15:00', '55899932', 'Waleema', 100, 100, '1', '', 1, 1, 144000, 0, 0, 1, 1100),
(8, 'Ali Awan', '2021-03-13', '2023-03-02', '12:00', '15:00', '03066400081', 'shadi', 120, 120, '1', '', 1, 0, 144000, 0, 0, 1, 1200);

-- --------------------------------------------------------

--
-- Table structure for table `booking_extra`
--

CREATE TABLE `booking_extra` (
  `id` int(11) NOT NULL,
  `product_name` text,
  `qty` int(11) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `booking_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `booking_extra`
--

INSERT INTO `booking_extra` (`id`, `product_name`, `qty`, `price`, `booking_id`) VALUES
(1, 'camera', 1, 1000, 2),
(2, 'camera', 1, 1200, 4);

-- --------------------------------------------------------

--
-- Table structure for table `booking_menu`
--

CREATE TABLE `booking_menu` (
  `id` int(11) NOT NULL,
  `dish_id` int(11) NOT NULL,
  `booking_id` int(11) NOT NULL,
  `cost` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `booking_menu`
--

INSERT INTO `booking_menu` (`id`, `dish_id`, `booking_id`, `cost`) VALUES
(12, 1, 2, 3492),
(13, 2, 2, 1800),
(37, 1, 4, 3492),
(38, 2, 4, 1800),
(39, 3, 4, 3600),
(40, 1, 5, 8730),
(41, 2, 5, 4500),
(42, 3, 5, 9000),
(43, 1, 6, 3492),
(44, 2, 6, 1800),
(45, 3, 6, 3600),
(46, 5, 6, 10800),
(47, 1, 7, 3492),
(48, 2, 7, 1800),
(49, 3, 7, 3600),
(50, 5, 7, 10800),
(51, 1, 8, 3492),
(52, 2, 8, 1800),
(53, 3, 8, 3600);

-- --------------------------------------------------------

--
-- Table structure for table `booking_transactions`
--

CREATE TABLE `booking_transactions` (
  `id` int(11) NOT NULL,
  `referance` text COMMENT 'person name',
  `amount` double DEFAULT NULL,
  `detail` text,
  `date` date DEFAULT NULL,
  `booking_id` int(11) DEFAULT NULL,
  `is_advance` int(11) DEFAULT '1' COMMENT '0=advance, 1=not-advance'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `booking_transactions`
--

INSERT INTO `booking_transactions` (`id`, `referance`, `amount`, `detail`, `date`, `booking_id`, `is_advance`) VALUES
(1, 'khuram shahzad', 1200, 'Booking Advance', '2021-03-05', 2, 0),
(2, 'ali', 2000, 'receive from ali', '2021-03-05', 2, 1),
(3, 'Usama', 10000, 'Booking Advance', '2021-03-06', 3, 0),
(4, 'ali', 3000, 'amount receive from ali', '2021-03-06', 3, 1),
(5, 'khuram shahzad', 1000, 'Booking Advance', '2021-03-06', 4, 0),
(8, 'khuram shahzad', 147200, 'receive on event close', '2021-03-08', 4, 1),
(9, 'Adnan Shafique', 12000, 'Booking Advance', '2021-03-09', 5, 0),
(12, 'Adnan Shafique', -500, 'Booking Advance return', '2021-03-09', 5, 0),
(13, 'ramzan', 3000, 'Booking Advance', '2021-03-12', 6, 0),
(14, 'ramzan', 0, 'receive on event close', '2021-03-12', 6, 1),
(15, 'ramzan', 0, 'receive on event close', '2021-03-12', 6, 1),
(16, 'ramzan', 0, 'receive on event close', '2021-03-12', 6, 1),
(17, 'ahsan', 12000, 'Booking Advance', '2021-03-12', 7, 0),
(18, 'ahsan', 0, 'receive on event close', '2021-03-12', 7, 1),
(19, 'Ali Awan', 14000, 'Booking Advance', '2021-03-13', 8, 0);

-- --------------------------------------------------------

--
-- Table structure for table `cash`
--

CREATE TABLE `cash` (
  `id` int(11) NOT NULL,
  `cash_type` int(11) DEFAULT NULL COMMENT '0=pay, 1=receive',
  `account_type` text COMMENT 'vendor, employee, bank',
  `amount` double DEFAULT NULL,
  `date` date DEFAULT NULL,
  `detail` text,
  `voucher_no` text,
  `bank_account_id` int(11) DEFAULT NULL,
  `paid_person_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cash`
--

INSERT INTO `cash` (`id`, `cash_type`, `account_type`, `amount`, `date`, `detail`, `voucher_no`, `bank_account_id`, `paid_person_id`) VALUES
(5, 0, 'vendor', 3000, '2021-03-08', 'cash pay', '9878', 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `dishes`
--

CREATE TABLE `dishes` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `no_of_persons_serving` text NOT NULL,
  `active` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `dishes`
--

INSERT INTO `dishes` (`id`, `name`, `no_of_persons_serving`, `active`) VALUES
(1, 'Chicken Biryani', '100', 1),
(2, 'Naan', '100', 1),
(3, 'Water', '100', 1),
(4, 'cock', '100', 1),
(5, 'PEPSI', '100', 1);

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` int(11) NOT NULL,
  `designation` text,
  `name` text,
  `phone` text,
  `city` text,
  `address` text,
  `salary` int(11) DEFAULT NULL,
  `allow_holyday` int(11) DEFAULT NULL,
  `fine_per_day` int(11) DEFAULT NULL,
  `active` int(11) DEFAULT '1' COMMENT '1=active, 0=delete'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `designation`, `name`, `phone`, `city`, `address`, `salary`, `allow_holyday`, `fine_per_day`, `active`) VALUES
(24, 'staff', 'Naeem cheema', '06787887', 'Lahore', 'lahore', 20000, 3, 10, 1),
(25, '', '', '', '', '', 0, 0, 0, 1),
(26, '', '', '', '', '', 0, 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `employee_account`
--

CREATE TABLE `employee_account` (
  `id` int(11) NOT NULL,
  `date` date DEFAULT NULL,
  `detail` text,
  `total_amount` double DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `cash_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `name` text,
  `active` int(11) DEFAULT '1' COMMENT '1=active 0=delete'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `name`, `active`) VALUES
(1, 'Chicken Qorma', 1);

-- --------------------------------------------------------

--
-- Table structure for table `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(11) NOT NULL,
  `dish_id` int(11) DEFAULT NULL,
  `menu_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `menu_items`
--

INSERT INTO `menu_items` (`id`, `dish_id`, `menu_id`) VALUES
(1, 1, 1),
(2, 2, 1),
(3, 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `menu_rate`
--

CREATE TABLE `menu_rate` (
  `id` int(11) NOT NULL,
  `menu_id` int(11) DEFAULT NULL,
  `p100` int(11) DEFAULT '0',
  `p200` int(11) DEFAULT '0',
  `p300` int(11) DEFAULT '0',
  `p400` int(11) DEFAULT '0',
  `p500` int(11) DEFAULT '0',
  `p600` int(11) DEFAULT '0',
  `p700` int(11) DEFAULT '0',
  `extra_rate` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `menu_rate`
--

INSERT INTO `menu_rate` (`id`, `menu_id`, `p100`, `p200`, `p300`, `p400`, `p500`, `p600`, `p700`, `extra_rate`) VALUES
(1, 1, 1100, 1180, 1150, 1000, 990, 985, 980, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `raw_material`
--

CREATE TABLE `raw_material` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `unit` text NOT NULL,
  `purchase_price` text NOT NULL,
  `qty` text NOT NULL,
  `alert_qty` text NOT NULL,
  `active` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `raw_material`
--

INSERT INTO `raw_material` (`id`, `name`, `unit`, `purchase_price`, `qty`, `alert_qty`, `active`) VALUES
(1, 'Rice', '3', '90', '228', '10', 1),
(2, 'chciken', '3', '300', '200', '20', 1),
(3, 'Oil', '3', '200', '4.5', '2', 1),
(4, 'salt', '1', '20', '-0.5', '2', 1),
(5, 'Water', '2', '30', '-20', '10', 1),
(6, 'Naan', '4', '150', '-10', '10', 1),
(7, 'Cock', '2', '90', '780', '30', 1);

-- --------------------------------------------------------

--
-- Table structure for table `recipe`
--

CREATE TABLE `recipe` (
  `id` int(11) NOT NULL,
  `qty` double DEFAULT NULL,
  `raw_material_id` int(11) DEFAULT NULL,
  `dish_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `recipe`
--

INSERT INTO `recipe` (`id`, `qty`, `raw_material_id`, `dish_id`) VALUES
(1, 10, 1, 1),
(2, 5, 2, 1),
(3, 2.5, 3, 1),
(4, 500, 4, 1),
(5, 120, 6, 2),
(6, 100, 5, 3),
(7, 100, 7, 4),
(8, 100, 7, 5);

-- --------------------------------------------------------

--
-- Table structure for table `unit`
--

CREATE TABLE `unit` (
  `id` int(11) NOT NULL,
  `measure_unit` text NOT NULL,
  `receipe_unit` text NOT NULL,
  `converstion_factor` int(11) NOT NULL,
  `active` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `unit`
--

INSERT INTO `unit` (`id`, `measure_unit`, `receipe_unit`, `converstion_factor`, `active`) VALUES
(1, 'KG', 'Gram', 1000, 1),
(2, 'pcs', 'pcs', 1, 1),
(3, 'Full KG', 'Full KG', 1, 1),
(4, 'Dozen', 'pcs', 12, 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` text,
  `email` text,
  `password` text,
  `active` int(11) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `active`) VALUES
(1, 'admin', 'admin@gmail.com', 'admin123', 1);

-- --------------------------------------------------------

--
-- Table structure for table `vandors`
--

CREATE TABLE `vandors` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `phone` text NOT NULL,
  `city` text NOT NULL,
  `address` text NOT NULL,
  `active` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `vandors`
--

INSERT INTO `vandors` (`id`, `name`, `phone`, `city`, `address`, `active`) VALUES
(1, 'hamza', '0554550006', 'Rujranwala', 'grw', 1),
(2, 'Ali Raza', '089733244', 'Lahrore', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `vendors_supply`
--

CREATE TABLE `vendors_supply` (
  `id` int(11) NOT NULL,
  `bill_book_no` text NOT NULL,
  `detail` text NOT NULL,
  `date` text NOT NULL,
  `credit` int(11) NOT NULL,
  `debit` int(11) NOT NULL,
  `total_amount` bigint(20) NOT NULL,
  `vendor_id` int(11) NOT NULL,
  `amount_type` int(11) DEFAULT '0' COMMENT '0=supply, 1=cash',
  `cash_id` int(11) DEFAULT '0' COMMENT '0=supply, other=cash'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `vendors_supply`
--

INSERT INTO `vendors_supply` (`id`, `bill_book_no`, `detail`, `date`, `credit`, `debit`, `total_amount`, `vendor_id`, `amount_type`, `cash_id`) VALUES
(1, '1218', '', '2021-03-08', 150000, 0, 150000, 1, 0, 0),
(3, '', 'late receive', '2021-03-08', 21000, 0, 21000, 1, 0, 0),
(4, '', '', '2021-03-08', 9000, 0, 9000, 1, 0, 0),
(8, '9878', 'cash pay', '2021-03-08', 0, 2000, 3000, 2, 1, 5),
(11, '', 'cash pay', '2021-03-12', 0, 4000, 4000, 1, 1, 10),
(12, '7868', 'salt receive', '2021-03-12', 300, 0, 300, 1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `vendor_supply_data`
--

CREATE TABLE `vendor_supply_data` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `vendors_supply_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `vendor_supply_data`
--

INSERT INTO `vendor_supply_data` (`id`, `product_id`, `qty`, `price`, `vendors_supply_id`) VALUES
(1, 2, 500, 300, 1),
(3, 2, 70, 300, 3),
(4, 1, 100, 90, 4),
(5, 4, 15, 20, 12);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bank`
--
ALTER TABLE `bank`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bank_account`
--
ALTER TABLE `bank_account`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `booking`
--
ALTER TABLE `booking`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `booking_extra`
--
ALTER TABLE `booking_extra`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `booking_menu`
--
ALTER TABLE `booking_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `booking_transactions`
--
ALTER TABLE `booking_transactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cash`
--
ALTER TABLE `cash`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dishes`
--
ALTER TABLE `dishes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employee_account`
--
ALTER TABLE `employee_account`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu_rate`
--
ALTER TABLE `menu_rate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `raw_material`
--
ALTER TABLE `raw_material`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `recipe`
--
ALTER TABLE `recipe`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `unit`
--
ALTER TABLE `unit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vandors`
--
ALTER TABLE `vandors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendors_supply`
--
ALTER TABLE `vendors_supply`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendor_supply_data`
--
ALTER TABLE `vendor_supply_data`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bank`
--
ALTER TABLE `bank`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `bank_account`
--
ALTER TABLE `bank_account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `booking`
--
ALTER TABLE `booking`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `booking_extra`
--
ALTER TABLE `booking_extra`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `booking_menu`
--
ALTER TABLE `booking_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;
--
-- AUTO_INCREMENT for table `booking_transactions`
--
ALTER TABLE `booking_transactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `cash`
--
ALTER TABLE `cash`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `dishes`
--
ALTER TABLE `dishes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `employee_account`
--
ALTER TABLE `employee_account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `menu_rate`
--
ALTER TABLE `menu_rate`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `raw_material`
--
ALTER TABLE `raw_material`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `recipe`
--
ALTER TABLE `recipe`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `unit`
--
ALTER TABLE `unit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `vandors`
--
ALTER TABLE `vandors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `vendors_supply`
--
ALTER TABLE `vendors_supply`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `vendor_supply_data`
--
ALTER TABLE `vendor_supply_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
