<?php
if ( !defined( 'BASEPATH' ) )exit( 'No direct script access allowed' );

function parse_date($date){
    return date_format(date_create($date),"d F, Y");
}

function get_user_name($table, $userId){
    $ci=& get_instance();
    $ci->load->database();
    $ci->db->select('*');
    $ci->db->from($table);
    $ci->db->where('id', $userId);
    return $ci->db->get()->row()->name;
}

function get_default_bank_id(){
    return 1;
}

function get_booking_Account(){
    $ci=& get_instance();
    $ci->load->database();
    $ci->db->select('*');
    $ci->db->from('settings');
    $ci->db->where('setting_name', 'booking_transaction_account');
    return $ci->db->get()->row();
}

function add_amount_in_booking_account($amount, $transactionId, $bookingId){
    $ci=& get_instance();
    $ci->load->database();
    $ci->db->select('*');
    $ci->db->from('booking');
    $ci->db->where('id', $bookingId);
    $booking = $ci->db->get()->row();

    $detail = 'Amount receive of '.$booking->customer_name.' booking# ('.$booking->serial_no.')';

    $bankData = array('date' => date('Y-m-d'), 'detail' => $detail,
        'debit' => 0, 'credit' => $amount, 'bank_id' => get_booking_Account()->table_id,
        'booking_transaction_id' => $transactionId);
    $ci->db->insert('bank_account', $bankData);
}

function get_menu_rate($mr, $totalPersons){
    if(!isset($mr))
        return 0;
    if ($totalPersons <= 199) {
        $menuRate = $mr->p100;
    } else if ($totalPersons <= 299) {
        $menuRate = $mr->p200;
    } else if ($totalPersons <= 399) {
        $menuRate = $mr->p300;
    } else if ($totalPersons <= 499) {
        $menuRate = $mr->p400;
    } else if ($totalPersons <= 599) {
        $menuRate = $mr->p500;
    } else if ($totalPersons <= 699) {
        $menuRate = $mr->p600;
    } else if ($totalPersons <= 799 || $totalPersons > 799) {
        $menuRate = $mr->p700;
    }
    return $menuRate;
}

function get_supply_detail($supplyId){
    $ci=& get_instance();
    $ci->load->database();
    $ci->db->select('vs.qty as supply_qty, vs.price as supply_price, rm.name as rm_name, rm.unit');
    $ci->db->from('vendor_supply_data vs');
    $ci->db->join('raw_material rm', 'vs.product_id = rm.id', 'left');
    $ci->db->where('vs.vendors_supply_id', $supplyId);
    $data = $ci->db->get()->result();
    $detail = '';
    foreach ($data as $d){
        $detail .= $d->rm_name.' ('.$d->supply_qty.') '. get_measure_unit($d->unit).'<br>' ;
    }
    return $detail;
}

function get_measure_unit($unitId){
    $ci=& get_instance();
    $ci->load->database();
    $ci->db->select('*');
    $ci->db->from('unit');
    $ci->db->where('id', $unitId);
    $data =  $ci->db->get()->row();
    if(empty($data))
        return '';
    else
        return $data->measure_unit;
}

?>