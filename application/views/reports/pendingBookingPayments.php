<style>
    @media print {
        @page {
            size: auto;
        }

        .report_headings {
            display: block !important;
        }

        .print_btn {
            display: none;
        }

        .btn {
            display: none;
        }

        .addExpense_print {
            display: none !important;
            background: white;
            color: white;
        }

        .actions_print {
            display: none !important;
            background: #fff;
        }

    }

    table, th, td {
        border-collapse: collapse;
        border: 1px solid black;
        padding: 5px;
        text-align: left;
    }
</style>

<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-wrapper-before"></div>

        <div class="content-body">
            <!-- Basic form layout section start -->
            <section id="basic-form-layouts">
                <div class="row match-height">

                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <?php if ($this->session->flashdata('msg')): ?>
                                        <?php echo $this->session->flashdata('msg'); ?>
                                    <?php endif; ?>
                                    <form method="post" class="form"
                                          action="<?php echo base_url(); ?>Report/pendingBookingPayments"
                                          enctype="multipart/form-data" accept-charset="ISO-8859-1">
                                        <div class="form-body">
                                            <h4 class="form-section">
                                                <i class="ft-flag"></i>Pending Booking Payments Report
                                            </h4>
                                            <div class="row">

                                                <div class="col-md-5 form-group">
                                                    <label for="">Start Date.</label>
                                                    <input type="date" class="form-control" required
                                                           placeholder="Start Date"
                                                           value="<?= isset($start_date) ? $start_date : date('Y-m-01'); ?>"
                                                           name="start_date">
                                                </div>

                                                <div class="col-md-5 form-group">
                                                    <label for="">End Date.</label>
                                                    <input type="date" class="form-control" required
                                                           placeholder="End Date"
                                                           value="<?= isset($end_date) ? $end_date : date('Y-m-d', time()); ?>"
                                                           name="end_date">
                                                </div>


                                                <div class="col-md-2 mt-1">
                                                    <button type="submit" name="filter" class="btn btn-primary m-1">
                                                        <i class="la la-check-square-o"></i> Filter
                                                    </button>
                                                </div>
                                            </div>


                                        </div>

                                    </form>

                                </div>


                                <?php if (!empty($booking)) { ?>
                                    <div class="row">
                                        <div class="col-12">

                                            <div class="heading-elements">
                                                <a class="btn btn-primary btn-sm pull-right text-white mr-2"
                                                   onclick="printDailyReport()">Print</a>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="card" id="card">
                                                <div class="card-header">
                                                    <div class="row">
                                                        <!-- <h4 class="col-md-1"></h4> -->
                                                        <div class="col-md-6">
                                                            <label>Start Date:
                                                                <span><?= parse_date($start_date) ?></span></label><br>
                                                            <label>End Date:
                                                                <span><?= parse_date($end_date) ?></span></label>
                                                        </div>
                                                        <!-- <div class="col-md-6 text-right">
                                                    <label>Total Amount: <span><?= $total_amount ?></span> </label><br>
                                                    <label>Recieve Amount: <span><?= $receive_amount ?></span></label><br>
                                                    <label>Balance: <span><?= $balance ?></span></label>
                                                </div> -->
                                                    </div>


                                                    <a class="heading-elements-toggle"><i
                                                                class="la la-ellipsis-v font-medium-3"></i></a>
                                                </div>
                                                <div class="card-content collapse show">
                                                    <div class="table-responsive">
                                                        <table style="width: 100%" id="tbl" class="ml-1">
                                                            <thead class="black">
                                                            <tr>
                                                                <th style="width: 5%">#</th>
                                                                <th style="width: 10%">Date</th>
                                                                <th style="width: 20%">Customer</th>
                                                                <th style="width: 15%">Phone</th>
                                                                <th style="width: 5%">Function</th>
                                                                <th style="width: 15%">Total Amount</th>
                                                                <th style="width: 15%">Receive Amount</th>
                                                                <th style="width: 15%">Remaining</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody id="tbody">
                                                            <?php
                                                            $totalAmount=0; $receiveAmount=0; $remainingAmount=0;
                                                            foreach ($booking as $k => $data) {
                                                                $totalAmount += $data->total_amount;
                                                                $receiveAmount += $data->receive_amunt;
                                                                $remainingAmount += $data->total_amount - $data->receive_amunt;
                                                                ?>
                                                                <tr>
                                                                    <td><?= ($k + 1) ?></td>
                                                                    <td><?= parse_date($data->function_date) ?></td>
                                                                    <td><?= $data->customer_name ?></td>
                                                                    <td><?= $data->phone ?></td>
                                                                    <td><?= $data->function_name ?></td>
                                                                    <td style="text-align: right"><?= $data->total_amount ?></td>
                                                                    <td style="text-align: right"><?= $data->receive_amunt ?></td>
                                                                    <td style="text-align: right"><?= $data->total_amount - $data->receive_amunt ?></td>
                                                                </tr>
                                                            <?php } ?>
                                                            <tr>
                                                                <td style="text-align: center" colspan="5"><b>Total:</b></td>
                                                                <td style="text-align: right"><?= $totalAmount ?></td>
                                                                <td style="text-align: right"><?= $receiveAmount ?></td>
                                                                <td style="text-align: right"><?= $remainingAmount ?></td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>


                            </div>
                        </div>
                    </div>
                </div>
            </section>


        </div>
    </div>
</div>


<script>
    function printDailyReport() {
        $("table").removeClass("table table-striped table-bordered ");
        $('#card').printThis({
            debug: false,               // show the iframe for debugging
            importCSS: true,            // import parent page css
            importStyle: true,         // import style tags
            printContainer: true,       // print outer container/$.selector
            loadCSS: "",                // path to additional css file - use an array [] for multiple
            pageTitle: "",              // add title to print page
            removeInline: false,        // remove inline styles from print elements
            removeInlineSelector: "*",  // custom selectors to filter inline styles. removeInline must be true
            printDelay: 333,            // variable print delay
            header: null,               // prefix to html
            footer: null,               // postfix to html
            base: false,                // preserve the BASE tag or accept a string for the URL
            formValues: true,           // preserve input/form values
            canvas: false,              // copy canvas content
            doctypeString: '',       // enter a different doctype for older markup
            removeScripts: false,       // remove script tags from print content
            copyTagClasses: false,      // copy classes from the html & body tag
            beforePrintEvent: null,     // function for printEvent in iframe
            beforePrint: null,          // function called before iframe is filled
            afterPrint: null            // function called before iframe is removed
        });
    }
</script>


<!--
<script type="text/javascript">

    function setVal(Url) {
        var prod_select = $('#prod_select').val();
        $.ajax({
            url: Url+'/'+prod_select,
            data: {id:prod_select},
            type: "POST",
           success: function(res){
            // console.log(res);

            var newA = JSON.parse(res);
            $('#price').val(newA['price']);
            $('#qty').val(newA['qty']);
            $('#product').val(newA['name']);
           }
       }); 
    //    console.log(prod_select);
    }

</script> -->