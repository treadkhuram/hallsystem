

<style>
    @media print{
        @page{
            size: auto;
        }
        .report_headings{
            display: block !important;
        }
        .print_btn{
            display: none;
        }
        .btn{
            display: none;
        }
        .addExpense_print{
            display: none !important;
            background: white;
            color: white;
        }
        .actions_print{
            display: none !important;
            background: #fff;
        }

    }
    table, th, td {
        border-collapse: collapse;
        border: 1px solid black;
        padding: 5px;
        text-align: left;
    }
</style>

<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-wrapper-before"></div>

        <div class="content-body">
            <!-- Basic form layout section start -->
            <section id="basic-form-layouts">
                <div class="row match-height">

                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <?php if($this->session->flashdata('msg')):?>
                                    <?php echo $this->session->flashdata('msg');?>
                                    <?php endif;?>
                                    <form method="post" class="form" action="<?php echo base_url();?>Report/allEmployeeSalerySheet"
                                        enctype="multipart/form-data" accept-charset="ISO-8859-1">
                                        <div class="form-body">
                                            <h4 class="form-section">
                                                <i class="ft-flag"></i>Emplyee Salery Sheet
                                            </h4>
                                            <div class="row">
                                                

                                            <!-- <div class="col-md-3 form-group">
                                                    <label for="">Select Booking Type</label>
                                                    <select name="booking_status" id="booking_type" class="select2 select2-size-sm form-control" required>
                                                        <option value="">Select Type</option>
                                                        <option <?= $booking_type == 0 ? 'selected': '' ?> value="0" >Pending</option>
                                                        <option <?= $booking_type == 1 ? 'selected': '' ?> value="1" >Completed</option>
                                                        <option <?= $booking_type == -1 ? 'selected': '' ?> value="-1" >Canceled</option>
                                                    </select>
                                                </div> -->

                                                <div class="col-md-5 form-group">
                                                    <label for="">Start Date.</label>
                                                    <input type="date"  class="form-control" required placeholder="Start Date"
                                                           value="<?= isset($start_date) ? $start_date : date('Y-m-01'); ?>"
                                                        name="start_date">
                                                </div>

                                                <div class="col-md-5 form-group">
                                                    <label for="">End Date.</label>
                                                    <input type="date"  class="form-control" required placeholder="End Date"
                                                           value="<?= isset($end_date) ? $end_date : date('Y-m-d', time()); ?>"
                                                        name="end_date">
                                                </div>

                                                <!-- <div class="col-md-1"></div> -->
                                                <div class="col-md-2"> 
                                                    <button type="submit" name="filter" class="btn btn-primary m-1">
                                                    <i class="la la-check-square-o"></i> Filter </button>
                                                </div>
                                            </div>


                                            </div>
                                       
                                    </form>

                                    </div>


                            <?php if(!empty($transactions)){ ?>
                                <div class="row">
                                    <div class="col-12">
                                        
                                    <div class="heading-elements">
                                                <a class="btn btn-primary box-shadow-2 round btn-min-width pull-right text-white mr-2" onclick="printDailyReport()">Print</a>
                                            </div>
                                            <div class="clearfix"></div>
                                        <div class="card" id="card">
                                        <div class="card-header">
                                            <div class="row">
                                                <!-- <h4 class="col-md-1"></h4> -->
                                                <div class="col-md-6">
                                                    <label>Start Date:  <span><?= parse_date($start_date) ?></span></label><br>
                                                    <label>End Date: <span><?= parse_date($end_date) ?></span></label>
                                                </div>
                                                <!-- <div class="col-md-6 text-right">
                                                    <label>Total Amount: <span><?= $total_amount ?></span> </label><br>
                                                    <label>Recieve Amount: <span><?= $receive_amount ?></span></label><br>
                                                    <label>Balance: <span><?= $balance ?></span></label>
                                                </div> -->
                                            </div>


                                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                        </div>
                                        <div class="card-content collapse show">
                                            <div class="table-responsive">
                                            <table id="tbl" class="ml-1">
                                                <thead class="black">
                                                <tr>
                                                    <th style="width: 10%">#</th>
                                                    <th style="width: 23%">Name</th>
                                                    <th style="width: 10%">Phone</th>
                                                    <th style="width: 13%">Salary</th>
                                                    <th style="width: 12%">Paid Amount</th>
                                                    <th style="width: 12%">Remaining Amount</th>
                                                </tr>
                                                </thead>
                                                <tbody id="tbody">
                                                <?php
                                                $totalSalary=0; $totalPaidAmount=0; $totalRemainingAmount=0;
                                                foreach($transactions as $k=>$data){
                                                    $totalSalary += $data->salary;
                                                    $totalPaidAmount += $data->paid_amount;
                                                    $totalRemainingAmount +=$data->salary - $data->paid_amount;
                                                    ?>
                                                <tr>
                                                    <td><?= ($k+1) ?></td>
                                                    <td><?= $data->name ?></td>
                                                    <td><?= $data->phone?></td>
                                                    <td class="text-right"><?= $data->salary?></td>
                                                    <td class="text-right"><?= $data->paid_amount ?></td>
                                                    <td class="text-right"><?= $data->salary - $data->paid_amount ?></td>
                                                </tr>
                                                    <?php }?>
                                                <tr>
                                                    <td style="text-align: center" colspan="3"><b>Total:</b></td>
                                                    <td style="text-align: right"><?= $totalSalary ?></td>
                                                    <td style="text-align: right"><?= $totalPaidAmount ?></td>
                                                    <td style="text-align: right"><?= $totalRemainingAmount ?></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                            <?php }?>


                            </div>
                        </div>
                    </div>
                </div>
            </section>

        

        </div>
    </div>
</div>




<script>
    function printDailyReport()
    {
        $("table").removeClass("table table-striped table-bordered ");
        $('#card').printThis({
            debug: false,               // show the iframe for debugging
            importCSS: true,            // import parent page css
            importStyle: true,         // import style tags
            printContainer: true,       // print outer container/$.selector
            loadCSS: "",                // path to additional css file - use an array [] for multiple
            pageTitle: "All Employee SalarySheet",              // add title to print page
            removeInline: false,        // remove inline styles from print elements
            removeInlineSelector: "*",  // custom selectors to filter inline styles. removeInline must be true
            printDelay: 333,            // variable print delay
            header: null,               // prefix to html
            footer: null,               // postfix to html
            base: false,                // preserve the BASE tag or accept a string for the URL
            formValues: true,           // preserve input/form values
            canvas: false,              // copy canvas content
            doctypeString: '',       // enter a different doctype for older markup
            removeScripts: false,       // remove script tags from print content
            copyTagClasses: false,      // copy classes from the html & body tag
            beforePrintEvent: null,     // function for printEvent in iframe
            beforePrint: null,          // function called before iframe is filled
            afterPrint: null            // function called before iframe is removed
        });
    }
</script>




<!-- 
<script type="text/javascript">

    function setVal(Url) {
        var prod_select = $('#prod_select').val();
        $.ajax({
            url: Url+'/'+prod_select,
            data: {id:prod_select},
            type: "POST",
           success: function(res){
            // console.log(res);

            var newA = JSON.parse(res);
            $('#price').val(newA['price']);
            $('#qty').val(newA['qty']);
            $('#product').val(newA['name']);
           }
       }); 
    //    console.log(prod_select);
    }

</script> -->