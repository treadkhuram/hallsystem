<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="author" content="ThemeSelect">
    <title>Hall System</title>
    <link rel="apple-touch-icon" href="<?php echo base_url(); ?>assets/logo.png">
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url(); ?>assets/logo.png">
    <link href="https://fonts.googleapis.com/css?family=Muli:300,300i,400,400i,600,600i,700,700i%7CComfortaa:300,400,700"
          rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css"
          href="<?php echo base_url(); ?>assets/app-assets/vendors/css/vendors.min.css">
    <link rel="stylesheet" type="text/css"
          href="<?php echo base_url(); ?>assets/app-assets/vendors/css/charts/chartist.css">
    <link rel="stylesheet" type="text/css"
          href="<?php echo base_url(); ?>assets/app-assets/vendors/css/charts/chartist-plugin-tooltip.css">
    <link rel="stylesheet" type="text/css"
          href="<?php echo base_url(); ?>assets/app-assets/vendors/css/tables/datatable/datatables.min.css">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/app-assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/app-assets/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/app-assets/css/colors.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/app-assets/css/components.css">
    <!-- END: Theme CSS-->

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css"
          href="<?php echo base_url(); ?>assets/app-assets/css/core/menu/menu-types/vertical-menu.css">
    <link rel="stylesheet" type="text/css"
          href="<?php echo base_url(); ?>assets/app-assets/css/core/colors/palette-gradient.css">
    <link rel="stylesheet" type="text/css"
          href="<?php echo base_url(); ?>assets/app-assets/css/core/colors/palette-gradient.css">
    <link rel="stylesheet" type="text/css"
          href="<?php echo base_url(); ?>assets/app-assets/css/pages/chat-application.css">
    <link rel="stylesheet" type="text/css"
          href="<?php echo base_url(); ?>assets/app-assets/css/pages/dashboard-analytics.css">
    <!-- END: Page CSS-->

    <link rel="stylesheet" type="text/css"
          href="<?php echo base_url(); ?>assets/app-assets/vendors/css/forms/selects/select2.min.css">

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/style.css">
    <!-- END: Custom CSS-->
    <style>
        .navbar-header .navbar-brand .brand-logo {
            width: 149px;
        }
    </style>
    <style>
        div.parent {
        }

        div.child {
            width: 40%;
        }

        div.mid {
            width: 20%;
            text-align: center;
        }

        h1.header {
            padding-top: 65px;
            padding-left: 25px;
            padding-right: 25px;
            padding-bottom: px;

        }

        #spacing {
            padding-top: 60px;
            padding-left: 25px;
            padding-right: 25px;
            padding-bottom: 60px;

        }

        div.form {
            width: 50%;

        }

        #underline {
            text-decoration: underline;
            height: 10px;
        }

        div {
            padding-top: 10px;
            padding-right: 10px;
            padding-bottom: 5px;
            padding-left: 10px;
        }
    </style>
</head>
<body>
<div class="container-fluids">
    <div class="row">
        <div class="col-md-3 text-left">
            <label for=""><b> Bory Wala Cheema</b></label><br>
            <label for="">GT ROAD Wazirabad</label>
        </div>
        <div class="col-md-6 text-center">
            <h1><b> Ashraf اشرف </b></h1>
            <h2>Mahal</h2>
            <h4>Event Venue</h4>
        </div>
        <div class="col-md-3 text-right">
            <label for=""><b> Contact No: </b></label> <br>
            <label for="">03106587888</label> <br>
            <label for="">03036587888</label> <br>
            <label for="">055-6587888 / 6588200</label>
        </div>
    </div>


    <div class="row">
        <div class="col-md-6"><b> Booking Date: </b><span>
        <?= $booking->booking_date ?>
      </span></div>
        <div class="col-md-6 text-right"><b>S.No: </b><span>
        <?= $booking->serial_no ?>
      </span></div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <label for=""><b> Customer:</b> <span>
          <?= $booking->customer_name ?>
        </span></label>
        </div>
        <div class="col-md-6 text-right">
            <label for=""><b> Function Date: </b><span>
          <?= $booking->function_date ?>
        </span></label>
        </div>
        <div class="col-md-6">
            <label for=""><b>Phone No:</b> <span>
          <?= $booking->phone ?>
        </span></label>
        </div>
        <div class="col-md-6 text-right">
            <label for=""> <b>Time:</b> <span>
          <?= $booking->fun_st_time . '-' . $booking->fun_end_time ?>
        </span></label>
        </div>
        <div class="col-md-6">
            <label for=""><b> Function Name:</b> <span>
          <?= $booking->function_name ?>
        </span></label>
        </div>
        <div class="col-md-6 text-right">
            <label for=""><b> Start Time:</b> <span>
          <?= $booking->fun_st_time ?>
        </span></label>
        </div>
        <div class="col-md-6">
            <label for=""><b>Confirm Persons: </b> <span>
          <?= $booking->confirm_persons ?>
        </span></label>
        </div>
        <div class="col-md-6 text-right">
            <label for=""><b> Booking Persons: </b><span>
          <?= $booking->booking_persons ?>
        </span></label>
        </div>
        <div class="col-md-6">
            <label for=""><b> Hall: </b><span>
          <?= $booking->hall ?>
        </span></label>
        </div>
        <div class="col-md-6 text-right">
            <label for=""><b> Total Amount: </b><span>
          <?= $booking->total_amount + $booking->tax ?>
        </span></label>
        </div>
        <div class="offset-6 col-md-6 text-right">
            <label for=""><b> Booking Advance: </b><span>
          <?php $amount = 0;
          foreach ($booking_trans as $bt) {
              $amount += $bt->amount;
          }
          echo $amount;
          ?>
        </span></label>
        </div>
        <div class="offset-6 col-md-6 text-right">
            <label for=""><b> Total Amount: </b><span>
          <?= $booking->total_amount + $booking->tax - $amount ?>
        </span></label>
        </div>
        <div class="col-md-12">
            <label for=""><b> Menu: </b><span>
          <?php foreach ($booking_menu as $bm) {
              echo $bm->dish_name . ', ';

          } ?>

        </span></label>
        </div>
        <div class="col-md-12">
            <label for=""><b> Note: </b><span>
          <?= $booking->notes ?>
        </span></label>
        </div>

    </div>

    <div class="row text-center">
        <div class="col-md-6">
            <label for=""><b> Signature Manager:</b></label> <br> <span>_______________________</span>
        </div>

        <div class="col-md-6">
            <label for=""><b>Signature Customer: </b> </label> <br> <span>_______________________</span>
        </div>
    </div>

</div>
<script>
    window.print();
</script>
</body>
</html>