
<style>
    @media print{
        @page{
            size: auto;
        }
        .report_headings{
            display: block !important;
        }
        .print_btn{
            display: none;
        }
        .btn{
            display: none;
        }
        .addExpense_print{
            display: none !important;
            background: white;
            color: white;
        }
        .actions_print{
            display: none !important;
            background: #fff;
        }

    }
    table, th, td {
        border-collapse: collapse;
        border: 1px solid black;
        padding: 5px;
        text-align: left;
    }
</style>


<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-wrapper-before"></div>

        <div class="content-body">
            <!-- Basic form layout section start -->
            <section id="basic-form-layouts">
                <div class="row match-height">

                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="card">
                                                <div class="card-header">
                                                    <h4 class="form-section">
                                                        All Vendors Balance
                                                    </h4>
                                                    <div class="heading-elements">
                                                        <a class="btn btn-primary box-shadow-2 round btn-min-width pull-right text-white" onclick="printDailyReport()">Print</a>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <a class="heading-elements-toggle"><i
                                                                class="la la-ellipsis-v font-medium-3"></i></a>
                                                </div>
                                                <div class="card-content collapse show" id="card">
                                                    <div class="col-md-12 text-right">
                                                        <label>Total Balance: <?= $total_balance ?></label><br>
                                                    </div>
                                                    <div class="table-responsive">
                                                    <table id="tbl" width="100%" class="">
                                                        <thead class="black">
                                                            <tr>
                                                                <th style="width: 10%">#</th>
                                                                <th style="width: 25%">Vendor Name</th>
                                                                <th style="width: 22%">Phone</th>
                                                                <th style="width: 21%">City</th>
                                                                <th style="width: 22%">Balance</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <?php foreach ($vendors as $k => $ven) { ?>
                                                                <tr>
                                                                    <td><?= ($k + 1) ?></td>
                                                                    <td><?= $ven->name ?></td>
                                                                    <td><?= $ven->phone ?></td>
                                                                    <td><?= $ven->city ?></td>
                                                                    <td><?= $ven->balance ?></td>
                                                                </tr>
                                                            <?php } ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>






<script>
    function printDailyReport()
    {
        $("table").removeClass("table table-striped table-bordered ");
        $('#card').printThis({
            debug: false,               // show the iframe for debugging
            importCSS: true,            // import parent page css
            importStyle: true,         // import style tags
            printContainer: true,       // print outer container/$.selector
            loadCSS: "",                // path to additional css file - use an array [] for multiple
            pageTitle: "All Vendors Balance",              // add title to print page
            removeInline: false,        // remove inline styles from print elements
            removeInlineSelector: "*",  // custom selectors to filter inline styles. removeInline must be true
            printDelay: 333,            // variable print delay
            header: null,               // prefix to html
            footer: null,               // postfix to html
            base: false,                // preserve the BASE tag or accept a string for the URL
            formValues: true,           // preserve input/form values
            canvas: false,              // copy canvas content
            doctypeString: '',       // enter a different doctype for older markup
            removeScripts: false,       // remove script tags from print content
            copyTagClasses: false,      // copy classes from the html & body tag
            beforePrintEvent: null,     // function for printEvent in iframe
            beforePrint: null,          // function called before iframe is filled
            afterPrint: null            // function called before iframe is removed
        });
    }
</script>

