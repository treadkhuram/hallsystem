
<style>
    @media print{
        @page{
            size: auto;
        }
        .report_headings{
            display: block !important;
        }
        .print_btn{
            display: none;
        }
        .btn{
            display: none;
        }
        .addExpense_print{
            display: none !important;
            background: white;
            color: white;
        }
        .actions_print{
            display: none !important;
            background: #fff;
        }

    }
    table, th, td {
        border-collapse: collapse;
        border: 1px solid black;
        padding: 5px;
        text-align: left;
    }
</style>


<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-wrapper-before"></div>

        <div class="content-body">
            <!-- Basic form layout section start -->
            <section id="basic-form-layouts">
                <div class="row match-height">

                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <?php if($this->session->flashdata('msg')):?>
                                    <?php echo $this->session->flashdata('msg');?>
                                    <?php endif;?>
                                    <form method="post" class="form" action="<?php echo base_url();?>Report/accountBalancereport"
                                        enctype="multipart/form-data" accept-charset="ISO-8859-1">
                                        <div class="form-body">
                                            <h4 class="form-section">
                                                <i class="ft-flag"></i>Account Balance Report
                                            </h4>
                                            
                                            <div class="row">
                                            <div class="col-md-3 form-group">
                                                    <label for="">Account Name</label>
                                                    <select name="bank_id" id="" class="select2 select2-size-sm form-control" required>
                                                        <option value="">Select Type</option>
                                                        <?php foreach($allAccounts as $al){?>
                                                        <option <?= $bankId == $al->id ? 'selected': '' ?> value="<?= $al->id?>"><?= $al->name?></option>
                                                        <?php }?>
                                                    </select>
                                                </div>

                                                <div class="col-md-3 form-group">
                                                    <label for="">Start Date.</label>
                                                    <input type="date"  class="form-control" required placeholder="Start Date"
                                                           value="<?= isset($start_date) ? $start_date : date('Y-m-01'); ?>"
                                                        name="start_date">
                                                </div>

                                                <div class="col-md-3 form-group">
                                                    <label for="">End Date.</label>
                                                    <input type="date"  class="form-control" required placeholder="End Date"
                                                           value="<?= isset($end_date) ? $end_date : date('Y-m-d', time()); ?>"
                                                        name="end_date">
                                                </div>

                                                <div class="col-md-1"></div>
                                                <div class="col-md-2 mt-1"> 
                                                    <button type="submit" name="filter" class="btn btn-primary m-1">
                                                    <i class="la la-check-square-o"></i> Filter </button>
                                                </div>
                                            </div>


                                            </div>
                                       
                                    </form>

                                    </div>


                            <?php if(!empty($transactions)){ ?>
                                <div class="row">
                                    <div class="col-12">
                                        
                                    <div class="heading-elements">
                                                <a class="btn btn-primary btn-sm mr-2 pull-right text-white" onclick="printDailyReport()">Print</a>
                                            </div>
                                            <div class="clearfix"></div>
                                        <div class="card" id="card">
                                        <div class="">
                                            <div class="row mx-1">
                                                <!-- <h4 class="col-md-1"></h4> -->
                                                <div style="width: 50%" >
                                                    <label id="lbl_selected_vendor_name" class="">
                                                        Account: <?= get_user_name('bank', $bankId) ?>
                                                    </label><br>
                                                    <label>Previous Balance: <?= $previous_balance ?></label><br>
                                                    <label id="lbl_total_balance" ></label>
                                                </div>
                                                <div style="width: 50%" class="text-right">
                                                    <label>Start Date:  <span><?= parse_date($start_date) ?></span></label><br>
                                                    <label>End Date: <span><?= parse_date($end_date) ?></span></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-content collapse show mx-1">
                                            <div class="table-responsive">
                                            <table id="tbl" width="100%" class="">
                                                <thead class="black">
                                                <tr>
                                                    <th style="width: 4%">#</th>
                                                    <th style="width: 8%">Date</th>
                                                    <th style="width: 15%">Detail</th>
                                                    <th style="width: 10%">Debit</th>
                                                    <th style="width: 10%">Credit</th>
                                                    <th style="width: 12%">Total Balance</th>
                                                    <!-- <th style="width: 12%">Remaining Amount</th> -->
                                                </tr>
                                                </thead>
                                                <tbody id="tbody">
                                                <?php
                                                $totalBalance =$previous_balance;
                                                    foreach($transactions as $k=>$data){
                                                        $totalBalance +=  $data->debit - $data->credit;
                                                        ?>
                                                <tr>
                                                    <td><?= ($k+1) ?></td>
                                                    <td><?= parse_date($data->date) ?></td>
                                                    <td><?= $data->detail ?></td>
                                                    <td><?= $data->debit ?></td>
                                                    <td><?= $data->credit ?></td>
                                                    <td><?= $totalBalance ?></td>
                                                </tr>
                                                    <?php }?>
                                                </tbody>
                                            </table>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                                <script>document.getElementById('lbl_total_balance').innerText='Balance: '+<?=$totalBalance ?></script>
                            <?php }?>


                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>






<script>
    function printDailyReport()
    {
        $("table").removeClass("table table-striped table-bordered ");
        $('#card').printThis({
            debug: false,               // show the iframe for debugging
            importCSS: true,            // import parent page css
            importStyle: true,         // import style tags
            printContainer: true,       // print outer container/$.selector
            loadCSS: "",                // path to additional css file - use an array [] for multiple
            pageTitle: "Account transactions detail",              // add title to print page
            removeInline: false,        // remove inline styles from print elements
            removeInlineSelector: "*",  // custom selectors to filter inline styles. removeInline must be true
            printDelay: 333,            // variable print delay
            header: null,               // prefix to html
            footer: null,               // postfix to html
            base: false,                // preserve the BASE tag or accept a string for the URL
            formValues: true,           // preserve input/form values
            canvas: false,              // copy canvas content
            doctypeString: '',       // enter a different doctype for older markup
            removeScripts: false,       // remove script tags from print content
            copyTagClasses: false,      // copy classes from the html & body tag
            beforePrintEvent: null,     // function for printEvent in iframe
            beforePrint: null,          // function called before iframe is filled
            afterPrint: null            // function called before iframe is removed
        });
    }
</script>

