<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>


<div class="app-content content">

    <div class="content-wrapper">
        <div class="content-wrapper-before"></div>

        <div class="content-body">
            <!-- Basic form layout section start -->
            <section id="basic-form-layouts">
                <div class="row match-height">

                    <div class="offset-md-1 col-md-10">
                        <div class="card">
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <?php if ($this->session->flashdata('msg')): ?>
                                        <?php echo $this->session->flashdata('msg'); ?>
                                    <?php endif; ?>
                                    <form method="post" class="form"
                                          action="<?php echo base_url(); ?>Cash/saveUpdateCash"
                                          enctype="multipart/form-data" accept-charset="ISO-8859-1">
                                        <div class="form-body">
                                            <h4 class="form-section">
                                                <i class="ft-flag"></i>Edit Cash Transactions
                                            </h4>
                                            <div class="row">

                                                <div class="col-md-6 form-group">
                                                    <label for="companyName">Transaction Type</label>
                                                    <select name="update_type" id="update_type"
                                                            class="form-control form-control-primary">
                                                        <option value="<?= $vendor->cash_type ?>"><?php if ($vendor->cash_type == 0) {
                                                                echo 'Cash Payment';
                                                            } else {
                                                                echo 'Cash Receive';
                                                            } ?></option>
                                                    </select>
                                                </div>
                                                <div class="col-md-6 form-group">
                                                    <label for="name">Ref:#</label>
                                                    <input type="text" id="referance" value="<?= $vendor->voucher_no ?>" class="form-control">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6 form-group">
                                                    <label for="name">Date</label>
                                                    <input type="date" name="current_date" id="date"
                                                           value="<?= $vendor->date ?>" class="form-control">
                                                </div>
                                            </div>

                                            <hr>

                                            <div class="row">

                                                <div class="col-md-6 form-group">
                                                    <label for="companyName">Account Type</label>
                                                    <select name="account_type" id="account_type"
                                                            class="form-control form-control-primary" >
                                                        <option <?= $vendor->account_type=='vendor' ? 'selected':'' ?>
                                                                value="vendor">Vendor</option>
                                                        <option <?= $vendor->account_type=='employee' ? 'selected':'' ?>
                                                                value="employee">Employee</option>
                                                        <option <?= $vendor->account_type=='bank' ? 'selected':'' ?>
                                                                value="bank">Personal Account</option>
                                                    </select>
                                                </div>

                                                <div class="col-md-6 form-group">
                                                    <label for="name">Narration</label>
                                                    <input type="text" name="det" id="detail" class="form-control"
                                                           value="<?= $vendor->detail ?>">
                                                </div>
                                            </div>

                                            <div class="row">

                                                <div class="col-md-6 form-group">
                                                    <label for="companyName">Account</label>
                                                    <select id="customer_id" name="vendor_id"
                                                            class="form-control form-control-primary">
                                                        <?php foreach ($accounts as $ac){ ?>
                                                            <option <?= $ac->id == $vendor->vendor_id ? 'selected':'' ?> value="<?= $ac->id ?>"><?= $ac->name ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>

                                                <div class="col-md-6 form-group">
                                                    <label for="name">Amount</label>
                                                    <input type="number" id="amount" name="amount" class="form-control"
                                                           value="<?= $vendor->amount ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <input type="hidden" name="update_id" value="<?= $vendor->id ?>">
                                        <button type="submit" class="btn btn-primary" style="float:right;margin: 2px;">
                                            Update
                                        </button>

                                    </form>
                                </div>


                            </div>
                        </div>
                    </div>


                </div>

        </div>
        </section>

        <!-- // Basic form layout section end -->

    </div>
</div>
</div>


<script>
</script>

        
