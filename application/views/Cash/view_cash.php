
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-wrapper-before"></div>

        <div class="content-body">
            <!-- Basic form layout section start -->
            <section id="basic-form-layouts">
                <div class="row match-height">

                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <?php if ($this->session->flashdata('msg')): ?>
                                        <?php echo $this->session->flashdata('msg'); ?>
                                    <?php endif; ?>
                                    <form method="post" class="form" action="<?php echo base_url(); ?>cash/viewCash"
                                          enctype="multipart/form-data" accept-charset="ISO-8859-1">
                                        <div class="form-body">
                                            <h4 class="form-section">
                                                <i class="ft-flag"></i>View Cash
                                            </h4>
                                            <div class="row">


                                                <div class="col-md-5 form-group">
                                                    <label for="">Start Date.</label>
                                                    <input type="date" name="start_date"
                                                           value="<?= isset($start_date) ? $start_date : date('Y-m-01') ?>"
                                                           class="form-control">
                                                </div>

                                                <div class="col-md-5 form-group">
                                                    <label for="">End Date.</label>
                                                    <input type="date" name="end_date"
                                                           value="<?= isset($end_date) ? $end_date : date('Y-m-d') ?>"
                                                           class="form-control">
                                                </div>

                                                <div class="col-md-2 mt-1">
                                                    <button type="submit" name="filter" class="btn btn-primary m-1">
                                                        <i class="la la-check-square-o"></i> Filter
                                                    </button>
                                                </div>
                                            </div>


                                        </div>

                                    </form>

                                </div>

                                <div class="row">
                                    <div class="col-12">
                                        <div class="card">
                                            <div class="card-header">
                                                <h4 class="card-title">Cash Transaction</h4>
                                                <div class="heading-elements">
                                                    <!-- <a class="btn btn-sm btn-danger box-shadow-2 round btn-min-width pull-right" href="<?= base_url() ?>Vandors/addVandor">Add Vandor</a> -->
                                                </div>
                                                <a class="heading-elements-toggle"><i
                                                            class="la la-ellipsis-v font-medium-3"></i></a>
                                            </div>
                                            <div class="card-content collapse show">
                                                <div class="table-responsive">
                                                    <table id="tbl"
                                                           class="table table-striped table-bordered zero-configuration">
                                                        <thead class="bg-primary white">
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Date</th>
                                                            <th>Type</th>
                                                            <th>Voucher#</th>
                                                            <th>Type</th>
                                                            <th>Name</th>
                                                            <th>Detail</th>
                                                            <th>Amount</th>
                                                            <th>Action</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody id="tbody">
                                                        <?php $i = 0;
                                                        foreach ($cash as $data) {
                                                            $i++;
                                                            ?>
                                                            <tr>
                                                                <td><?= $i ?></td>
                                                                <td><?= parse_date($data->date) ?></td>
                                                                <td><?= $data->cash_type == 0 ? 'Cash Payment' : 'Cash Receive' ?></td>
                                                                <td><?= $data->voucher_no ?></td>
                                                                <td><?= $data->account_type ?></td>
                                                                <td><?php
                                                                    if ($data->account_type == 'vendor') {
                                                                        echo get_user_name('vandors', $data->paid_person_id);
                                                                    } else if ($data->account_type == 'employee') {
                                                                        echo get_user_name('employees', $data->paid_person_id);
                                                                    } else if ($data->account_type == 'bank') {
                                                                        echo get_user_name('bank', $data->paid_person_id);
                                                                    } ?>
                                                                </td>
                                                                <td><?= $data->detail ?></td>
                                                                <td><?= $data->amount ?></td>
                                                                <td>
                                                                    <a href="<?php echo base_url() ?>Cash/updateCash/<?= $data->id ?>">
                                                                        <i class="ft-edit text-primary"></i></i>
                                                                    </a>
                                                                    <a onclick="deleteCash('<?= base_url() ?>Cash/cashDelete/<?= $data->id ?>')"
                                                                       class="a_button"><i class="ft-trash-2 text-danger"></i></a>
                                                                </td>

                                                            </tr>
                                                        <?php } ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </section>


        </div>
    </div>
</div>


<script type="text/javascript">

    function deleteCash(url) {
        var r = confirm("Are you sure, You wants to delete ?");
        if (r == true) {
            window.open(url, "_self");
        }
    }

</script>