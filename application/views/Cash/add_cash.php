<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>


<div class="app-content content">

    <div class="content-wrapper">
        <div class="content-wrapper-before"></div>

        <div class="content-body">
            <!-- Basic form layout section start -->
            <section id="basic-form-layouts">
                <div class="row match-height">

                    <div class="offset-md-1 col-md-10">
                        <div class="card">
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <?php if ($this->session->flashdata('msg')): ?>
                                        <?php echo $this->session->flashdata('msg'); ?>
                                    <?php endif; ?>
                                    <form method="post" class="form" action="<?php echo base_url(); ?>Cash/add_cash"
                                          enctype="multipart/form-data" accept-charset="ISO-8859-1">
                                        <div class="form-body">
                                            <h4 class="form-section">
                                                <i class="ft-flag"></i>Cash Transactions
                                            </h4>
                                            <div class="row">

                                                <div class="col-md-6 form-group">
                                                    <label for="companyName">Transaction Type</label>
                                                    <select name="update_type" id="update_type"
                                                            class="form-control form-control-primary">
                                                        <option value="0">Cash Payment</option>
                                                        <option value="1">Cash Receive</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-6 form-group">
                                                    <label for="name">Ref:#</label>
                                                    <input type="text" id="referance" class="form-control">
                                                </div>
                                            </div>

                                            <div class="row">

                                                <div class="col-md-6 form-group">
                                                    <label for="name">Date</label>
                                                    <input type="date" name="current_date" id="date" value="<?=
                                                    date('Y-m-d') ?>" class="form-control">
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="row">

                                                <div class="col-md-6 form-group">
                                                    <label for="companyName">Transaction Type</label>
                                                    <select name="account_type" id="account_type"
                                                            class="form-control form-control-primary">
                                                        <option value="">Select Account Type ---</option>
                                                        <option value="vendor">Vendor</option>
                                                        <option value="employee">Employee</option>
                                                        <option value="bank">Personal Account</option>
                                                    </select>
                                                </div>

                                                <div class="col-md-6 form-group">
                                                    <label for="name">Narration/Detail</label>
                                                    <input type="text" id="detail" class="form-control">
                                                </div>
                                            </div>

                                            <div class="row">

                                                <div class="col-md-6 form-group">
                                                    <label for="companyName">Account</label>
                                                    <select id="customer_id" name="vendor_id"
                                                            class="form-control form-control-primary">
                                                        <option value="">Select Account ---</option>
                                                        <?php if (!empty($Accounts)): ?>
                                                            <?php foreach ($Accounts as $oa): ?>
                                                                <option value="<?= $oa->id ?>"><?= $oa->name ?></option>
                                                            <?php endforeach ?>
                                                        <?php endif ?>
                                                    </select>
                                                </div>

                                                <div class="col-md-6 form-group">
                                                    <label for="name">Amount</label>
                                                    <input type="number" onclick="this.select()" id="amount" class="form-control" value="0">
                                                </div>
                                            </div>
                                        </div>


                                        <div class="form-actions" style="float:right;margin: 10px;">
                                            <span class="text-danger" id="dynamic_error"></span>
                                            <input type="button" class="btn btn-success btn-sm" id="dynamic_btn" value="Add">
                                        </div>
                                        <div class="clearfix"></div>

                                        <table class="table">
                                            <thead>
                                            <tr class="bg-primary">
                                                <th style="color: #fff;">Code</th>
                                                <th style="color: #fff;">Account Name</th>
                                                <th style="color: #fff;">Narration</th>
                                                <th style="color: #fff;">Amount</th>
                                                <th style="color: #fff;">Action</th>
                                            </tr>
                                            </thead>
                                            <input type="hidden" name="index" value="0">
                                            <tbody id="dynamic_tbl">
                                            </tbody>
                                        </table>
                                        <br><br>
                                        <button type="submit" class="btn btn-primary" style="float:right;margin: 2px;">
                                            Submit
                                        </button>
                                        <div class="clearfix"></div>

                                    </form>
                                </div>


                            </div>
                        </div>
                    </div>


                </div>

        </div>
        </section>

        <!-- // Basic form layout section end -->

    </div>
</div>
</div>


<script>

    $('#dynamic_btn').click(function () {
        var type = $('#type').val();
        var vocherNo = $('#vocherNo').val();
        var referance = $('#referance').val();
        var date = $('#date').val();
        var customer_id = $('#customer_id').val();
        var customerName = $('#customer_id :selected').text();
        var accountType = $('#account_type').val();
        var detail = $('#detail').val();
        var amount = $('#amount').val();

        if (date == "") {
            $('#dynamic_error').html('Date is Required*');
        } else if (accountType == "") {
            $('#dynamic_error').html('Please select an account type*');
        } else if (customer_id == "") {
            $('#dynamic_error').html('Please select an account*');
        } else if (detail == "") {
            $('#dynamic_error').html('Narration is Required*');
        } else if (amount == "" || amount == 0) {
            $('#dynamic_error').html('Amount should be grater then 0');
        } else {
            $('#dynamic_error').html('');
            var index = $('input[name="index"]').val();
            var i = parseInt(index) + parseInt(1);
            $('input[name="index"]').val(i);

            $tbody = '<tr><td>' + i + '</td><td>' + customerName + '</td><td>' + detail + '</td><td>' + amount + '</td><td><i class="ft-trash-2 text-danger" id="delete_row" style="cursor:pointer"></i><input type="hidden" name="type[]" value="' + type + '" /><input type="hidden" name="vocherNo[]" value="' + vocherNo + '" /><input type="hidden" name="customerName[]" value="' + customerName + '" /><input type="hidden" name="customer_id[]" value="' + customer_id + '" /><input type="hidden" name="referance[]" value="' + referance + '" /><input type="hidden" name="date[]" value="' + date + '" /><input type="hidden" name="detail[]" value="' + detail + '" /><input type="hidden" name="amount[]" value="' + amount + '" /><input type="hidden" name="accountType[]" value="' + accountType + '" /></td></tr>';

            //console.log($tbody);
            $('#dynamic_tbl').append($tbody);
            $('#detail').val('');
            $('#amount').val(0);
            $('#accountType').val('');
            $('#referance').val('');
        }


    });


    $('#account_type').change(function () {
        var account = $(this).val();

        if (account == "") {
            $('#dynamic_error').html('Please Select Account Type*');
        } else {
            $('#dynamic_error').html('');
            $('#loading_bar').css("display", "block");
            $.ajax({
                url: "getAccounts",
                method: "POST",
                data: {account: account},
                success: function (success) {
                    $('#loading_bar').css("display", "none");
                    //alert(success);
                    if (success != "") {
                        var data = JSON.parse(success);
                        console.log(data.length);
                        if ($('#account_type').val() == "Account") {
                            $('#customer_id').html('<option value="">Select Account---</optoin>')
                            for (var i = 0; i < data.length; i++) {
                                $('#customer_id').append('<option value="' + data[i].id + '">' + data[i].bankName + '</option>');
                            }
                        } else {
                            $('#customer_id').html('<option value="">Select Account---</optoin>')
                            for (var i = 0; i < data.length; i++) {
                                $('#customer_id').append('<option value="' + data[i].id + '">' + data[i].name + '</option>');
                            }
                        }
                    } else {
                        alert('No Record Found');
                    }
                }, error: function (xhr) {
                    console.log(xhr.responseText);
                }
            });
        }
    });


    $('body').delegate('#delete_row', 'click', function (event) {
        var trToDelete = $(this).parent().parent();

        //alert(trToDelete);

        var conf = confirm('Are you sure you want to delete?');

        if (conf == true) {
            trToDelete.remove();
        }

    });


</script>

        
