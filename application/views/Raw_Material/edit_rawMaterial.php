<div class="app-content content">
  <div class="content-wrapper">
    <div class="content-wrapper-before"></div>

    <div class="content-body">
      <!-- Basic form layout section start -->
      <section id="basic-form-layouts">
        <div class="row match-height">

          <div class="offset-md-1 col-md-10">
            <div class="card">
              <div class="card-content collapse show">
                <div class="card-body">
                <?php if($this->session->flashdata('msg')):?>
                  <?php echo $this->session->flashdata('msg');?>
                <?php endif;?>
                  <form method="post" class="form" action="<?php echo base_url();?>Raw_Material/saveUpdateRawMaterial" enctype="multipart/form-data" accept-charset="ISO-8859-1">
                    <div class="form-body">
                      <h4 class="form-section">
                        <i class="ft-flag"></i>update Raw Material</h4>
                        <div class="row">  
                        <input type="hidden" name="update_id" value="<?= $update_id ?>" />                      
                        <div class="col-md-6 form-group">
                          <label for="companyName">Full Name</label>
                          <input type="text" value="<?= $customer->name ?>" class="form-control" placeholder="Name" name="name">
                        </div>

                        <div class="col-md-6 form-group">
                          <label for="companyName">Unit</label>
                          <select name="unit" class="form-control">
                              <option disabled> Select Units</option>
                            <?php foreach($Units as $unit){?>
                              <option value="<?= $unit->id?>" <?php if($unit->id == $customer->unit){echo 'selected';} ?>><?= $unit->measure_unit?></option>
                              <?php } ?>
                          </select>
                        </div>
                        
                      </div>
                      <div class="row">
                        <div class="col-md-6 form-group">
                          <label for="companyName">Purchase Price</label>
                          <input pattern="^\d*(\.\d{0,2})?$" value="<?= $customer->purchase_price ?>"  class="form-control" placeholder="Purchase Price" name="purchase_price">
                        </div>

                        <div class="col-md-6 form-group">
                          <label for="companyName">Qty</label>
                          <input type="number" value="<?= $customer->qty ?>"  class="form-control" placeholder="Quantity" name="qty">
                        </div>
                        <div class="col-md-6 form-group">
                          <label for="companyName">Alert Qty</label>
                          <input type="number" value="<?= $customer->alert_qty ?>"  class="form-control" placeholder="Alert Quantity" name="alert_qty">
                        </div>
                      </div>
                      
                    </div>

                    <div class="form-actions">
                      <button type="submit" class="btn btn-primary">
                        <i class="la la-check-square-o"></i> Submit
                      </button>
                    </div>
                  </form>
                </div>
              </div>
            </div>


          </div>

        </div>
      </section>

      <!-- // Basic form layout section end -->
    </div>
  </div>
</div>
