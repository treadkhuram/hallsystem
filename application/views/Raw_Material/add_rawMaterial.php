<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-wrapper-before"></div>

        <div class="content-body">
            <!-- Basic form layout section start -->
            <section id="basic-form-layouts">
                <div class="row match-height">

                    <div class="offset-md-1 col-md-10">
                        <div class="card">
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <?php if($this->session->flashdata('msg')):?>
                                    <?php echo $this->session->flashdata('msg');?>
                                    <?php endif;?>
                                    <form method="post" class="form" action="<?php echo base_url();?>Raw_Material/saveRawMaterial"
                                        enctype="multipart/form-data" accept-charset="ISO-8859-1">
                                        <div class="form-body">
                                            <h4 class="form-section">
                                                <i class="ft-flag"></i>Add New Raw Material
                                            </h4>
                                            <div class="row">
                                                <div class="col-md-6 form-group">
                                                    <label for="name">Full Name</label>
                                                    <input type="text" required id="name" class="form-control" placeholder="Name"
                                                        name="name">
                                                </div>

                                            <div class="col-md-6 form-group">
                                                <label for="companyName">Unit</label>
                                                <select name="unit" required class="form-control">
                                                    <option value="" disabled selected> Select Units</option>
                                                   <?php foreach($Units as $unit){?>
                                                    <option value="<?= $unit->id?>"><?= $unit->measure_unit?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                                   </div>
                                            <div class="row">
                                                <div class="col-md-6 form-group">
                                                    <label for="purchase_price">Purchase Price</label>
                                                    <input pattern="^\d*(\.\d{0,2})?$" required id="purchase_price" class="form-control" placeholder="Purchase Price"
                                                        name="purchase_price">
                                                </div>

                                                <div class="col-md-6 form-group">
                                                    <label for="qty">Quantity</label>
                                                    <input type="number" required value="0" onclick="this.select()" id="qty" class="form-control"
                                                        placeholder="Quantity" name="qty">
                                                </div>
                                                <div class="col-md-6 form-group">
                                                    <label for="alert_qty">Alert Quantity</label>
                                                    <input type="text" required value="0" onclick="this.select()" id="alert_qty" class="form-control"
                                                        placeholder="Alert Quantity" name="alert_qty">
                                                </div>
                                            </div>

                                        </div>

                                        <div class="form-actions">
                                            <button type="submit" class="btn btn-primary">
                                                <i class="la la-check-square-o"></i> Submit
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>


                    </div>

                </div>
            </section>

            <!-- // Basic form layout section end -->
        </div>
    </div>
</div>