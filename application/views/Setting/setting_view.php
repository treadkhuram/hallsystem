<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-wrapper-before"></div>
        <div class="content-body">
            <section id="basic-tabs-components">
                <div class="row match-height">
                    <div class="col-xl-12 col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">All Bookings</h4>
                                <div class="heading-elements">
                                    <a class="btn btn-sm btn-danger box-shadow-2 round btn-min-width pull-right"
                                       href="<?= base_url() ?>Booking/addBooking">Add Booking</a>
                                </div>
                                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                            </div>
                            <div class="card-content collapse show">
                                <?php if($this->session->flashdata('msg')):?>
                                    <?php echo $this->session->flashdata('msg');?>
                                <?php endif;?>
                                <div class="table-responsive">

                                </div>
                            </div>
                            <div class="card-content">
                                <div class="card-body">
                                    <ul class="nav nav-tabs">
                                        <li class="nav-item">
                                            <a style=" border: solid;border-color: #d4d0cf;border-width: 1px;"
                                               class="nav-link active" id="base-tab1" data-toggle="tab" aria-controls="newOrder"
                                               href="#newOrder" aria-expanded="true">Active Bookings
                                                <span class="badge badge badge-info badge-pill float-right"></span></a>
                                        </li>
                                        <li class="nav-item">
                                            <a style=" border: solid;border-color: #d4d0cf;border-width: 1px;"
                                               class="nav-link" id="base-tab3" data-toggle="tab" aria-controls="onHold"
                                               href="#onHold" aria-expanded="false">Completed</a>
                                        </li>
                                        <li class="nav-item">
                                            <a style=" border: solid;border-color: #d4d0cf;border-width: 1px;"
                                               class="nav-link" id="base-tab2" data-toggle="tab" aria-controls="underProcess"
                                               href="#underProcess" aria-expanded="false">Canceled</a>
                                        </li>
                                    </ul>
                                    <div class="tab-content px-1 pt-1">
                                        <div role="tabpanel" class="tab-pane active" id="newOrder" aria-expanded="true" aria-labelledby="base-tab1">
                                            <div class="table-responsive">
                                                <table id="tbl" class="table table-striped table-bordered zero-configuration">
                                                    <thead class="bg-primary white">
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Customer Name</th>
                                                        <th>Phone</th>
                                                        <th>Function</th>
                                                        <th>Date</th>
                                                        <th>Time</th>
                                                        <th>Persons</th>
                                                        <th>Actions</th>
                                                    </tr>
                                                    </thead>

                                                </table>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="onHold" aria-labelledby="base-tab3">
                                            <div class="table-responsive">
                                                <table id="tbl" class="table table-striped table-bordered zero-configuration">
                                                    <thead class="bg-primary white">
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Customer Name</th>
                                                        <th>Phone</th>
                                                        <th>Function Name</th>
                                                        <th>Date</th>
                                                        <th>Time</th>
                                                        <th>Persons</th>
                                                        <th>Actions</th>
                                                    </tr>
                                                    </thead>

                                                </table>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="underProcess" aria-labelledby="underProcess">
                                            <div class="table-responsive">
                                                <table id="tbl" class="table table-striped table-bordered zero-configuration">
                                                    <thead class="bg-primary white">
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Customer Name</th>
                                                        <th>Phone</th>
                                                        <th>Function Name</th>
                                                        <th>Date</th>
                                                        <th>Time</th>
                                                        <th>Persons</th>
                                                        <th>Actions</th>
                                                    </tr>
                                                    </thead>

                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

        </div>
    </div>
</div>