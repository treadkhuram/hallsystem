<div class="app-content content">
  <div class="content-wrapper">
    <div class="content-wrapper-before"></div>

    <div class="content-body">
      <!-- Basic form layout section start -->
      <section id="basic-form-layouts">
        <div class="row match-height">

          <div class="offset-md-1 col-md-10">
            <div class="card">
              <div class="card-content collapse show">
                <div class="card-body">
                <?php if($this->session->flashdata('msg')):?>
                  <?php echo $this->session->flashdata('msg');?>
                <?php endif;?>
                  <form method="post" class="form" action="<?php echo base_url();?>Unit/saveUpdateUnit" enctype="multipart/form-data" accept-charset="ISO-8859-1">
                    <div class="form-body">
                      <h4 class="form-section">
                        <i class="ft-flag"></i>update Unit</h4>
                        <div class="row">  
                        <input type="hidden" name="update_id" value="<?= $update_id ?>" />                      
                        <div class="col-md-6 form-group">
                          <label for="companyName">Measure Unit</label>
                          <input type="text" value="<?= $customer->measure_unit ?>" class="form-control" placeholder="Measure Unit" name="measure_unit">
                        </div>

                        <div class="col-md-6 form-group">
                          <label for="companyName">Receipe Unit.</label>
                          <input type="text" value="<?= $customer->receipe_unit ?>" class="form-control" placeholder="Receipe Unit" name="receipe_unit">
                        </div>
                        <div class="col-md-6 form-group">
                          <label for="companyName">Convertion Factor.</label>
                          <input type="text" value="<?= $customer->converstion_factor ?>" class="form-control" placeholder="Receipe Unit" name="converstion_factor">
                        </div>
                      </div>
                      
                    </div>

                    <div class="form-actions">
                      <button type="submit" class="btn btn-primary">
                        <i class="la la-check-square-o"></i> Submit
                      </button>
                    </div>
                  </form>
                </div>
              </div>
            </div>


          </div>

        </div>
      </section>

      <!-- // Basic form layout section end -->
    </div>
  </div>
</div>
