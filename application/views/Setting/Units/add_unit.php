<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-wrapper-before"></div>

        <div class="content-body">
            <!-- Basic form layout section start -->
            <section id="basic-form-layouts">
                <div class="row match-height">

                    <div class="offset-md-1 col-md-10">
                        <div class="card">
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <?php if($this->session->flashdata('msg')):?>
                                    <?php echo $this->session->flashdata('msg');?>
                                    <?php endif;?>
                                    <form method="post" class="form" action="<?php echo base_url();?>Unit/saveUnit"
                                        enctype="multipart/form-data" accept-charset="ISO-8859-1">
                                        <div class="form-body">
                                            <h4 class="form-section">
                                                <i class="ft-flag"></i>Add New Unit
                                            </h4>
                                            <div class="row">
                                                <div class="col-md-6 form-group">
                                                    <label for="name">Measure Unit</label>
                                                    <input type="text" id="name" class="form-control" placeholder="Measure Unit"
                                                        name="measure_unit">
                                                </div>

                                                <div class="col-md-6 form-group">
                                                    <label for="sale_price">Receipe Unit.</label>
                                                    <input type="text" id="sale_price" class="form-control"
                                                        placeholder="Receipe Unit" name="receipe_unit">
                                                </div>
                                                <div class="col-md-6 form-group">
                                                    <label for="factor">Convertion Factor.</label>
                                                    <input type="text" id="factor" class="form-control"
                                                        placeholder="Converston Factor" name="converstion_factor">
                                                </div>
                                            </div>
                                           
                                        </div>

                                        <div class="form-actions">
                                            <button type="submit" class="btn btn-primary">
                                                <i class="la la-check-square-o"></i> Submit
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>


                    </div>

                </div>
            </section>

            <!-- // Basic form layout section end -->
        </div>
    </div>
</div>