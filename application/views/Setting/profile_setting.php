<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-wrapper-before"></div>

        <div class="content-body">
            <!-- Basic form layout section start -->
            <section id="basic-form-layouts">
                <div class="row match-height">

                    <div class="offset-md-1 col-md-10">
                        <div class="card">
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <?php if($this->session->flashdata('msg')):?>
                                        <?php echo $this->session->flashdata('msg');?>
                                    <?php endif;?>
                                    <form autocomplete="off" method="post" class="form" action="<?= base_url();?>Setting/updateProfile" enctype="multipart/form-data" accept-charset="ISO-8859-1">
                                        <div class="form-body">
                                            <h4 class="form-section">
                                                <i class="ft-flag"></i>Admin Profile</h4>
                                            <div class="row">
                                                <div class="col-md-6 form-group">
                                                    <label for="companyName">Name</label>
                                                    <input type="text" value="<?= $user->name ?>" required class="form-control" name="name">
                                                </div>

                                                <div class="col-md-6 form-group">
                                                    <label for="companyName">Email</label>
                                                    <input type="text" value="<?= $user->email ?>" disabled  class="form-control" name = "email">
                                                </div>

                                            </div>

                                            <div class="row">
                                                <div class="col-md-6 form-group">
                                                    <label for="companyName">Previous Password</label>
                                                    <input type="password" autocomplete="off" onkeyup="passwordCheck()" id="cp" class="form-control" name="p_password">
                                                </div>
                                                <div class="col-md-6 form-group">
                                                    <label for="companyName">New Password</label>
                                                    <input type="password" id="password" class="form-control" name="n_password">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6 form-group">
                                                    <label for="companyName">Confirm Password</label>
                                                    <label style="color:red" id="c_pass_error"></label>
                                                    <input type="password" onkeyup="matchPassword()" id="cpassword" class="form-control" name="c_password">
                                                </div>

                                            </div>
                                            <script>
                                                function matchPassword() {
                                                    var p = $('#password').val();
                                                    var cp = $('#cpassword').val();
                                                    if (p != cp) {
                                                        $('#btn_submit').prop('disabled', true);
                                                        $('#c_pass_error').html('Password did not match..');
                                                    }
                                                    else {
                                                        $('#c_pass_error').html('');
                                                        $('#btn_submit').removeAttr('disabled');
                                                    }
                                                }
                                                function passwordCheck(){
                                                    var p = $('#cp').val();
                                                    $('#btn_submit').prop('disabled', true);
                                                    if(p==''){
                                                        $('#c_pass_error').html('');
                                                        $('#btn_submit').removeAttr('disabled');
                                                    }
                                                }
                                            </script>

                                        </div>

                                        <div class="form-actions">
                                            <button id="btn_submit" name="btn_submit" type="submit" class="btn btn-primary">
                                                <i class="la la-check-square-o"></i> Save Changes
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>


                    </div>

                </div>
            </section>

            <!-- // Basic form layout section end -->
        </div>
    </div>
</div>

