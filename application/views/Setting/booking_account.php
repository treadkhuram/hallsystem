<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-wrapper-before"></div>

        <div class="content-body">
            <!-- Basic form layout section start -->
            <section id="basic-form-layouts">
                <div class="row match-height">

                    <div class="offset-md-1 col-md-10">
                        <div class="card">
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <?php if ($this->session->flashdata('msg')): ?>
                                        <?php echo $this->session->flashdata('msg'); ?>
                                    <?php endif; ?>
                                    <form method="post" class="form" action="<?php echo base_url(); ?>Setting/bookingAccount"
                                          enctype="multipart/form-data" accept-charset="ISO-8859-1">
                                        <div class="form-body">
                                            <h4 class="form-section">
                                                <i class="ft-flag"></i>Booking Account
                                            </h4>

                                            <div class="row">
                                                <div class="col-md-6 form-group">
                                                    <label for="name">Select booking transaction account</label>
                                                    <select class="select2 form-control" name="booking_account">
                                                        <?php foreach ($accounts as $ac){ ?>
                                                            <option <?= $ac->id == $booking_account->table_id ? 'selected' : '' ?> value="<?= $ac->id ?>"><?= $ac->name ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="form-actions">
                                            <button type="submit" name="submit" class="btn btn-primary">
                                                <i class="la la-check-square-o"></i> Submit
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>


                    </div>

                </div>
            </section>

            <!-- // Basic form layout section end -->
        </div>
    </div>
</div>