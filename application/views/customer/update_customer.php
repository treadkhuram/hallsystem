<div class="app-content content">
  <div class="content-wrapper">
    <div class="content-wrapper-before"></div>

    <div class="content-body">
      <!-- Basic form layout section start -->
      <section id="basic-form-layouts">
        <div class="row match-height">

          <div class="offset-md-1 col-md-10">
            <div class="card">
              <div class="card-content collapse show">
                <div class="card-body">
                <?php if($this->session->flashdata('msg')):?>
                  <?php echo $this->session->flashdata('msg');?>
                <?php endif;?>
                  <form method="post" class="form" action="<?php echo base_url();?>customer/saveUpdateCustomer" enctype="multipart/form-data" accept-charset="ISO-8859-1">
                    <div class="form-body">
                      <h4 class="form-section">
                        <i class="ft-flag"></i>update Customer</h4>
                        <div class="row">  
                        <input type="hidden" name="update_id" value="<?= $update_id ?>" />                      
                        <div class="col-md-6 form-group">
                          <label for="companyName">Full Name</label>
                          <input type="text" value="<?= $customer->full_name ?>" class="form-control" placeholder="Name" name="name">
                        </div>

                        <div class="col-md-6 form-group">
                          <label for="companyName">Mobile No.</label>
                          <input type="text" value="<?= $customer->mobile ?>" class="form-control" placeholder="Mobile No" name="phone">
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-6 form-group">
                          <label for="companyName">Email</label>
                          <input type="email" value="<?= $customer->email ?>"  class="form-control" placeholder="Email" name="email">
                        </div>

                        <div class="col-md-6 form-group">
                          <label for="companyName">Password</label>
                          <input type="password" value="<?= $customer->password ?>"  class="form-control" placeholder="password" name="password">
                        </div>
                      </div>
                      <div class="row">
                          <div class="col-md-6 form-group">
                            <label for="companyName">Gender</label>
                            <select name="gender" class="form-control">
                            <option <?= $customer->gender == 'Male' ? "selected" : '' ?> >Male</option>
                            <option <?= $customer->gender == 'Female' ? "selected" : '' ?> >Female</option>
                            </select>
                          </div>
                      <div class="col-md-6 form-group">
                        <label for="qty">Cover Image</label>
                        <input type="file" data-validation-allowing="gif,png,jpg" id="img1" name="image" class="form-control-file">
                      </div>
                    </div>

                    </div>

                    <div class="form-actions">
                      <button type="submit" class="btn btn-primary">
                        <i class="la la-check-square-o"></i> Submit
                      </button>
                    </div>
                  </form>
                </div>
              </div>
            </div>


          </div>

        </div>
      </section>

      <!-- // Basic form layout section end -->
    </div>
  </div>
</div>
