<style>
	/* .table th,
	.table td {
		padding: 1rem 0.2rem;
	} */

	
.tab_border {
    border: solid;
    border-color: #000;
    border-width: 1px;
}

</style>
<div class="app-content content">
	<div class="content-wrapper">
		<div class="content-wrapper-before"></div>
		<div class="content-header row">
			<div class="content-header-left col-md-4 col-12 mb-2">
				<h3 class="content-header-title">Customer Detail</h3>
			</div>

		</div>
		<div class="content-body">
			<section>
				<div class="row">
					<div class="col-12 col-md-4">
						<div class="card">
							<div class="card-header">
								<h4 class="card-title">Personal Info</h4>
								<div class="heading-elements">
									<ul class="list-inline mb-0 display-block">
										<li>
											<a class="btn btn-md  round btn-min-width pull-right" href="#"
												target="_blank">
											</a>
										</li>
									</ul>
								</div>
							</div>
							<div class="card-content collapse show">
								<div class="card-body pt-0 pb-1">
									<div class="table-responsive">
										<table id="tbl" class="table table-bordered">
											<tbody>
												<tr>
													<td>Name</td>
													<td><?= $customer->full_name ?></td>
												</tr>
												<tr>
													<td>Email</td>
													<td><?= $customer->email ?></td>
												</tr>
												<tr>
													<td>Mobile</td>
													<td><?= $customer->mobile ?></td>
												</tr>
												<tr>
													<td>Gender</td>
													<td><?= $customer->gender ?></td>
												</tr>
												
											</tbody>
										</table>
										


									</div>


								</div>
							</div>
						</div>
					</div>
					<div class="col-12 col-md-8">
						<div class="card">
							<div class="card-header">
								<ul class="nav nav-tabs">
									<li class="nav-item">
										<a style=" border: solid;border-color: #d4d0cf;border-width: 1px;"
										 class="nav-link active" id="base-tab1" data-toggle="tab"
											aria-controls="active" href="#active" aria-expanded="true">Active
										</a>
									</li>
									<li class="nav-item">
										<a style=" border: solid;border-color: #d4d0cf;border-width: 1px;" class="nav-link tab_border" id="base-tab3" data-toggle="tab" aria-controls="history"
											href="#history" aria-expanded="false">History</a>
									</li>
								</ul>
							</div>
							<div class="card-content collapse show">
								<div class="card-body pt-0 pb-1">
									<div class="tab-content px-1 pt-1">
										<div role="tabpanel" class="tab-pane active" id="active" aria-expanded="true"
											aria-labelledby="base-tab1">
											<div class="table-responsive">
												<table id="tbl"
													class="table table-striped table-bordered zero-configuration">
													<thead class="bg-primary white">
														<tr>
															<th>#</th>
															<th>Items</th>
															<th>Pickup</th>
															<th>Drop off</th>
															<th>Date</th>
															<th>Status</th>
															<th>Amount</th>
															<th>Action</th>
														</tr>
													</thead>
													<?php foreach($orderData as $key=>$ad){ if($ad->order_status != 6){ ?>
                                                            <tr>
                                                                <td><?= $key+1 ?></td>
                                                                <td><?= $ad->type ?></td>
                                                                <td><?= $ad->pickup_address ?></td>
                                                                <td><?= $ad->dropoff_address ?></td>
                                                                <td><?= $ad->pickup_date ?></td>
                                                                <td>
                                                                    <?php
                                                                        $status='';
                                                                        if($ad->order_status == 1)
                                                                          $status = 'New Order';
                                                                        else if($ad->order_status == 2)
                                                                          $status = 'On Hold';
                                                                        else if($ad->order_status == 3)
                                                                          $status = 'In Process';
                                                                        else if($ad->order_status == 4 || $ad->order_status == 5)
                                                                          $status = 'Dispatch';
                                                                        else if($ad->order_status == 6)
                                                                          $status = 'Delivered';
                                                                        else if($ad->order_status == 7)
                                                                          $status = 'Canceled';
                                                                        echo $status;
                                                                    ?>
                                                                </td>
                                                                <td><?= $ad->total_amount ?></td>
                                                                <td> <a ><i class="ft-edit text-primary"></i></i></a></td>
                                                            </tr>
                                                        <?php } } ?>
													
												</table>
											</div>


										</div>

										<div class="tab-pane" id="history" aria-labelledby="base-tab3">
											<div class="table-responsive">
												<table id="tbl"
													class="table table-striped table-bordered zero-configuration">
													<thead class="bg-primary white">
                                                        <tr>
															<th>#</th>
															<th>Items</th>
															<th>Pickup</th>
															<th>Drop off</th>
															<th>Date</th>
															<th>Status</th>
															<th>Amount</th>
															<th>Action</th>
														</tr>
													</thead>
													<tbody>
														<?php foreach($orderData as $key=>$ad){ if($ad->order_status == 6){ ?>
                                                            <tr>
                                                                <td><?= $key+1 ?></td>
                                                                <td><?= $ad->type ?></td>
                                                                <td><?= $ad->pickup_address ?></td>
                                                                <td><?= $ad->dropoff_address ?></td>
                                                                <td><?= $ad->pickup_date ?></td>
                                                                <td>
                                                                    <?php
                                                                        $status='';
                                                                        if($ad->order_status == 1)
                                                                          $status = 'New Order';
                                                                        else if($ad->order_status == 2)
                                                                          $status = 'On Hold';
                                                                        else if($ad->order_status == 3)
                                                                          $status = 'In Process';
                                                                        else if($ad->order_status == 4 || $ad->order_status == 5)
                                                                          $status = 'Dispatch';
                                                                        else if($ad->order_status == 6)
                                                                          $status = 'Delivered';
                                                                        else if($ad->order_status == 7)
                                                                          $status = 'Canceled';
                                                                        echo $status;
                                                                    ?>
                                                                </td>
                                                                <td><?= $ad->total_amount ?></td>
                                                                <td> <a ><i class="ft-edit text-primary"></i></i></a></td>
                                                            </tr>
                                                        <?php } } ?>
													</tbody>
												</table>
											</div>
										</div>


									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</section>
		</div>
	</div>
</div>
