<div class="app-content content">
  <div class="content-wrapper">
    <div class="content-wrapper-before"></div>

    <div class="content-body">
      <!-- Basic form layout section start -->
      <section id="basic-form-layouts">
        <div class="row match-height">


          <div class="col-md-12">

            <div class="row">
              <div class="col-12">
                <div class="card">
                  <div class="card-header">
                    <h4 class="card-title">All Customers</h4>
                    <div class="heading-elements">
                        <a class="btn btn-sm btn-danger box-shadow-2 round btn-min-width pull-right" href="<?= base_url() ?>customer/addCustomer">Add Customer</a>
                    </div>
                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                  </div>
                  <div class="card-content collapse show">
                  <?php if($this->session->flashdata('msg')):?>
                    <?php echo $this->session->flashdata('msg');?>
                  <?php endif;?>
                    <div class="table-responsive">
                      <table id="tbl" class="table table-striped table-bordered zero-configuration">
                        <thead class="bg-primary white">
                          <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Mobile</th>
                            <th>Email</th>
                            <th>Gender</th>
                            <th>Actions</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php foreach($allCustomer as $key=>$ac){ ?>
                            <tr>
                              <td><?= $key+1 ?></td>
                              <td><?= $ac->full_name ?></td>
                              <td><?= $ac->mobile ?></td>
                              <td><?= $ac->email ?></td>
                              <td><?= $ac->gender ?></td>
                              <td>
                              <a href="<?= base_url().'customer/customerDetail/'.$ac->id?>"><i class="ft-eye text-info"></i></i></a>
                              <a href="<?= base_url().'customer/updateCustomer/'.$ac->id ?>"><i class="ft-edit text-primary"></i></i></a>
                              <a onclick="deleteCustomer('<?= base_url().'customer/deleteCustomer/'.$ac->id ?>')" class="a_button"><i class="ft-trash-2 text-danger"></i></a></td>
                            </tr>
                          <?php } ?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  </div>
</div>

<script>
function deleteCustomer(url){
      var r = confirm("Are you sure, You wants to delete ?");
      if (r == true) {
        window.open(url,"_self");
      }
    }
</script>


