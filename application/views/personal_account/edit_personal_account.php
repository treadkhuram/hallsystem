<div class="app-content content">
  <div class="content-wrapper">
    <div class="content-wrapper-before"></div>

    <div class="content-body">
      <!-- Basic form layout section start -->
      <section id="basic-form-layouts">
        <div class="row match-height">

          <div class="offset-md-1 col-md-10">
            <div class="card">
              <div class="card-content collapse show">
                <div class="card-body">
                <?php if($this->session->flashdata('msg')):?>
                  <?php echo $this->session->flashdata('msg');?>
                <?php endif;?>
                  <form method="post" class="form" action="<?php echo base_url();?>PersonalAccount/updatePersonalAccount" enctype="multipart/form-data" accept-charset="ISO-8859-1">
                    <div class="form-body">
                      <h4 class="form-section">
                        <i class="ft-flag"></i>Update Account</h4>
                        <div class="row">  
                        <input type="hidden" name="id" value="<?= $accounts->id ?>" />                      
                        <div class="col-md-6 form-group">
                          <label for="companyName">Full Name</label>
                          <input type="text" value="<?= $accounts->name ?>" required class="form-control" placeholder="Name" name="name">
                        </div>

                        <div class="col-md-6 form-group">
                          <label for="companyName">Detail.</label>
                          <input type="text" required value="<?= $accounts->detail?>" class="form-control" placeholder="Detail" name="detail">
                        </div>
                      </div>
                     
                      
                    </div>

                    <div class="form-actions">
                      <button type="submit" class="btn btn-primary">
                        <i class="la la-check-square-o"></i> Submit
                      </button>
                    </div>
                  </form>
                </div>
              </div>
            </div>


          </div>

        </div>
      </section>

      <!-- // Basic form layout section end -->
    </div>
  </div>
</div>
