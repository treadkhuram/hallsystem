<div class="app-content content">
        <div class="content-wrapper">
            <div class="content-wrapper-before"></div>
            <div class="content-header row">
            </div>
            <div class="content-body">
                <!-- Emails Products & Avg Deals -->
                <div class="row">
                    <div class="col-md-12 col-lg-8">
                        <div class="card">
                            <div class="card-header p-1">
                                <h4 class="card-title float-left">Bookings<span class="blue-grey lighten-2 font-small-3 mb-0"></span></h4>
                                <span class="badge badge-pill badge-info float-right m-0">Total</span>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-footer text-center p-1">
                                    <div class="row">
                                        <div class="col-md-4 col-12 border-right-blue-grey border-right-lighten-5 text-center">
                                            <p class="blue-grey lighten-2 mb-0">Pending</p>
                                            <p class="font-medium-5 text-bold-400"><?= $pending ?></p>
                                        </div>
                                        <div class="col-md-4 col-12 border-right-blue-grey border-right-lighten-5 text-center">
                                            <p class="blue-grey lighten-2 mb-0">Completed</p>
                                            <p class="font-medium-5 text-bold-400"><?= $completed ?></p>
                                        </div>
                                        <div class="col-md-4 col-12 border-right-blue-grey border-right-lighten-5 text-center">
                                            <p class="blue-grey lighten-2 mb-0">Canceled</p>
                                            <p class="font-medium-5 text-bold-400"><?= $canceled ?></p>
                                        </div>
                                    </div>
                                    <hr>
                                    <span class="text-muted"><a href="<?= base_url('Booking/allBookings')?>" class="danger darken-2">Booking</a> Statistics</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-4">
                        <div class="card pull-up border-top-info border-top-3 rounded-0">
                            <div class="card-header">
                                <h4 class="card-title">Today's Bookings<span class="badge badge-pill badge-info float-right m-0"></span></h4>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body p-1">
                                    <h4 class="font-large-1 text-bold-400"><?= $todayBooking ?><i class="ft-grid float-right"></i></h4>
                                </div>
                                <div class="card-footer p-1">
                                    <span class="text-muted"><i class="la la-arrow-circle-o-up info"></i>Booking of the day</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-8">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header p-1">
                                    <h4 class="card-title float-left">Today Cash<span class="blue-grey lighten-2 font-small-3 mb-0"></span></h4>
                                    <span class="badge badge-pill badge-info float-right m-0">Total</span>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-footer text-center p-1">
                                        <div class="row">
                                            <div class="col-md-4 col-12 text-center">
                                                <p class="blue-grey lighten-2 mb-0">Cash Receive</p>
                                                <p class="font-medium-5 text-bold-400"><?= $todayAmountReceive ?></p>
                                            </div>
                                            <div class="col-md-4 col-12 border-right-blue-grey border-right-lighten-5 text-center">
                                                <p class="blue-grey lighten-2 mb-0">Cash Pay</p>
                                                <p class="font-medium-5 text-bold-400"><?= $todayAmountPay ?></p>
                                            </div>
                                            <div class="col-md-4 col-12 border-right-blue-grey border-right-lighten-5 text-center">
                                                <p class="blue-grey lighten-2 mb-0">Balance</p>
                                                <p class="font-medium-5 text-bold-400"><?= $todayAmountReceive-$todayAmountPay ?></p>
                                            </div>
                                        </div>
                                        <hr>
                                        <span class="text-muted"><a href="#" class="danger darken-2">Cash</a> Statistics</span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header p-1">
                                    <h4 class="card-title float-left">Last seven day Cash<span class="blue-grey lighten-2 font-small-3 mb-0"></span></h4>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-footer text-center p-1">
                                        <div class="row">
                                            <div class="col-md-4 col-12 text-center">
                                                <p class="blue-grey lighten-2 mb-0">Cash Receive</p>
                                                <p class="font-medium-5 text-bold-400"><?= $seven_day_amount_receive ?></p>
                                            </div>
                                            <div class="col-md-4 col-12 border-right-blue-grey border-right-lighten-5 text-center">
                                                <p class="blue-grey lighten-2 mb-0">Cash Pay</p>
                                                <p class="font-medium-5 text-bold-400"><?= $seven_day_amount_pay ?></p>
                                            </div>
                                            <div class="col-md-4 col-12 border-right-blue-grey border-right-lighten-5 text-center">
                                                <p class="blue-grey lighten-2 mb-0">Balance</p>
                                                <p class="font-medium-5 text-bold-400"><?= ($seven_day_amount_receive - $seven_day_amount_pay) ?></p>
                                            </div>
                                        </div>
                                        <hr>
                                        <span class="text-muted"><a href="#" class="danger darken-2">Cash</a> Statistics</span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header p-1">
                                    <h4 class="card-title float-left">Current Month Cash<span class="blue-grey lighten-2 font-small-3 mb-0"></span></h4>
                                    <span class="badge badge-pill badge-info float-right m-0">Total</span>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-footer text-center p-1">
                                        <div class="row">
                                            <div class="col-md-4 col-12 text-center">
                                                <p class="blue-grey lighten-2 mb-0">Cash Receive</p>
                                                <p class="font-medium-5 text-bold-400"><?= $month_amount_receive ?></p>
                                            </div>
                                            <div class="col-md-4 col-12 border-right-blue-grey border-right-lighten-5 text-center">
                                                <p class="blue-grey lighten-2 mb-0">Cash Pay</p>
                                                <p class="font-medium-5 text-bold-400"><?= $month_amount_pay ?></p>
                                            </div>
                                            <div class="col-md-4 col-12 border-right-blue-grey border-right-lighten-5 text-center">
                                                <p class="blue-grey lighten-2 mb-0">Balance</p>
                                                <p class="font-medium-5 text-bold-400"><?= ($month_amount_receive - $month_amount_pay) ?></p>
                                            </div>
                                        </div>
                                        <hr>
                                        <span class="text-muted"><a href="#" class="danger darken-2">Cash</a> Statistics</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header p-1">
                                    <h4 class="card-title float-left">Current Month employee Salary<span class="blue-grey lighten-2 font-small-3 mb-0"></span></h4>
                                    <span class="badge badge-pill badge-info float-right m-0">Total</span>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-footer text-center p-1">
                                        <div class="row">
                                            <div class="col-md-4 col-12 text-center">
                                                <p class="blue-grey lighten-2 mb-0">Total Salary</p>
                                                <p class="font-medium-5 text-bold-400"><?= $total_employee_salary ?></p>
                                            </div>
                                            <div class="col-md-4 col-12 border-right-blue-grey border-right-lighten-5 text-center">
                                                <p class="blue-grey lighten-2 mb-0">Paid Salary</p>
                                                <p class="font-medium-5 text-bold-400"><?= $paid_employee_salary ?></p>
                                            </div>
                                            <div class="col-md-4 col-12 border-right-blue-grey border-right-lighten-5 text-center">
                                                <p class="blue-grey lighten-2 mb-0">Remaining Salary</p>
                                                <p class="font-medium-5 text-bold-400"><?= ($total_employee_salary - $paid_employee_salary) ?></p>
                                            </div>
                                        </div>
                                        <hr>
                                        <span class="text-muted"><a href="#" class="danger darken-2">Salary</a> Statistics</span>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-4">
                        <div class="card pull-up border-top-info border-top-3 rounded-0">
                            <div class="card-header">
                                <h4 class="card-title">Personal Accounts<span class="badge badge-pill badge-info float-right m-0"></span></h4>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body p-1">

                                    <?php foreach ($accounts as $k=>$acc){ ?>
                                        <a href="<?= base_url().'Report/accountBalancereport/'.$acc->id ?>" class="btn btn-link"><?= $acc->name ?></a><br>
                                    <?php if($k==9)break; } ?>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>