<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-wrapper-before"></div>

        <div class="content-body">
            <!-- Basic form layout section start -->
            <section id="basic-form-layouts">
                <div class="row match-height">

                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <?php if ($this->session->flashdata('msg')): ?>
                                        <?php echo $this->session->flashdata('msg'); ?>
                                    <?php endif; ?>
                                    <form method="post" class="form"
                                          action="<?php echo base_url(); ?>Vandors/add_supplyData"
                                          enctype="multipart/form-data" accept-charset="ISO-8859-1">
                                        <div class="form-body">
                                            <h4 class="form-section">
                                                <i class="ft-flag"></i>Add Supply
                                            </h4>
                                            <div class="row">

                                                <div class="col-md-12 form-group">
                                                    <label for="companyName">Select Vendor</label>
                                                    <select name="vandor_id" id="vandor_id" class="select2 form-control">
                                                        <option value="">Select Vendor--</option>
                                                        <?php foreach ($allVandors as $vandor) { ?>
                                                            <option value="<?= $vandor->id ?>"><?= $vandor->name ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>

                                                <div class="col-md-6 form-group">
                                                    <label for="">Bill Book No.</label>
                                                    <input type="text" value="" id="" class="form-control"
                                                           placeholder="Bill Book Number"
                                                           name="bill_book_number">
                                                </div>

                                                <div class="col-md-6 form-group">
                                                    <label for="">Date</label>
                                                    <input type="date" value="<?= date('Y-m-d'); ?>" id="date"
                                                           class="form-control" placeholder="Date"
                                                           name="date">
                                                </div>

                                                <div class="col-md-6 form-group">
                                                    <label for="">Select Raw Material</label>
                                                    <select name="" id="prod_select"
                                                            onchange="setVal('<?= base_url() ?>Vandors/selectData')"
                                                            class="form-control">
                                                        <option value="">Select Products</option>
                                                        <?php foreach ($allProducts as $product) { ?>
                                                            <option value="<?= $product->id ?>"><?= $product->name ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                                <input type="hidden" name="product" id="product" value="">

                                                <div class="col-md-6 form-group">
                                                    <label for="">Detail</label>
                                                    <input type="text" value="" id="" class="form-control"
                                                           placeholder="Detail"
                                                           name="detail">
                                                </div>


                                                <div class="col-md-6 form-group">
                                                    <label for="">Price.</label>
                                                    <input type="number" id="price" class="form-control"
                                                           placeholder="Price" onClick="this.select();" value="0"
                                                           name="price">
                                                </div>


                                                <div class="col-md-6 form-group">
                                                    <label for="qty">Available Quantity: </label>
                                                    <input type="number" disabled id="qty" class="form-control"
                                                           placeholder="Available Quantity" id="available_qty" value="0">
                                                </div>
                                                <div class="col-md-6 form-group">
                                                    <label for="add_qty">Add Quantity: </label>
                                                    <input type="number" onClick="this.select();" id="add_qty"
                                                           class="form-control"
                                                           placeholder="Quantity" name="qty" value="0">
                                                </div>

                                                <!-- <div class="col-md-3 form-group">
                                                    <label for="">Total Products.</label>
                                                    <input type="number" disabled id="t_prod" class="form-control"
                                                        placeholder="Total Products" value="0" name="t_products">
                                                </div>
                                                <div class="col-md-3 form-group">
                                                    <label for="">Total Amount.</label>
                                                    <input type="number" disabled id="t_amount" class="form-control"
                                                        placeholder="Total Amount" value="0" name="t_amount">
                                                </div>
                                             -->
                                                <input type="hidden" name="total_amount" id="total_amount">


                                            </div>

                                            <input type="button" class="col-md-1 btn btn-primary float-right"
                                                   value="Add" id="add_prod" onclick="addfunction()" name="add_prod">
                                            <div class="clearfix"></div>
                                            <hr width="90%">

                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="card">
                                                        <div class="card-header">
                                                            <h4 class="card-title">Raw Materials</h4>
                                                            <div class="heading-elements">
                                                                <!-- <a class="btn btn-sm btn-danger box-shadow-2 round btn-min-width pull-right" href="<?= base_url() ?>Vandors/addVandor">Add Vandor</a> -->
                                                            </div>
                                                            <a class="heading-elements-toggle"><i
                                                                        class="la la-ellipsis-v font-medium-3"></i></a>
                                                        </div>
                                                        <div class="card-content collapse show">
                                                            <div class="table-responsive">
                                                                <table id="tbl"
                                                                       class="table table-striped table-bordered">
                                                                    <thead class="bg-primary white">
                                                                    <tr>
                                                                        <th>#</th>
                                                                        <th>Product</th>
                                                                        <th>Quantity</th>
                                                                        <th>Price</th>
                                                                        <th>Actions</th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody id="tbody">

                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                        </div>
                                        <div class="form-actions">
                                            <button type="submit" class="btn btn-primary float-right m-3">
                                                <i class="la la-check-square-o"></i> Submit
                                            </button>
                                        </div>
                                    </form>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </section>


        </div>
    </div>
</div>


<script type="text/javascript">

    function setVal(Url) {
        var prod_select = $('#prod_select').val();
        $.ajax({
            url: Url + '/' + prod_select,
            data: {id: prod_select},
            type: "POST",
            success: function (res) {
                // console.log(res);

                var newA = JSON.parse(res);
                $('#price').val(newA['price']);
                $('#qty').val(newA['qty']);
                $('#product').val(newA['name']);
            }
        });
        //    console.log(prod_select);
    }

    var i = 0;
    var t_qty = 0;
    var t_amnt = 0;

    function addfunction() {
        var prod_id = $('#prod_select').val();
        var product = $('#product').val();
        i++;

        var qty = $('#add_qty').val();
        var price = $('#price').val();

        if($('#vandor_id').val()==''){
            alert('Please select vendor');
            return;
        }else if(prod_id==''){
            alert('Please select product');
            return;
        }else if(qty == '' || qty == 0){
            alert('Please enter qunatity');
            return;
        }else if(price == '' || price == 0){
            alert('Please enter qunatity');
            return;
        }

        var newCell = '<tr><td>' + i + '</td><td>' + product + '</td><td>' + qty + '</td><td>' + price + '</td>' +
            '<td> <a onclick="removeRow(this)" class="a_button"><i class="ft-trash-2 text-danger"></i></a>' +
            '<input type="hidden" value="' + prod_id + '" name="product_id[]"/>' +
            '<input type="hidden" value="' + qty + '" name="product_qty[]"/>' +
            '<input type="hidden" value="' + price + '" name="product_price[]"/>' +
            '</td></tr>';

        $('#tbl').append(newCell);

        t_amnt += parseInt(price) * parseInt(qty);
        $('#t_amount').val(t_amnt);
        $('#total_amount').val(t_amnt);

        $('#add_qty').val('0');
        $('#price').val('0');
        $('#available_qty').val('0');


    }

    // $('#price')..on("click", function () {
    //     $(this).select();
    // });
    // $("input[type='number']").select();
    // $("input").select();
    // $("input[type='number']").on("click", function () {
    //     $(this).select();
    // });
    // $("input[type='number']").on("click", function() {
    //     $(this).select();
    // });


    function removeRow(obj) {
        var r = $(obj).parent().parent();
        r.remove();
        console.log(r);
    }

    window.addEventListener("load", function () {
        console.log('hello');
        $('body').delegate('#delete_row', 'click', function (event) {
            console.log(event);
        });
    });

</script>