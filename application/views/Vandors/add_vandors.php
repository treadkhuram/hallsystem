<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-wrapper-before"></div>

        <div class="content-body">
            <!-- Basic form layout section start -->
            <section id="basic-form-layouts">
                <div class="row match-height">

                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <?php if($this->session->flashdata('msg')):?>
                                    <?php echo $this->session->flashdata('msg');?>
                                    <?php endif;?>
                                    <form method="post" class="form" action="<?php echo base_url();?>Vandors/saveVandor"
                                        enctype="multipart/form-data" accept-charset="ISO-8859-1">
                                        <div class="form-body">
                                            <h4 class="form-section">
                                                <i class="ft-flag"></i>Add New Vandor
                                            </h4>
                                            <div class="row">
                                                <div class="col-md-6 form-group">
                                                    <label for="name">Full Name</label>
                                                    <input type="text" required id="name" class="form-control" placeholder="Name"
                                                        name="name">
                                                </div>

                                                <div class="col-md-6 form-group">
                                                    <label for="phone">Mobile No.</label>
                                                    <input type="number" required id="phone" class="form-control"
                                                        placeholder="Mobile No" name="phone">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6 form-group">
                                                    <label for="city">City</label>
                                                    <input type="text" required id="city" class="form-control" placeholder="City"
                                                        name="city">
                                                </div>

                                                <div class="col-md-6 form-group">
                                                    <label for="address">Address</label>
                                                    <input type="text" id="address" class="form-control"
                                                        placeholder="Address" name="address">
                                                </div>
                                            </div>

                                        </div>

                                        <div class="form-actions">
                                            <button type="submit" class="btn btn-primary">
                                                <i class="la la-check-square-o"></i> Submit
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>


                    </div>

                </div>
            </section>

            <!-- // Basic form layout section end -->
        </div>
    </div>
</div>