<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="author" content="ThemeSelect">
    <title>Hall System</title>
    <link rel="apple-touch-icon" href="<?php echo base_url(); ?>assets/logo.png">
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url(); ?>assets/logo.png">
    <link href="https://fonts.googleapis.com/css?family=Muli:300,300i,400,400i,600,600i,700,700i%7CComfortaa:300,400,700" rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/app-assets/vendors/css/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/app-assets/vendors/css/charts/chartist.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/app-assets/vendors/css/charts/chartist-plugin-tooltip.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/app-assets/vendors/css/tables/datatable/datatables.min.css">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/app-assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/app-assets/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/app-assets/css/colors.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/app-assets/css/components.css">
    <!-- END: Theme CSS-->

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/app-assets/css/core/menu/menu-types/vertical-menu.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/app-assets/css/core/colors/palette-gradient.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/app-assets/css/core/colors/palette-gradient.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/app-assets/css/pages/chat-application.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/app-assets/css/pages/dashboard-analytics.css">
    <!-- END: Page CSS-->

    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/app-assets/vendors/css/forms/selects/select2.min.css">

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/style.css">
    <!-- END: Custom CSS-->
<style>
.navbar-header .navbar-brand .brand-logo {
    width: 149px;
}
</style>
</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu 2-columns   fixed-navbar" data-open="click" data-menu="vertical-menu" data-color="bg-blue" data-col="2-columns">

    <!-- BEGIN: Header-->
    <nav class="header-navbar navbar-expand-md navbar navbar-with-menu navbar-without-dd-arrow fixed-top navbar-semi-light">
        <div class="navbar-wrapper">
            <div class="navbar-container content">
                <div class="collapse navbar-collapse show" id="navbar-mobile">
                    <ul class="nav navbar-nav mr-auto float-left">
                        <li class="nav-item mobile-menu d-md-none mr-auto"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu font-large-1"></i></a></li>
                        <li class="nav-item d-none d-md-block"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu"></i></a></li>
                        <li class="nav-item d-none d-md-block"><a class="nav-link nav-link-expand" href="#"><i class="ficon ft-maximize"></i></a></li>
                    
                      
                    </ul>
                    <ul class="nav navbar-nav float-right">
                        
                        <li class="dropdown dropdown-user nav-item"><a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown"> <span class="avatar avatar-online"><img src="<?php echo base_url(); ?>assets/uploaded_images/default_user.png" alt="avatar"></span></a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <div class="arrow_box_right"><a class="dropdown-item" href="<?= base_url().'setting/updateProfile' ?>"><span class="avatar avatar-online"><span class="user-name text-bold-700 ml-1">Admin</span></span></a>
                                    <div class="dropdown-divider"></div><a class="dropdown-item" href="<?php echo base_url()?>login/logout"><i class="ft-power"></i> Logout</a>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
    <!-- END: Header-->


    <!-- BEGIN: Main Menu-->
    <div class="main-menu menu-fixed menu-light menu-accordion    menu-shadow " data-scroll-to-active="true" data-img="<?= base_url()?>assets/app-assets/images/backgrounds/02.jpg">
        <div class="navbar-header">
            <ul class="nav navbar-nav flex-row">
                <li class="nav-item mr-auto"><a class="navbar-brand" href="<?= base_url('home');?>">
                    <!-- <img class="brand-logo" alt="admin" height="90" src="<?= base_url() ?>assets/logo.png"> -->
                        <h3 class="brand-text">Ashrif MIS</h3>
                    </a></li>
                <li class="nav-item d-md-none"><a class="nav-link close-navbar"><i class="ft-x"></i></a></li>
            </ul>
        </div>
        <div class="navigation-background"></div>
        <div class="main-menu-content">
            <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
                <li class="nav-item <?= $active=='dashboard' ? 'open' : '' ?>"><a href="<?= base_url('home');?>"><i class="ft-home"></i>
                  <span class="menu-title" data-i18n=""><?= $this->lang->line('dashboard') ?> </span></a>
                </li>
                <li class="has-sub nav-item <?= $active=='Vandors' ? 'open' : '' ?>"><a>
                 <i class="ft-users"></i><span class="menu-title" data-i18n="">Vendors </span></a>
                  <ul class="menu-content">
                      <li class=""><a class="menu-item" href="<?= base_url('Vandors/allVandors')?>">>> View Vendors </a></li>
                      <li class=""><a class="menu-item" href="<?= base_url('Vandors/addSupply')?>">>> Add Supply </a></li>
                      <li class=""><a class="menu-item" href="<?= base_url('Vandors/viewSupply')?>">>> Vendor Ledger</a></li>
                      <li class=""><a class="menu-item" href="<?= base_url('Report/vendorBalance')?>">>> All Vendor Balance </a></li>
                  </ul>
                </li>            
                <li class="has-sub nav-item <?= $active=='Raw_Materials' ? 'open' : '' ?>"><a>
                 <i class="ft-briefcase"></i><span class="menu-title" data-i18n="">Raw_Materials </span></a>
                  <ul class="menu-content">
                      <li class=""><a class="menu-item" href="<?= base_url('Raw_Material/addRawMaterial')?>">>> Add Raw_Materials </a></li>
                      <li class=""><a class="menu-item" href="<?= base_url('Raw_Material/allRawMaterials')?>">>> View Raw Materials </a></li>
                  </ul>
                </li>            
                <li class="has-sub nav-item <?= $active=='Dishes' ? 'open' : '' ?>"><a>
                 <i class="ft-clipboard"></i><span class="menu-title" data-i18n="">Dishes </span></a>
                  <ul class="menu-content">
                      <li class=""><a class="menu-item" href="<?= base_url('Dishes/addDishes')?>">>> Add Dishes </a></li>
                      <li class=""><a class="menu-item" href="<?= base_url('Dishes/allDishes')?>">>> View Dishes </a></li>
                      <li class=""><a class="menu-item" href="<?= base_url('Dishes/view_menu')?>">>> Menu </a></li>
                      <li class=""><a class="menu-item" href="<?= base_url('Dishes/menu_rates')?>">>> Menu Rates </a></li>
                  </ul>
                </li>
                <li class="has-sub nav-item <?= $active=='Booking' ? 'open' : '' ?>"><a>
                 <i class="ft-book"></i><span class="menu-title" data-i18n="">Booking </span></a>
                  <ul class="menu-content">
                      <li class=""><a class="menu-item" href="<?= base_url('Booking/addBooking')?>">>> Add Booking </a></li>
                      <li class=""><a class="menu-item" href="<?= base_url('Booking/allBookings')?>">>> View Bookings </a></li>
                      <li class=""><a class="menu-item" href="<?= base_url('Report/bookingReport')?>">>> Booking Report </a></li>
                      <li class=""><a class="menu-item" href="<?= base_url('Report/pendingBookingPayments')?>">>> Pending Booking Payments </a></li>
                  </ul>
                </li>

                <li class="has-sub nav-item <?= $active=='Cash' ? 'open' : '' ?>"><a>
                 <i class="ft-credit-card"></i><span class="menu-title" data-i18n="">Cash </span></a>
                  <ul class="menu-content">
                      <li class=""><a class="menu-item" href="<?= base_url('Cash/addCash')?>">>> Add Cash </a></li>
                      <li class=""><a class="menu-item" href="<?= base_url('Cash/viewCash')?>">>> View Cash </a></li>
                      <li class=""><a class="menu-item" href="<?= base_url('Report/dailyCashRegister')?>">>> Cash Register </a></li>
                  </ul>
                </li>
                <li class="has-sub nav-item <?= $active=='Personal Account' ? 'open' : '' ?>"><a>
                        <i class="ft-pie-chart"></i><span class="menu-title" data-i18n="">Personal Account </span></a>
                    <ul class="menu-content">
                        <li class=""><a class="menu-item" href="<?= base_url('PersonalAccount/personal_account')?>">>> Accounts </a></li>
                        <li class=""><a class="menu-item" href="<?= base_url('Report/accountBalancereport')?>">>> Account Balance Report </a></li>
                        <li class=""><a class="menu-item" href="<?= base_url('Report/totalAccountBalance')?>">>> All Account Balance </a></li>
                    </ul>
                </li>
                <li class="has-sub nav-item <?= $active=='Employees' ? 'open' : '' ?>"><a>
                 <i class="ft-users"></i><span class="menu-title" data-i18n="">Employees </span></a>
                  <ul class="menu-content">
                       <li class=""><a class="menu-item" href="<?= base_url('Employees/Employees')?>">>> View Employee </a></li>
                       <li class=""><a class="menu-item" href="<?= base_url('Employees/employeesDetail')?>">>> Salary Detail </a></li>
                      <li class=""><a class="menu-item" href="<?= base_url('Report/allEmployeeSalerySheet ')?>">>> Salary Sheet </a></li>
                  </ul>
                </li>
                <li hidden class="has-sub nav-item <?= $active=='Reports' ? 'open' : '' ?>"><a>
                 <i class="ft-pie-chart"></i><span class="menu-title" data-i18n="">Reports </span></a>
                  <ul class="menu-content">
                      <li class=""><a class="menu-item" href="<?= base_url('Report/dailyCashRegister')?>">>> Cash Register </a></li>
                      <li class=""><a class="menu-item" href="<?= base_url('Report/accountBalancereport')?>">>> Account Balance Report </a></li>
                      <li class=""><a class="menu-item" href="<?= base_url('Report/bookingReport')?>">>> Booking Report </a></li>
                      <li class=""><a class="menu-item" href="<?= base_url('Report/vendorBalance')?>">>> Vendor Balance </a></li>
                      <li class=""><a class="menu-item" href="<?= base_url('Report/totalAccountBalance')?>">>> Total Account Balance </a></li>
                      <li class=""><a class="menu-item" href="<?= base_url('Report/pendingBookingPayments')?>">>> Pending Booking Payments </a></li>
                      <li class=""><a class="menu-item" href="<?= base_url('Report/allEmployeeSalerySheet ')?>">>> Employees Salary Sheet </a></li>
                  </ul>
                </li>

                <li class="has-sub nav-item <?= $active=='Setting' ? 'open' : '' ?>"><a>
                 <i class="ft-settings"></i><span class="menu-title" data-i18n="">Settings </span></a>
                    <ul class="menu-content">
                        <ul class="menu-content">
                            <li class=""><a class="menu-item" href="<?= base_url('Unit/allUnit')?>">>> Units</a></li>
                            <li hidden class=""><a class="menu-item" href="<?= base_url('Setting/cashInHand')?>">>> Cash in hand</a></li>
                            <li class=""><a class="menu-item" href="<?= base_url('Setting/bookingAccount')?>">>> Booking Account</a></li>
                        </ul>

                    </ul>
                </li

            </ul>    
          
        </div>
     
    </div>


<?php
    $this->load->view($view);
  ?>




    
    <footer class="footer footer-static footer-light navbar-border navbar-shadow">
        <div class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2"><span class="float-md-left d-block d-md-inline-block">2020 &copy; Copyright <a class="text-bold-800 grey darken-2" href="https://sentaxlab.com" target="_blank">Sentax Lab</a></span>
        </div>
    </footer>
    <!-- END: Footer-->


    <!-- BEGIN: Vendor JS-->
    <script src="<?php echo base_url(); ?>assets/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="<?php echo base_url(); ?>assets/app-assets/vendors/js/charts/chartist.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/app-assets/vendors/js/charts/chartist-plugin-tooltip.min.js" type="text/javascript"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="<?php echo base_url(); ?>assets/app-assets/js/core/app-menu.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/app-assets/js/core/app.js" type="text/javascript"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src="<?php echo base_url(); ?>assets/app-assets/js/scripts/pages/dashboard-analytics.js" type="text/javascript"></script>
    <!-- END: Page JS-->
<!--    <script src="--><?php //echo base_url(); ?><!--assets/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>-->
    <script src="<?php echo base_url(); ?>assets/app-assets/js/scripts/tables/datatables/datatable-basic.js" type="text/javascript"></script>


    <!-- <script src="<?php echo base_url('assets/');?>vendor/jquery-2.1.4.min.js"></script> -->
    <script src="<?php echo base_url()?>assets/print_this/printThis.js"></script>
    <script src="<?php echo base_url(); ?>assets/app-assets/vendors/js/forms/select/select2.full.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/app-assets/js/scripts/forms/select/form-select2.js" type="text/javascript"></script>

    <script>
        function mySearchFunction(obj, tableId, index) {
            var input, filter, table, tr, td, i;
            input = document.getElementById(obj.id);
            filter = input.value.toUpperCase();
            table = document.getElementById(tableId);
            tr = table.getElementsByTagName("tr");
            for (i = 0; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td")[index];
                if (td) {
                    if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                    } else {
                        tr[i].style.display = "none";
                    }
                }
            }
        }
    </script>



</body>
</html>