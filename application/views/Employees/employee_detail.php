<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-wrapper-before"></div>

        <div class="content-body">
            <!-- Basic form layout section start -->
            <section id="basic-form-layouts">
                <div class="row match-height">

                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <?php if ($this->session->flashdata('msg')): ?>
                                        <?php echo $this->session->flashdata('msg'); ?>
                                    <?php endif; ?>
                                    <form method="post" class="form"
                                          action="<?php echo base_url(); ?>Employees/employeesDetail"
                                          enctype="multipart/form-data" accept-charset="ISO-8859-1">
                                        <div class="form-body">
                                            <h4 class="form-section">
                                                <i class="ft-flag"></i>Employee Cash
                                            </h4>
                                            <div class="row">


                                                <div class="col-md-3 form-group">
                                                    <label for="">Select Employee</label>
                                                    <select name="employee_id" id="employee_id"
                                                            class="select2 form-control" required>
                                                        <option value="">Select Employee</option>
                                                        <?php foreach ($employees as $emp) { ?>
                                                            <option <?= isset($employee_id)? ($employee_id == $emp->id ? 'selected' : "") : "" ?>
                                                                    value="<?= $emp->id ?>"><?= $emp->name ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>

                                                <div class="col-md-3 form-group">
                                                    <label for="">Start Date.</label>
                                                    <input type="date" class="form-control" required
                                                           placeholder="Start Date"
                                                           value="<?= isset($start_date) ? $start_date : date('Y-m-01') ?>"
                                                           name="start_date">
                                                </div>

                                                <div class="col-md-3 form-group">
                                                    <label for="">End Date.</label>
                                                    <input type="date" class="form-control" required
                                                           placeholder="End Date"
                                                           value="<?= isset($end_Date) ? $end_Date: date('Y-m-d') ?>"
                                                           name="end_date">
                                                </div>

                                                <div class="col-md-1"></div>
                                                <div class="col-md-2">
                                                    <button type="submit" name="filter" class="btn btn-primary m-1">
                                                        <i class="la la-check-square-o"></i> Filter
                                                    </button>
                                                </div>
                                            </div>


                                        </div>

                                    </form>

                                </div>


                                <?php if (!empty($employeeData)) { ?>
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="card">
                                                <div class="card-header">
                                                    <h4 class="card-title">Employee Cash Detail</h4>
                                                    <div class="heading-elements">

                                                            <h4 class="card-title">Total Salary
                                                                : <?= $employee->salary ?></h4>
                                                    </div>
                                                    <a class="heading-elements-toggle"><i
                                                                class="la la-ellipsis-v font-medium-3"></i></a>
                                                </div>
                                                <div class="card-content collapse show">
                                                    <div class="table-responsive">
                                                        <table id="tbl" class="table">
                                                            <thead class="bg-primary white">
                                                            <tr>
                                                                <th>#</th>
                                                                <th>Date</th>
                                                                <th>Employee Name</th>
                                                                <th>Amount</th>
                                                                <th>Detail</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody id="tbody">

                                                            <?php $i = 0;;
                                                            foreach ($employeeData as $data) {
                                                                $i++; ?>
                                                                <tr>
                                                                    <td><?= $i ?></td>
                                                                    <td><?= parse_date($data->date) ?></td>
                                                                    <td><?= $data->name ?></td>
                                                                    <td><?= $data->total_amount ?></td>
                                                                    <td><?= $data->detail ?></td>
                                                                </tr>


                                                                <?php
                                                            }


                                                            ?>

                                                            </tbody>
                                                        </table>
                                                    </div>


                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>


                            </div>
                        </div>
                    </div>
                </div>
            </section>


        </div>
    </div>
</div>

<!--
<script type="text/javascript">

    function setVal(Url) {
        var prod_select = $('#prod_select').val();
        $.ajax({
            url: Url+'/'+prod_select,
            data: {id:prod_select},
            type: "POST",
           success: function(res){
            // console.log(res);

            var newA = JSON.parse(res);
            $('#price').val(newA['price']);
            $('#qty').val(newA['qty']);
            $('#product').val(newA['name']);
           }
       }); 
    //    console.log(prod_select);
    }

</script> -->