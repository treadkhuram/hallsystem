<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-wrapper-before"></div>

        <div class="content-body">
            <!-- Basic form layout section start -->
            <section id="basic-form-layouts">
                <div class="row match-height">

                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <?php if ($this->session->flashdata('msg')): ?>
                                        <?php echo $this->session->flashdata('msg'); ?>
                                    <?php endif; ?>
                                    <form method="post" class="form"
                                          action="<?php echo base_url(); ?>Employees/saveEmployees"
                                          enctype="multipart/form-data" accept-charset="ISO-8859-1">
                                        <div class="form-body">
                                            <h4 class="form-section">
                                                <i class="ft-flag"></i>Add Employees
                                            </h4>
                                            <div class="row">
                                                <div class="col-md-6 form-group">
                                                    <label for="name">Name</label>
                                                    <input type="text" id="name" class="form-control" placeholder="Name"
                                                           required name="name">
                                                </div>

                                                <div class="col-md-6 form-group">
                                                    <label for="phone">Mobile No.</label>
                                                    <input type="number" id="phone" class="form-control" required
                                                           placeholder="Mobile No" name="phone">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6 form-group">
                                                    <label for="city">City</label>
                                                    <input type="text" id="city" required class="form-control" placeholder="City"
                                                           name="city">
                                                </div>

                                                <div class="col-md-6 form-group">
                                                    <label for="address">Address</label>
                                                    <input type="text" id="address" class="form-control"
                                                           placeholder="Address" name="address">
                                                </div>
                                            </div>

                                        </div>

                                        <div class="row">
                                            <div class="col-md-6 form-group">
                                                <label for="city">Designation</label>
                                                <input type="text" id="city" class="form-control"
                                                       placeholder="Designation" required
                                                       name="designation">
                                            </div>

                                            <div class="col-md-6 form-group">
                                                <label for="address">Salary</label>
                                                <input type="number" id="address" class="form-control" required
                                                       placeholder="Salary" name="salary">
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6 form-group">
                                                <label for="city">Allow Holiday</label>
                                                <input type="number" id="city" class="form-control"
                                                       placeholder="Allow Holiday"
                                                       value="0" onclick="this.select()"
                                                       name="holiday">
                                            </div>

                                            <div class="col-md-6 form-group">
                                                <label for="address">Fine Per Day</label>
                                                <input type="number" id="address" class="form-control"
                                                       value="0" onclick="this.select()"
                                                       placeholder="Fine Per Day" name="fine">
                                            </div>
                                        </div>

                                        <div class="form-actions">
                                            <button type="submit" class="btn btn-primary">
                                                <i class="la la-check-square-o"></i> Submit
                                            </button>
                                        </div>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>


                </div>

        </div>
        </section>

        <!-- // Basic form layout section end -->
    </div>
</div>