<div class="app-content content">
   <div class="content-wrapper">
      <div class="content-wrapper-before"></div>
      <div class="content-body">
         <!-- Basic form layout section start -->
         <section id="basic-form-layouts">
            <div class="row match-height">
               <div class="offset-md-1 col-md-10">
                  <div class="card">
                     <div class="card-content collapse show">
                        <div class="card-body">
                           <?php if($this->session->flashdata('msg')):?>
                           <?php echo $this->session->flashdata('msg');?>
                           <?php endif;?>
                           <form method="post" class="form"
                              action="<?php echo base_url();?>Booking/saveBooking"
                              enctype="multipart/form-data" accept-charset="ISO-8859-1">
                              <div class="form-body">
                                 <h4 class="form-section">
                                    <i class="ft-flag"></i>Add New Booking
                                 </h4>
                                 <div class="row">
                                    <div class="col-md-6 form-group">
                                       <label for="booking_date">Booking Date: </label>
                                       <input type="date" required id="booking_date"
                                          value="<?= date('Y-m-d') ?>" class="form-control"
                                          placeholder="Booking Date" name="booking_date">
                                    </div>
                                     <div class="col-md-6 form-group">
                                         <label for="customer_name">Serial No</label>
                                         <input type="text" required id="serial_no" class="form-control"
                                                placeholder="Serial No" name="serial_no">
                                     </div>
                                    <div class="col-md-6 form-group">
                                       <label for="customer_name">Customer Name</label>
                                       <input type="text" required id="customer_name" class="form-control"
                                          placeholder="Customer Name" name="customer_name">
                                    </div>
                                    <div class="col-md-6 form-group">
                                       <label for="function_date">Function Date:</label>
                                       <input type="date" required id="function_date" class="form-control"
                                          placeholder="Function Date" name="function_date">
                                    </div>
                                    <div class="col-md-6 form-group">
                                       <label for="phone">Phone No.</label>
                                       <input type="text" required id="phone" class="form-control"
                                          placeholder="Phone No." name="phone">
                                    </div>
                                    <div class="col-md-3 form-group">
                                       <label for="fun_st_time">Function Starting Time.</label>
                                       <input type="time" value="12:00" required id="fun_st_time" class="form-control"
                                          placeholder="Function Start Time" name="fun_st_time">
                                    </div>
                                    <div class="col-md-3 form-group">
                                       <label for="fun_end_time">Function Ending Time.</label>
                                       <input type="time" value="15:00" required id="fun_end_time" class="form-control"
                                          placeholder="Function Ending Time." name="fun_end_time">
                                    </div>
                                    <div class="col-md-6 form-group">
                                       <label for="hall">Hall </label>
                                       <select name="hall" required class="form-control">
                                          <option disabled> Select Hall</option>
                                          <option value="1">Hall 1</option>
                                          <option value="2">Hall 2</option>
                                          <option value="3">Hall 3</option>
                                       </select>
                                    </div>
                                     <div class="col-md-6 form-group">
                                         <label for="hall_charges">Hall Charges.</label>
                                         <input type="number" required id="hall_charges" class="form-control"
                                                placeholder="hall Charges" value="0" onclick="this.select()" name="hall_charges">
                                     </div>
                                     <div class="col-md-6 form-group">
                                         <label for="hall_charges">Function Labour Cost.</label>
                                         <input type="number" required id="labour_cost" class="form-control"
                                                placeholder="Labour cost" value="0" onclick="this.select()" name="labour_cost">
                                     </div>
                                     <div class="col-md-6 form-group">
                                         <label for="function_tax">Function Tax in %</label>
                                         <input type="number" required id="function_tax" class="form-control"
                                                value="0" onclick="this.select()" name="function_tax">
                                     </div>
                                    <div class="col-md-12 form-group">
                                       <label for="function_name">Function Name.</label>
                                       <input type="text" required id="function_name" class="form-control"
                                          placeholder="function_name." name="function_name">
                                    </div>
                                    <div class="col-md-6 form-group">
                                       <label for="booking_persons">Booking Persons.</label>
                                       <input type="number" required id="booking_persons"
                                          class="form-control" value="0" onclick="this.select()" placeholder="Booking Persons."
                                          name="booking_persons">
                                    </div>
                                    <div class="col-md-6 form-group">
                                       <label for="confirm_persons">Confirm Persons.</label>
                                       <input type="number" id="confirm_persons" class="form-control"
                                          placeholder="Confirm Persons." name="confirm_persons">
                                    </div>
                                    <div class="col-md-6 form-group">
                                       <label for="more_advance">Per Person Rate.</label>
                                       <input type="number" requires value="0" onclick="this.select()" id="per_person_rate" class="form-control"
                                          placeholder="Per Person Rate." name="per_person">
                                    </div>
                                    <div class="col-md-6 form-group">
                                       <label for="booking_advance">Booking Advance.</label>
                                       <input type="number" required id="booking_advance"
                                          class="form-control" placeholder="Booking Advance."
                                          name="booking_advance">
                                    </div>
                                    <div class="col-md-12 form-group">
                                       <label for="note">Note.</label>
                                       <textarea name="notes" id="note" cols="30" rows="5"
                                          class="form-control" placeholder="Note"></textarea>
                                    </div>
                                    <div class="col-md-12 form-group">
                                       <label for="">Menu</label>
                                       <select class="select2 form-control" name="function_menu" onchange="addfunction()" id="menu_id">
                                          <option value="">Select Menu...</option>
                                          <?php foreach($menus as $m){ ?>
                                          <option value="<?=$m->id?>"><?= $m->name?></option>
                                          <?php }?>
                                       </select>
                                    </div>
                                 </div>
                                 <div class="border p-2" style="display:none" id="div_rates">
                                    <div hidden class="row">
                                        <div class="col-md-3 form-group">
                                        <label id="lbl_menu_rate">Menu rate:</label>
                                        </div>
                                        <div class="col-md-4 form-group">
                                        <label id="menu_rate"></label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3 form-group">
                                        <label >Booking rate:</label>
                                        </div>
                                        <div class="col-md-4 form-group">
                                        <label id="booking_per_person"></label>
                                        </div>
                                    </div>
                                     <div class="row">
                                         <div class="col-md-3 form-group">
                                             <label >Tax:</label>
                                             <input type="hidden" id="txt_function_tax_amount" name="txt_function_tax_amount">
                                         </div>
                                         <div class="col-md-3 form-group">
                                             <label id="lbl_tax"></label>
                                         </div>
                                     </div>
                                     <div class="row">
                                         <div class="col-md-3 form-group">
                                             <label >Booking Total Amount:</label>
                                         </div>
                                         <div class="col-md-4 form-group">
                                             <label id="booking_total_amount"></label>
                                         </div>
                                     </div>
                                    <div class="row">
                                        <div class="col-md-3 form-group">
                                        <label >Menu total cost:</label>
                                        </div>
                                        <div class="col-md-3 form-group">
                                        <label id="menu_cost"></label>
                                        </div>
                                    </div>
                                     <div class="row">
                                         <div class="col-md-3 form-group">
                                             <label >Hall Charges:</label>
                                         </div>
                                         <div class="col-md-3 form-group">
                                             <label id="lbl_hall_charges"></label>
                                         </div>
                                     </div>
                                     <div class="row">
                                         <div class="col-md-3 form-group">
                                             <label >Function Labour Cost:</label>
                                         </div>
                                         <div class="col-md-3 form-group">
                                             <label id="lbl_labour_cost"></label>
                                         </div>
                                     </div>
                                     <div class="row">
                                         <div class="col-md-3 form-group">
                                             <label >Total function Cost:</label>
                                         </div>
                                         <div class="col-md-3 form-group">
                                             <label id="lbl_function_cost"></label>
                                             <input type="hidden" id="txt_function_cost" name="txt_function_cost">
                                         </div>
                                     </div>
                                     <div class="row">
                                         <div class="col-md-3 form-group">
                                             <label class="text-danger" >Single person cost:</label>
                                         </div>
                                         <div class="col-md-3 form-group">
                                             <label id="per_person_cost"></label>
                                         </div>
                                     </div>
                                    <div class="row">
                                        <div class="col-md-3 form-group">
                                        <label>Profit:</label>
                                        </div>
                                        <div class="col-md-3 form-group">
                                        <label id="total_profit"></label>
                                        </div>
                                    </div>
                                 </div>
                                 <div style="display:none" id="div_add_more_dish" class="row mt-2">
                                    <div class="col-md-6 form-group">
                                        <label for="phone">Add More Dishes</label>
                                        <select id="select_more_dish" class="form-control">
                                            <option value="">Select dish</option>
                                            <?php foreach($dishes as $dish){ ?>
                                            <option value="<?= $dish->id ?>" ><?= $dish->name ?></option>
                                            <?php } ?>
                                            </select>
                                        </div>
                                    <div class="col-md-6 mt-3 form-group">
                                        <a onclick="addMoreDish()" id="btn_add_more">Add</a>
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-12">
                                    <div class="card">
                                       <div class="card-header">
                                          <h4 class="card-title">Menu Dishes</h4>
                                          <div class="heading-elements">
                                              <a onclick="showAddMoreDish()" style="" id="btn_add_more_dish" class="btn btn-sm btn-danger text-white" >Add More</a>
                                          </div>
                                          <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                       </div>
                                       <div style="height: 450px; overflow-y: scroll;" class="card-content collapse show">
                                          <div class="table-responsive">
                                             <table id="tbl" class="table table-striped table-bordered">
                                                <thead class="bg-primary white">
                                                   <tr>
                                                      <th>#</th>
                                                      <th>Dishes</th>
                                                      <th>Cost</th>
                                                      <th>Raw Material</th>
                                                      <th>Actions</th>
                                                   </tr>
                                                </thead>
                                             </table>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="form-actions">
                              <button type="submit" class="btn btn-primary float-right">
                              <i class="la la-check-square-o"></i> Submit
                              </button>
                              <div class="clearfix"></div>
                              </div>
                           </form>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <!-- // Basic form layout section end -->
      </div>
   </div>
</div>
<script type="text/javascript">

   function showAddMoreDish(){
      $('#div_add_more_dish').show();
      $('#btn_add_more_dish').hide();
   }

   function addMoreDish(){
      var dish = $('#select_more_dish').val();
      var totalPerson = $('#booking_persons').val();
      var perPersonRate = $('#per_person_rate').val();
      $.ajax({
         url: "<?= base_url()?>Booking/getSingleDishMenu",
         data: {dish_id:dish, total_person:totalPerson},
         method: "POST",
         success: function(res){
             console.log(res);
             var obj = JSON.parse(res);
            $('#tbl_menu_dishes').append(obj['table']);

             var menuCost = parseInt($('#menu_cost').html());
             var singlePersonCost = parseInt($('#per_person_cost').html());
             var totalCost = parseInt($('#lbl_function_cost').html());
             menuCost = menuCost+parseInt(obj['dish_cost']);

             $('#menu_cost').html(menuCost);
             calculateCharges();
             alert("Dish Added successfully.");
         }
      });
   }

   function calculateCharges(){
       var hallCharges = $('#hall_charges').val();
       var labourCost = $('#labour_cost').val();
       var tax = $('#function_tax').val();
       var bookingPersonRate = parseInt($('#per_person_rate').val());

       var totalMenuCost = parseInt($('#menu_cost').html());
       var totalPerson = $('#booking_persons').val();

       if(hallCharges =='')
           hallCharges =0;
       if(labourCost =='')
           labourCost = 0;
       if(tax == '')
           tax=0;
       hallCharges = parseInt(hallCharges);
       labourCost = parseInt(labourCost);
       tax = parseInt(tax);

       $('#lbl_hall_charges').html(hallCharges);
       $('#lbl_labour_cost').html(labourCost);


       totalMenuCost = totalMenuCost + hallCharges + labourCost;

       var totalBookingAmount = parseInt(bookingPersonRate*totalPerson);

       var taxAmount = tax/100 * totalBookingAmount;

       // include txt in cost
       // var totalCost = Math.ceil(totalMenuCost + taxAmount);
       // var personCost = Math.ceil(totalCost/totalPerson);

       var totalCost = Math.ceil(totalMenuCost);
       var personCost = Math.ceil(totalMenuCost/totalPerson);
       console.log(totalMenuCost);

       $('#lbl_tax').html(tax+' % = '+Math.ceil(taxAmount));
       totalBookingAmount = totalBookingAmount + taxAmount;
       $('#booking_total_amount').html(totalBookingAmount);

       $('#txt_function_tax_amount').val(Math.ceil(taxAmount));

       $('#lbl_function_cost').html(totalCost);
       $('#txt_function_cost').val(totalCost);
       $('#per_person_cost').html(personCost);
       $('#total_profit').html(totalBookingAmount-totalCost);

   }

   var i = 0;
   function addfunction(){
       // console.log('Hello');
       i++;
       if($('#dish_id').val() == ''){
           alert('select Menu');
       }else{
           var menuData ='';
           var menu_id   = $('#menu_id').val();
           var menu_text = $('#menu_id option:selected').text();
           var totalPerson = $('#booking_persons').val();
           var perPersonRate = $('#per_person_rate').val();

           if(totalPerson == '' || totalPerson == 0)
               alert('Enter total persons.');
           else if(perPersonRate == '')
               alert('Enter person rate.');
           $.ajax({
               url: "<?= base_url()?>Booking/getMenuDishes",
               data: {id:menu_id, total_persons: totalPerson},
               method: "POST",
               success: function(res){
                   // console.log(res);
                   $("#tbl tbody").remove();
                   var obj = JSON.parse(res);
                   $('#tbl').append(obj['table_body']);
                   $('#div_rates').show();
                   var totalMenuCost = parseInt(obj['total_cost']);
                   var menuRate = obj['menu_rate'];
                   $('#menu_rate').html('('+menuRate+'), '+menuRate+' X '+totalPerson+' = '+ menuRate*totalPerson );
                   $('#booking_per_person').html('('+perPersonRate+'), '+perPersonRate+' X '+totalPerson+' = '+ totalPerson*perPersonRate );

                   $('#menu_cost').html(totalMenuCost);
                   calculateCharges();

               }
           });         
       }
   }
   
   
   function removeRow(obj){
       var r = $(obj).parent().parent();
       r.remove();
       console.log(r);
   }


   function handleChange(e, cost) {
       const {checked} = e.target;
       console.log(cost);
       var menuCost = parseInt($('#menu_cost').html());
       var totalCost = parseInt($('#lbl_function_cost').html());
       var singlePersonCost = $('#per_person_cost').html();
       var person = $('#booking_persons').val();
       var personCost = parseInt(cost/parseInt(person));
       if(checked == false){
           menuCost = menuCost - cost;
           totalCost = totalCost - cost;
           singlePersonCost = parseInt(singlePersonCost) - personCost;
       }else if(checked == true){
           menuCost = menuCost + cost;
           totalCost = totalCost + cost;
           singlePersonCost = parseInt(singlePersonCost) + personCost;
       }
       // console.log(menuCost);
       // $('#per_person_cost').html(singlePersonCost);
       $('#menu_cost').html(menuCost);
       calculateCharges();
       // $('#lbl_function_cost').html(totalCost);
       //
       // var totalPerson = $('#booking_persons').val();
       // var perPersonRate = $('#per_person_rate').val();
       //
       // $('#total_profit').html(parseInt((perPersonRate*totalPerson)- totalCost));
   }

   window.addEventListener("load", function(){
       $('body').delegate('#delete_row', 'click', function(event){
           console.log(event);
       });

   });
   
</script>