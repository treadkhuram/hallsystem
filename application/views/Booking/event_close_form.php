<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-wrapper-before"></div>
        <div class="content-body">
            <!-- Basic form layout section start -->
            <section id="basic-form-layouts">
                <div class="row match-height">
                    <div class="offset-md-1 col-md-10">
                        <div class="card">
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <?php if ($this->session->flashdata('msg')): ?>
                                        <?php echo $this->session->flashdata('msg'); ?>
                                    <?php endif; ?>
                                    <form method="post" class="form"
                                          action="<?php echo base_url(); ?>Booking/saveEventCloseForm"
                                          enctype="multipart/form-data" accept-charset="ISO-8859-1">
                                        <div class="form-body">
                                            <h4 class="form-section">
                                                <i class="ft-flag"></i>Event Closing Form
                                            </h4>
                                            <input type="hidden" name="booking_id" value="<?= $bookingData->id ?>">
                                            <div class="row">
                                                <div class="col-md-12 form-group">
                                                    <label for="">Event Date: </label>
                                                    <input type="date" required id="" class="form-control"
                                                           value="<?= $bookingData->function_date ?>"
                                                           placeholder="Function Date"
                                                           name="function_date">
                                                </div>
                                                <div class="col-md-6 form-group">
                                                    <label for="customer_name">Customer Name</label>
                                                    <input type="text" required id="customer_name" class="form-control"
                                                           placeholder="Customer Name"
                                                           value="<?= $bookingData->customer_name ?>"
                                                           name="customer_name">
                                                </div>
                                                <div class="col-md-6 form-group">
                                                    <label for="">Function Name.</label>
                                                    <input type="text" required id="" class="form-control"
                                                           placeholder="Function Name"
                                                           value="<?= $bookingData->function_name ?>"
                                                           name="function_name">
                                                </div>
                                                <div class="col-md-12 form-group">
                                                    <label for="">Total booking Persons.</label>
                                                    <input type="number" required class="form-control"
                                                           placeholder="Total Persons." onkeyup="caltotal()"
                                                           value="<?= $bookingData->booking_persons ?>"
                                                           disabled name="total_booking_persons">
                                                </div>
                                                <div class="col-md-6 form-group">
                                                    <label for="">Total Confirm Persons.</label>
                                                    <input type="number" required id="t_persons" class="form-control"
                                                           placeholder="Total Persons." onkeyup="caltotal()"
                                                           value="<?= $bookingData->confirm_persons ?>"
                                                           name="total_persons">
                                                </div>
                                                <div class="col-md-6 form-group">
                                                    <label for="">Per/Person Rate.</label>
                                                    <input type="number" required id="p_person" class="form-control"
                                                           placeholder="Per Persons."
                                                           value="<?= $bookingData->per_person_rate ?>"
                                                           onkeyup="caltotal()"
                                                           name="per_person_rate">
                                                </div>
                                            </div>
                                            <hr width="90%"/>
                                            <h2 class="col-md-12">Extras Charges</h2>
                                            <a onclick="addExtraItems()" class="btn btn-light text-white btn-sm"><u>Add
                                                    Extras Charges</u></a>
                                            <div class="row border p-2" style="display:none" id="div_extra">
                                                <div class="col-md-2 form-group">
                                                </div>
                                                <div class="clearfix"></div>
                                                <script>
                                                    var imageCounter = 0;

                                                    function addExtraItems() {
                                                        $('#btnCalculate').show();
                                                        $('#div_extra').show();
                                                        imageCounter++;
                                                        $('#div_extra').append(
                                                            '<div id="pro_div' + imageCounter + '" class="col-md-12 p-0 m-0">' +
                                                            '<div class="row p-1">' +
                                                            '<div class="col-md-4 form-group">' +
                                                            '<label for="">Product Name.' + imageCounter + '</label>' +
                                                            '<input type="text" id="" class="form-control"' +
                                                            'placeholder="Name of Things." name="new_product_name[]">' +
                                                            '</div>' +
                                                            '<div class="col-md-3 form-group">' +
                                                            '<label for="">Qty.</label>' +
                                                            '<input type="number" id="" class="form-control"' +
                                                            'placeholder="Quantity." name="new_product_qty[]">' +
                                                            '</div>' +
                                                            '<div class="col-md-3 form-group">' +
                                                            '<label for="">Price.</label>' +
                                                            '<input type="number" id="" class="form-control"' +
                                                            'placeholder="Price of Things." name="new_product_price[]">' +
                                                            '</div>' + '<a onclick="removeProductImage(' + imageCounter + ')" class="float-right">' +
                                                            '<i class="text-danger ft-trash-2"></i></a>' +
                                                            '</div>' + '</div>'
                                                        );
                                                    }

                                                    function removeProductImage(id) {
                                                        $('#pro_div' + imageCounter).remove();
                                                        imageCounter--;
                                                        calculateExtraCharges();
                                                    }
                                                </script>
                                            </div>
                                        </div>
                                        <input type="hidden" name="total_extra_charges" value="0"
                                               id="total_extra_charges">
                                        <input type="button" id="btnCalculate" style="display:none"
                                               onclick="calculateExtraCharges()" value="Calculate"
                                               class="btn btn-primary btn-sm float-right">
                                        <div class="clearfix"></div>
                                        <div class="row">
                                            <div class="mt-2 col-md-6">
                                                <div class="form-group">
                                                    <label for="">Total Function Amount: </label>
                                                    <input type="number" readonly class="form-control"
                                                           placeholder="Sub Total" value="<?= $functionAmount ?>"
                                                           id="function_amount" name="function_amount">
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Advance Amount Received: </label>
                                                    <input type="number" disabled class="form-control"
                                                           placeholder="Advance Amount"
                                                           value="<?= $amountReceived ?>" name="advance_amount">
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Sub Total: </label>
                                                    <input type="number" disabled id="sub_total" class="form-control"
                                                           placeholder="Sub Total" value="<?= $subTotal ?>"
                                                           name="sub_total">
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Discount: </label>
                                                    <input type="number" onClick="this.select();" onkeyup="caltotal()"
                                                           id="discount" class="form-control"
                                                           value="0" name="discount">
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Tax: <?= $bookingData->hall_tax ?> %(while booking)</label>
                                                    <input type="number" onkeyup="caltotal()" id="tax"
                                                           class="form-control"
                                                           onClick="this.select();" value="<?= $bookingData->tax ?>" name="tax">
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Receive Amount: </label>
                                                    <input type="number" onClick="this.select();" onkeyup="caltotal()"
                                                           id="received_amount"
                                                           class="form-control" placeholder="Received Amount" value="0"
                                                           name="received_amount">
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Balance: </label>
                                                    <input type="number" id="total" class="form-control"
                                                           value="<?= $subTotal +$bookingData->tax ?>" disabled>
                                                    <input type="hidden" id="hidden_total" class="form-control"
                                                           value="0"
                                                           name="total">
                                                </div>
                                            </div>
                                            <div class="mt-2 col-md-6">
                                                <div class="form-group">
                                                    <label for="">Total Function Cost: </label>
                                                    <input type="number" disabled class="form-control"
                                                           placeholder="Advance Amount"
                                                           value="<?= $bookingData->function_cost ?>"
                                                           name="advance_amount">
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Hall Charges: </label>
                                                    <input type="number" disabled class="form-control"
                                                           placeholder="Advance Amount"
                                                           value="<?= $bookingData->hall_charges ?>"
                                                           name="advance_amount">
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Hall Labour Cost: </label>
                                                    <input type="number" disabled class="form-control"
                                                           placeholder="Advance Amount"
                                                           value="<?= $bookingData->hall_labour_cost ?>"
                                                           name="advance_amount">
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Cost/Person: </label>
                                                    <input type="number" disabled class="form-control"
                                                           placeholder="Advance Amount"
                                                           value="<?= ceil($bookingData->function_cost/$bookingData->booking_persons) ?>"
                                                           name="advance_amount">
                                                </div>
                                            </div>
                                        </div>
                                </div>
                                <div class="form-actions">
                                    <button type="submit" class="btn btn-primary float-right m-3">
                                        <i class="la la-check-square-o"></i> Submit
                                    </button>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
        </section>
        <!-- // Basic form layout section end -->
    </div>
</div>
</div>
<script type="text/javascript">

    function calculateExtraCharges() {
        var p1 = document.getElementsByName("new_product_name[]");
        var q1 = document.getElementsByName("new_product_qty[]");
        var pp = document.getElementsByName("new_product_price[]");
        for (var x = 0; x < p1.length; x++) {
            if (p1[x].value == null || p1[x].value == "") {
                alert("Enter Product Name");
                return false;
            }
        }
        for (var x = 0; x < q1.length; x++) {
            if (q1[x].value == null || q1[x].value == "") {
                alert("Enter Product Qty");
                return false;
            }
        }
        for (var x = 0; x < p1.length; x++) {
            if (pp[x].value == null || pp[x].value == "") {
                alert("Enter Product Price");
                return false;
            }
        }
        var totalAmount = 0;
        for (var i = 0; i < p1.length; i++) {
            var qty = parseInt(q1[i].value);
            var price = parseInt(pp[i].value);
            totalAmount += qty * price;
        }
        console.log(totalAmount);
        $('#total_extra_charges').val(totalAmount);
        caltotal();
    }

    function myFunction() {

    }

    function caltotal() {
        var total_persons = $('#t_persons').val();
        var per_persons = $('#p_person').val();
        var totalAmount = per_persons * total_persons;
        var extraAmount = $('#total_extra_charges').val();
        totalAmount = totalAmount + parseInt(extraAmount);
        $('#function_amount').val(totalAmount);

        var totalAmount = totalAmount - <?= $amountReceived ?>;
        $('#sub_total').val(totalAmount);

        var sub_total = $('#sub_total').val();
        var received_amount = $('#received_amount').val();
        var discount = $('#discount').val();
        var tax = $('#tax').val();
        discount = discount == '' ? 0 : discount;
        tax = tax == '' ? 0 : tax;
        received_amount = received_amount == '' ? 0 : received_amount;
        var total = parseInt(sub_total) - parseInt(discount) - parseInt(received_amount);

        var g_total = total + parseInt(tax);
        console.log(g_total);
        $('#total').val(g_total);
        $('#hidden_total').val(g_total);

    }

</script>