<style>
    #my_map {
        height: 250px;
        width: 100%;
    }
    /* .table th,
    .table td {
        padding: 1rem 0.2rem;
    } */

</style>
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-wrapper-before"></div>
        <div class="content-header row">
            <div class="content-header-left col-md-4 col-12 mb-2">
                <h3 class="content-header-title">Order Detail</h3>
            </div>
        </div>
        <div class="content-body">
            <section>
                <div class="row">
                    <div class="col-12 col-md-4">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Customer Detail</h4>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0 display-block">
                                        <li>
                                            <a class="btn btn-md  round btn-min-width pull-right" href="#"
                                               target="_blank">
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body pt-0 pb-1">
                                    <div class="table-responsive">
                                        <table id="tbl" class="table table-bordered">
                                            <tbody>
                                            <tr>
                                                <td>Name</td>
                                                <td><?php echo $booking_detail->customer_name ?></td>
                                            </tr>

                                            <tr>
                                                <td>Phone</td>
                                                <td><?php echo $booking_detail->phone ?></td>
                                            </tr>
                                            <tr>
                                                <td style="vertical-align: middle;"><b>Booking Persons</b></td>
                                                <td><?php echo $booking_detail->booking_persons ?></td>
                                            </tr>
                                            <tr>
                                                <td class="text-truncate pb-0">
                                                    <b> Confirm person</b>
                                                </td>
                                                <td><?php echo $booking_detail->confirm_persons	?></td>
                                            </tr>
                                            <tr>
                                                <td><b>Rate/person</b></td>
                                                <td><?php echo $booking_detail->per_person_rate?></td>
                                            </tr>
                                            <tr>
                                                <td>Function amount</td>
                                                <td><?php echo $booking_detail->function_amount?></td>
                                            </tr>
                                            <tr>
                                                <td>Discount</td>
                                                <td><?php echo $booking_detail->discount?></td>
                                            </tr>
                                            <tr>
                                                <td>Tax</td>
                                                <td><?php echo $booking_detail->tax?></td>
                                            </tr>
                                            <tr>
                                                <td>Net Total</td>
                                                <td><?php echo $booking_detail->net_total?></td>
                                            </tr>
                                            <tr>
                                                <td>Received Amount</td>
                                                <td><?php echo $booking_detail->total_Received_amount?></td>
                                            </tr>
                                            <tr>
                                                <td>Balance</td>
                                                <td><?php echo $booking_detail->balance?></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 text-center text-md-left">
                                                <div class="card-header">
                                                    <h4 class="card-title">Notes</h4>
                                                    <div class="heading-elements">
                                                        <ul class="list-inline mb-0 display-block">
                                                            <li>
                                                                <a class="btn btn-md  round btn-min-width pull-right" href="#"
                                                                   target="_blank">
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <p><?php echo $booking_detail->notes?></p>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-8">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Order Detail <?php ?></h4>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0 display-block">
                                        <li>
                                            <a class="btn btn-md btn-outline-danger ml-1 box-shadow-2 round btn-min-width pull-right"
                                               href="<?= base_url()?>booking/printBooking/<?= $booking_detail->id?>">Print</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body pt-0 pb-1">
                                    <!-- <div class="mt-3 col-md-12" id="my_map"></div> -->
                                    <div class="mt-3 table-responsive">
                                        <table id="tbl" class="table">
                                            <thead>
                                            <tr>
                                                <th>Function Name</th>
                                                <th>Function Date</th>
                                                <th>Function Time</th>
                                                <th>Hall</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td> <?php echo $booking_detail->function_name ?></td>
                                                <td><?php echo $booking_detail->function_date ?></td>
                                                <td><?php echo $booking_detail->fun_st_time. '-' .$booking_detail->fun_end_time  ?></td>
                                                <td><?php echo $booking_detail->hall ?></td>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="mt-3 table-responsive">
                                        <h4>Booking transactions</h4>
                                        <table id="tbl" class="table">
                                            <thead>
                                            <tr>
                                                <th>Date</th>
                                                <th>Detail</th>
                                                <th>Amount</th>
                                                <th>Referance</th>
                                                <th>Function</th>


                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php foreach($booking_transaction as $key){?>
                                                <tr>
                                                    <td><?php echo $key->date?></td>
                                                    <td><?php echo $key->detail  ?></td>
                                                    <td><?php echo $key->amount ?></td>
                                                    <td><?php echo $key->referance ?></td>
                                                    <td>
                                                        <a href="<?= base_url().'Booking/deleteTransaction/'.$key->id.'/'.$booking_detail->id ?>" class="a_button"><i class="ft-trash-2 text-danger"></i></a>
                                                    </td>
                                                </tr>
                                            <?php }?>
                                            </tbody>
                                        </table>
                                    </div>

                                    <div>

                                        <form method="post" class="form" action="<?php echo base_url();?>booking/saveCancelEvent" enctype="multipart/form-data" accept-charset="ISO-8859-1">
                                            <div class="form-body">
                                                <h4 class="form-section">
                                                    <i class="ft-flag"></i>Return Detail</h4>
                                                <input type="hidden" value="<?= $booking_detail->customer_name ?>" name="booking_person_name">
                                                <input type="hidden" name="booking_id" value="<?= $booking_detail->id ?>">
                                                <div class="row">
                                                    <div class="col-md-12 form-group">
                                                        <label for="companyName">Booking Return Amount</label>
                                                        <input type="number" id="txt_return_amount" class="form-control"
                                                               value="0" onclick="this.select()" placeholder="Name"
                                                               name="txt_return_amount" required>
                                                    </div>

                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12 form-group">
                                                        <label for="companyName">Booking Cancel Reason</label>
                                                        <textarea class="form-control" name="txt_detail" rows="3"></textarea>
                                                    </div>

                                                </div>

                                            </div>

                                            <div class="form-actions">
                                                <button type="submit" class="btn btn-primary">
                                                    <i class="la la-check-square-o"></i> Submit
                                                </button>
                                            </div>
                                        </form>

                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>



