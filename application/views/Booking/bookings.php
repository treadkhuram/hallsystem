<div class="app-content content">
   <div class="content-wrapper">
      <div class="content-wrapper-before"></div>
      <div class="content-body">
         <section id="basic-tabs-components">
            <div class="row match-height">
               <div class="col-xl-12 col-lg-12">
                  <div class="card">
                  <div class="card-header">
                              <h4 class="card-title">All Bookings</h4>
                              <div class="heading-elements">
                                 <a class="btn btn-sm btn-danger box-shadow-2 round btn-min-width pull-right"
                                    href="<?= base_url() ?>Booking/addBooking">Add Booking</a>
                              </div>
                              <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                           </div>
                           <div class="card-content collapse show">
                              <?php if($this->session->flashdata('msg')):?>
                              <?php echo $this->session->flashdata('msg');?>
                              <?php endif;?>
                              <div class="table-responsive">
                                 
                              </div>
                           </div>
                     <div class="card-content">
                        <div class="card-body">
                           <ul class="nav nav-tabs">
                              <li class="nav-item">
                                 <a style=" border: solid;border-color: #d4d0cf;border-width: 1px;"
                                    class="nav-link <?= $activeTab=='active'?'active':'' ?>" id="base-tab1" data-toggle="tab" aria-controls="newOrder"
                                    href="#newOrder" aria-expanded="true">Active Bookings
                                 <span class="badge badge badge-info badge-pill float-right"><?= count($pendingBooking) ?></span></a>
                              </li>
                              <li class="nav-item">
                                 <a style=" border: solid;border-color: #d4d0cf;border-width: 1px;"
                                    class="nav-link <?= $activeTab=='complete'?'active':'' ?>" id="base-tab3" data-toggle="tab" aria-controls="onHold"
                                    href="#onHold" aria-expanded="false">Completed</a>
                              </li>
                              <li class="nav-item">
                                 <a style=" border: solid;border-color: #d4d0cf;border-width: 1px;"
                                    class="nav-link" id="base-tab2" data-toggle="tab" aria-controls="underProcess" 
                                    href="#underProcess" aria-expanded="false">Canceled</a>
                              </li>
                           </ul>
                           <div class="tab-content px-1 pt-1">
                              <div role="tabpanel" class="tab-pane <?= $activeTab=='active'?'active':'' ?>" id="newOrder" aria-expanded="true" aria-labelledby="base-tab1">
                                 <div class="table-responsive">
                                     <input type="text" id="search_input" onkeyup="mySearchFunction2(this, 'tbl_active', 1)" class="form-control col-md-2 m-1" placeholder="Search..."">
                                    <table id="tbl_active" class="table table-striped table-bordered zero-configuration">
                                        <thead class="bg-primary white">
                                          <tr>
                                              <th>#</th>
                                              <th>Serial No</th>
                                              <th>Customer Name</th>
                                              <th>Phone</th>
                                              <th>Function</th>
                                              <th>Date</th>
                                              <th>Time</th>
                                              <th>Persons</th>
                                              <th>Actions</th>
                                          </tr>
                                        </thead>
                                        <tbody>
                                          <?php foreach($pendingBooking as $key=>$ac){ ?>
                                          <tr>
                                              <td><?= $key+1 ?></td>
                                              <td><?= $ac->serial_no ?></td>
                                              <td><?= $ac->customer_name ?></td>
                                              <td><?= $ac->phone ?></td>
                                              <td><?= $ac->function_name ?></td>
                                              <td><?= parse_date($ac->function_date) ?></td>
                                              <td><?= $ac->fun_st_time ?></td>
                                              <td><?= $ac->booking_persons ?></td>
                                              <td>

                                              <div class="btn-group mr-1 mb-1">
                                                   <button class="btn btn-danger btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                         Action
                                                   </button>
                                                   <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                         <a href="<?= base_url().'Booking/view_booking/'.$ac->id?>" class="dropdown-item">View</a>
                                                         <a href="<?= base_url().'Booking/updateBooking/'.$ac->id ?>" class="dropdown-item">Edit</a>
                                                         <a href="#" onclick="openCashModel(<?= $ac->id ?>)" class="dropdown-item">Add Amount</a>
                                                         <a href="<?= base_url()?>Booking/eventCloseForm/<?= $ac->id?>" class="dropdown-item">Close Booking</a>
                                                         <a href="<?= base_url()?>Booking/cancelEvent/<?= $ac->id?>" class="dropdown-item">Cancel Booking</a>
                                                         <a href="#" onclick="deleteCustomer('<?= base_url().'Booking/deleteBooking/'.$ac->id ?>')"class="a_button dropdown-item">Delete</a>
                                                   </div>
                                                </div>
                                             </td>
                                          </tr>
                                          <?php } ?>
                                        </tbody>
                                    </table>
                                 </div>
                              </div>
                              <div class="tab-pane <?= $activeTab=='complete'?'active':'' ?>" id="onHold" aria-labelledby="base-tab3">
                                  <form method="post" class="form" action="<?= base_url();?>Booking/allBookings"
                                        enctype="multipart/form-data" ">
                                      <div class="row">
                                          <div class="col-md-5 form-group">
                                              <label for="">Start Date.</label>
                                              <input type="date"  class="form-control" required placeholder="Start Date"
                                                     value="<?= isset($start_date) ? $start_date : date('Y-01-01'); ?>"
                                                     name="start_date">
                                          </div>

                                          <div class="col-md-5 form-group">
                                              <label for="">End Date.</label>
                                              <input type="date"  class="form-control" required placeholder="End Date"
                                                     value="<?= isset($end_date) ? $end_date : date('Y-m-d', time()); ?>"
                                                     name="end_date">
                                          </div>

                                          <div class="col-md-2 mt-1">
                                              <button type="submit" name="filter" class="btn btn-primary m-1">
                                                  <i class="la la-check-square-o"></i> Filter </button>
                                          </div>
                                      </div>
                                  </form>

                                  <div class="table-responsive">
                                 <table id="tbl" class="table table-striped table-bordered zero-configuration">
                                        <thead class="bg-primary white">
                                          <tr>
                                              <th>#</th>
                                              <th>Serial No</th>
                                              <th>Customer Name</th>
                                              <th>Phone</th>
                                              <th>Function Name</th>
                                              <th>F. Date</th>
                                              <th>Time</th>
                                              <th>Persons</th>
                                              <th>Actions</th>
                                          </tr>
                                        </thead>
                                        <tbody>
                                          <?php foreach($completeBooking as $key=>$ac){ ?>
                                          <tr>
                                              <td><?= $key+1 ?></td>
                                              <td><?= $ac->serial_no ?></td>
                                              <td><?= $ac->customer_name ?></td>
                                              <td><?= $ac->phone ?></td>
                                              <td><?= $ac->function_name ?></td>
                                              <td><?= parse_date($ac->function_date) ?></td>
                                              <td><?= $ac->fun_st_time ?></td>
                                              <td><?= $ac->booking_persons ?></td>
                                              <td>

                                              <div class="btn-group mr-1 mb-1">
                                                   <button class="btn btn-danger btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                         Action
                                                   </button>
                                                   <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                         <a href="<?= base_url().'Booking/view_booking/'.$ac->id?>" class="dropdown-item">View</a>
                                                         <a href="#" onclick="openCashModel(<?= $ac->id ?>)" class="dropdown-item">Add Amount</a>
                                                         <a href="#" onclick="deleteCustomer('<?= base_url().'Booking/deleteBooking/'.$ac->id ?>')"class="a_button dropdown-item">Delete</a>
                                                   </div>
                                                </div>
                                             </td>
                                          </tr>
                                          <?php } ?>
                                        </tbody>
                                    </table>
                                 </div>
                              </div>
                              <div class="tab-pane" id="underProcess" aria-labelledby="underProcess">
                                 <div class="table-responsive">
                                     <input type="text" id="search_input2" onkeyup="mySearchFunction2(this, 'tbl_cancel', 1)" class="form-control col-md-2 m-1" placeholder="Search..."">
                                 <table id="tbl_cancel" class="table table-striped table-bordered zero-configuration">
                                        <thead class="bg-primary white">
                                          <tr>
                                              <th>#</th>
                                              <th>Serial No</th>
                                              <th>Customer Name</th>
                                              <th>Phone</th>
                                              <th>Function Name</th>
                                              <th>Date</th>
                                              <th>Time</th>
                                              <th>Persons</th>
                                              <th>Actions</th>
                                          </tr>
                                        </thead>
                                        <tbody>
                                          <?php foreach($cancelBooking as $key=>$ac){ ?>
                                          <tr>
                                              <td><?= $key+1 ?></td>
                                              <td><?= $ac->serial_no ?></td>
                                              <td><?= $ac->customer_name ?></td>
                                              <td><?= $ac->phone ?></td>
                                              <td><?= $ac->function_name ?></td>
                                              <td><?= parse_date($ac->function_date) ?></td>
                                              <td><?= $ac->fun_st_time ?></td>
                                              <td><?= $ac->booking_persons ?></td>
                                              <td>

                                              <div class="btn-group mr-1 mb-1">
                                                   <button class="btn btn-danger btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                         Action
                                                   </button>
                                                   <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                         <a href="<?= base_url().'Booking/view_booking/'.$ac->id?>" class="dropdown-item">View</a>
                                                         <a href="#" onclick="deleteCustomer('<?= base_url().'Booking/deleteBooking/'.$ac->id ?>')"class="a_button dropdown-item">Delete</a>
                                                   </div>
                                                </div>
                                             </td>
                                          </tr>
                                          <?php } ?>
                                        </tbody>
                                    </table>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         
      </div>
   </div>
</div>

<div class="modal fade text-left show" id="cashModel" tabindex="-1" role="dialog" 
aria-labelledby="myModalLabel35" aria-modal="true" style="padding-right: 16px;">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h3 class="modal-title" id="myModalLabel35"> Add Cash</h3>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
            </button>
         </div>
         <form action="<?= base_url()?>Booking/addBookingAmount" method="post">
            <div class="modal-body">
               <input type="hidden" name="booking_id" id="booking_id" >
               <fieldset class="form-group floating-label-form-group">
                  <label for="">Reference: </label>
                  <input type="text" class="form-control" required id="referance" placeholder="Reference" name="referance">
               </fieldset>
               <br>
               <fieldset class="form-group floating-label-form-group">
                  <label for="">Amount:</label>
                  <input type="number" name="amount" required id="amount" class="form-control" id="" placeholder="Amount">
               </fieldset>
               <br>
               <fieldset class="form-group floating-label-form-group">
                  <label for="">Detail: </label>
                  <textarea name="detail" class="form-control" id="detail" rows="3" placeholder="Description"></textarea>
               </fieldset>
            </div>
            <div class="modal-footer">
               <input type="reset" class="btn btn-secondary " data-dismiss="modal" value="close">
               <input type="submit" name="submit" class="btn btn-primary " value="Submit">
            </div>
         </form>
      </div>
   </div>
</div>

<script>

function openCashModel(id){
   $('#booking_id').val(id);
   $('#cashModel').modal('show');
}

   function deleteCustomer(url) {
     var r = confirm("Are you sure, You wants to delete ?");
     if (r == true) {
       window.open(url, "_self");
     }
   }
</script>

<script>
    function mySearchFunction2(obj, tableId, index) {
        var input, filter, table, tr, td, td2, i;
        input = document.getElementById(obj.id);
        filter = input.value.toUpperCase();
        table = document.getElementById(tableId);
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[index];
            td2 = tr[i].getElementsByTagName("td")[index+1];
            if (td) {
                if (td.innerHTML.toUpperCase().indexOf(filter) > -1 || td2.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }
</script>
