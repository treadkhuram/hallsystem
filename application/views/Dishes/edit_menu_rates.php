<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-wrapper-before"></div>

        <div class="content-body">
            <!-- Basic form layout section start -->
            <section id="basic-form-layouts">
                <div class="row match-height">

                    <div class="offset-md-1 col-md-10">
                        <div class="card">
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <?php if($this->session->flashdata('msg')):?>
                                    <?php echo $this->session->flashdata('msg');?>
                                    <?php endif;?>
                                    <form method="post" class="form" action="<?php echo base_url();?>Dishes/updateMenuRates"
                                        enctype="multipart/form-data" accept-charset="ISO-8859-1">
                                        <div class="form-body">
                                            <h4 class="form-section">
                                                <i class="ft-flag"></i>Add Rate List
                                            </h4>
                                            
                                          
                                            <div class="row" id="row">
                                                <div class="col-md-10 mx-auto form-group">
                                                    <label for="">Select Menu</label>
                                                    <select name="menu_id" required id="menu_id" class="select2 form-control">
                                                            <option value="">Select Any Menu</option>
                                                        <?php foreach($Menu as $raw){ ?>
                                                            <option value="<?= $raw->id ?>" <?php if($oldData->menu_id == $raw->id){echo 'selected';} ?>><?= $raw->name ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>

                                                <div class="col-md-4 form-group">
                                                    <div class="pt-2 text-center"><h5> 100 Persons </h5></div>
                                                </div>

                                                <div class="col-md-6 form-group">
                                                    <label for="">Rate</label>
                                                    <input type="number" required id="" class="form-control"
                                                        placeholder="Price for 100 Persons" value="<?= $oldData->p100?>" name="p100">
                                                </div><br>
                                                <div class="col-md-4 form-group">
                                                    <div class="pt-2 text-center"><h5> 200 Persons </h5></div>
                                                </div>

                                                <div class="col-md-6 form-group">
                                                    <label for="">Rate</label>
                                                    <input type="number" required id="" class="form-control"
                                                        placeholder="Price for 200 Persons" value="<?= $oldData->p200?>" name="p200">
                                                </div><br>
                                                <div class="col-md-4 form-group">
                                                    <div class="pt-2 text-center"><h5> 300 Persons </h5></div>
                                                </div>

                                                <div class="col-md-6 form-group">
                                                    <label for="">Rate</label>
                                                    <input type="number" required id="" class="form-control"
                                                        placeholder="Price for 300 Persons" value="<?= $oldData->p300?>" name="p300">
                                                </div><br>
                                                <div class="col-md-4 form-group">
                                                    <div class="pt-2 text-center"><h5> 400 Persons </h5></div>
                                                </div>

                                                <div class="col-md-6 form-group">
                                                    <label for="">Rate</label>
                                                    <input type="number" required id="" class="form-control"
                                                        placeholder="Price for 400 Persons" value="<?= $oldData->p400?>" name="p400">
                                                </div><br>
                                                <div class="col-md-4 form-group">
                                                    <div class="pt-2 text-center"><h5> 500 Persons </h5></div>
                                                </div>

                                                <div class="col-md-6 form-group">
                                                    <label for="">Rate</label>
                                                    <input type="number" required id="" class="form-control"
                                                        placeholder="Price for 500 Persons" value="<?= $oldData->p500?>" name="p500">
                                                </div><br>
                                                <div class="col-md-4 form-group">
                                                    <div class="pt-2 text-center"><h5> 600 Persons </h5></div>
                                                </div>

                                                <div class="col-md-6 form-group">
                                                    <label for="">Rate</label>
                                                    <input type="number" required id="" class="form-control"
                                                        placeholder="Price for 600 Persons" value="<?= $oldData->p600?>" name="p600">
                                                </div><br>
                                                <div class="col-md-4 form-group">
                                                    <div class="pt-2 text-center"><h5> 700 Persons </h5></div>
                                                </div>

                                                <div class="col-md-6 form-group">
                                                    <label for="">Rate</label>
                                                    <input type="number" required id="" class="form-control"
                                                        placeholder="Price for 700 Persons" value="<?= $oldData->p700?>" name="p700">
                                                </div><br>

                                                <input type="hidden" name="update_id" value="<?= $oldData->id ?>">

                                                <hr width="90%">
                                                <div>
                                                   <a href="#submit" class="btn btn-primary btn-sm float-right text-white"
                                                        onclick="appendRow()">Add More Rate</a>
                                                </div>
                                                <div class="clearfix"></div>

                                                <?php if($oldData->extra_rate){
                                                    $extra = json_decode($oldData->extra_rate);
                                                    foreach($extra as $ex){?>
                                                       
                                                       <div class="row col-md-12">
                                                       <div class="col-md-1 form-group"></div>
                                                        <div class="col-md-2 form-group">
                                                        <div class="">
                                                        <label> Persons </label>
                                                        <input type="number" required id="" class="form-control" placeholder="Persons" value="<?= $ex->persons?>" name="persons[]">
                                                        </div>
                                                        </div>
                                                        <div class="col-md-1 form-group"></div>
                                                        <div class="col-md-6 form-group">
                                                        <label for="">Rate</label>
                                                        <input type="number" required id="" class="form-control" placeholder="Price" value="<?= $ex->rate?>" name="rate[]">
                                                        </div>
                                                        <div class="col-md-2 form-group"><a href="#submit" onclick="removeRow(this)" class="btn pt-3">
                                                        <i class="ft-trash-2 text-danger"></i></a>
                                                        </div>
                                                        </div><br>

                                                    <?php }
                                                    }else{
                                                        echo '';
                                                    }
                                                    ?>

                                            </div>
                                          
                                        </div>

                                        <div class="form-actions">
                                            <button type="submit" class="btn btn-primary float-right">
                                                <i class="la la-check-square-o"></i> Submit
                                            </button>
                                            <div class="clearfix"></div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>


                    </div>

                </div>
            </section>

            <!-- // Basic form layout section end -->
        </div>
    </div>
</div>




<script>

    function appendRow() {
        console.log('Hello');
        $('#row').append(
            '<div class="row col-md-12">' +
            '<div class="col-md-1 form-group"></div>' +
            '<div class="col-md-2 form-group">' +
            '<div class="">' +
            '<label> Persons </label>' +
            '<input type="number" required id="" class="form-control" placeholder="Number of Persons" name="persons[]">' +
            '</div>' +
            '</div>' +
            '<div class="col-md-1 form-group"></div>' +
            '<div class="col-md-6 form-group">' +
            '<label for="">Rate</label>' +
            '<input type="number" required id="" class="form-control" placeholder="Price for Persons" name="rate[]">' +
            '</div>' +
            '<div class="col-md-2 form-group"><a href="#submit" onclick="removeRow(this)" class="btn pt-3">' +
            '<i class="ft-trash-2 text-danger"></i></a>' +
            '</div>' +
            '</div><br>'
        );
    }

    function removeRow(obj) {
        var r = $(obj).parent().parent();
        r.remove();
        console.log(r);
    }

    window.addEventListener("load", function () {
        $('body').delegate('#delete_row', 'click', function (event) {
            console.log(event);
        });
    });


</script>