<div class="app-content content">
  <div class="content-wrapper">
    <div class="content-wrapper-before"></div>

    <div class="content-body">
      <!-- Basic form layout section start -->
      <section id="basic-form-layouts">
        <div class="row match-height">


          <div class="col-md-12">

            <div class="row">
              <div class="col-12">
                <div class="card">
                  <div class="card-header">
                    <h4 class="card-title">All Menu Rates</h4>
                    <div class="heading-elements">
                        <a class="btn btn-sm btn-danger box-shadow-2 round btn-min-width pull-right" href="<?= base_url() ?>Dishes/addMenuRates">Add Menu Rate</a>
                    </div>
                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                  </div>
                  <div class="card-content collapse show">
                  <?php if($this->session->flashdata('msg')):?>
                    <?php echo $this->session->flashdata('msg');?>
                  <?php endif;?>
                    <div class="table-responsive">
                      <table id="tbl" class="table table-striped table-bordered zero-configuration">
                        <thead class="bg-primary white">
                          <tr>
                            <th>#</th>
                            <th>Menu</th>
                            <th>P-100</th>
                            <th>P-200</th>
                            <th>P-300</th>
                            <th>P-400</th>
                            <th>P-500</th>
                            <th>P-600</th>
                            <th>P-700</th>
                            <th>Extra</th>
                            <th>Actions</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php $i = 0;
                                foreach($menuRates as $menuRates){ 
                                    $i++; ?>
                            <tr>
                              <td><?= $i?></td>
                              <td><?= $menuRates->menu_name?></td>
                              <td><?= $menuRates->p100?></td>
                              <td><?= $menuRates->p200?></td>
                              <td><?= $menuRates->p300?></td>
                              <td><?= $menuRates->p400?></td>
                              <td><?= $menuRates->p500?></td>
                              <td><?= $menuRates->p600?></td>
                              <td><?= $menuRates->p700?></td>
                              <td><?php if($menuRates->extra_rate){
                                $extra = json_decode($menuRates->extra_rate);
                                foreach($extra as $ex){
                                  echo $ex->persons.' p-> '.$ex->rate.'<br>';
                                }}else{
                                  echo '';
                                }
                              ?>
                                  
                              </td>
                              <td>
                              <!-- <a href="<?= base_url().'Dishes/viewRecipe/'.$ac->id?>"><i class="ft-eye text-info"></i></i></a> -->
                              <a href="<?= base_url().'Dishes/editMenuRates/'.$menuRates->id ?>"><i class="ft-edit text-primary"></i></i></a>
                              <!-- <a onclick="deleteCustomer('<?= base_url().'Dishes/deleteDishes/'.$ac->id ?>')" class="a_button"><i class="ft-trash-2 text-danger"></i></a> -->
                              </td>
                            </tr>
                            <?php }?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  </div>
</div>

<script>
function deleteCustomer(url){
      var r = confirm("Are you sure, You wants to delete ?");
      if (r == true) {
        window.open(url,"_self");
      }
    }
</script>


