<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-wrapper-before"></div>

        <div class="content-body">
            <!-- Basic form layout section start -->
            <section id="basic-form-layouts">
                <div class="row match-height">

                    <div class="offset-md-1 col-md-10">
                        <div class="card">
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <?php if($this->session->flashdata('msg')):?>
                                    <?php echo $this->session->flashdata('msg');?>
                                    <?php endif;?>
                                    <form method="post" class="form"
                                        action="<?php echo base_url();?>Dishes/saveMenuRates"
                                        enctype="multipart/form-data" accept-charset="ISO-8859-1">
                                        <div class="form-body">
                                            <h4 class="form-section">
                                                <i class="ft-flag"></i>Add Rate List
                                            </h4>

                                            <div class="row" id="row">

                                                <div class="col-md-10 mx-auto form-group">
                                                    <label for="">Select Menu</label>
                                                    <select name="menu_id" required id="menu_id"
                                                        class="select2 form-control">
                                                        <option value="">Select Any Menu</option>
                                                        <?php foreach($Menues as $raw){ ?>
                                                        <option value="<?= $raw->id ?>">
                                                            <?= $raw->name ?>
                                                        </option>
                                                        <?php } ?>
                                                    </select>
                                                </div>

                                                <div class="col-md-4 form-group">
                                                    <div class="pt-2 text-center">
                                                        <h5> 100 Persons </h5>
                                                    </div>
                                                </div>

                                                <div class="col-md-6 form-group">
                                                    <label for="">Rate</label>
                                                    <input type="number" required id="" class="form-control"
                                                        placeholder="Price for 100 Persons" name="p100">
                                                </div><br>
                                                <div class="col-md-4 form-group">
                                                    <div class="pt-2 text-center">
                                                        <h5> 200 Persons </h5>
                                                    </div>
                                                </div>

                                                <div class="col-md-6 form-group">
                                                    <label for="">Rate</label>
                                                    <input type="number" required id="" class="form-control"
                                                        placeholder="Price for 200 Persons" name="p200">
                                                </div><br>
                                                <div class="col-md-4 form-group">
                                                    <div class="pt-2 text-center">
                                                        <h5> 300 Persons </h5>
                                                    </div>
                                                </div>

                                                <div class="col-md-6 form-group">
                                                    <label for="">Rate</label>
                                                    <input type="number" required id="" class="form-control"
                                                        placeholder="Price for 300 Persons" name="p300">
                                                </div><br>
                                                <div class="col-md-4 form-group">
                                                    <div class="pt-2 text-center">
                                                        <h5> 400 Persons </h5>
                                                    </div>
                                                </div>

                                                <div class="col-md-6 form-group">
                                                    <label for="">Rate</label>
                                                    <input type="number" required id="" class="form-control"
                                                        placeholder="Price for 400 Persons" name="p400">
                                                </div><br>
                                                <div class="col-md-4 form-group">
                                                    <div class="pt-2 text-center">
                                                        <h5> 500 Persons </h5>
                                                    </div>
                                                </div>

                                                <div class="col-md-6 form-group">
                                                    <label for="">Rate</label>
                                                    <input type="number" required id="" class="form-control"
                                                        placeholder="Price for 500 Persons" name="p500">
                                                </div><br>
                                                <div class="col-md-4 form-group">
                                                    <div class="pt-2 text-center">
                                                        <h5> 600 Persons </h5>
                                                    </div>
                                                </div>

                                                <div class="col-md-6 form-group">
                                                    <label for="">Rate</label>
                                                    <input type="number" required id="" class="form-control"
                                                        placeholder="Price for 600 Persons" name="p600">
                                                </div><br>
                                                <div class="col-md-4 form-group">
                                                    <div class="pt-2 text-center">
                                                        <h5> 700 Persons </h5>
                                                    </div>
                                                </div>

                                                <div class="col-md-6 form-group">
                                                    <label for="">Rate</label>
                                                    <input type="number" required id="" class="form-control"
                                                        placeholder="Price for 700 Persons" name="p700">
                                                </div><br>

                                                <hr width="90%">
                                                <div>
                                                   <a href="#submit" class="btn btn-primary float-right text-white"
                                                        onclick="appendRow()">Add</a>
                                                </div>
                                                <div class="clearfix"></div>

                                            </div>

                                        </div>

                                        <div class="form-actions">
                                            <button type="submit" id="submit" class="btn btn-primary float-right">
                                                <i class="la la-check-square-o"></i> Submit
                                            </button>
                                            <div class="clearfix"></div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>


                    </div>

                </div>
            </section>

            <!-- // Basic form layout section end -->
        </div>
    </div>
</div>








<script>

    function appendRow() {
        console.log('Hello');
        $('#row').append(
            '<div class="row col-md-12">' +
            '<div class="col-md-1 form-group"></div>' +
            '<div class="col-md-2 form-group">' +
            '<div class="">' +
            '<label> Persons </label>' +
            '<input type="number" required id="" class="form-control" placeholder="Number of Persons" name="persons[]">' +
            '</div>' +
            '</div>' +
            '<div class="col-md-1 form-group"></div>' +
            '<div class="col-md-6 form-group">' +
            '<label for="">Rate</label>' +
            '<input type="number" required id="" class="form-control" placeholder="Price for Persons" name="rate[]">' +
            '</div>' +
            '<div class="col-md-2 form-group"><a href="#submit" onclick="removeRow(this)" class="btn pt-3">' +
            '<i class="ft-trash-2 text-danger"></i></a>' +
            '</div>' +
            '</div><br>'
        );
    }

    function removeRow(obj) {
        var r = $(obj).parent().parent();
        r.remove();
        console.log(r);
    }

    window.addEventListener("load", function () {
        $('body').delegate('#delete_row', 'click', function (event) {
            console.log(event);
        });
    });


</script>