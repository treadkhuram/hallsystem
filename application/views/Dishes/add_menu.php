
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-wrapper-before"></div>

        <div class="content-body">
            <!-- Basic form layout section start -->
            <section id="basic-form-layouts">
                <div class="row match-height">

                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <?php if($this->session->flashdata('msg')):?>
                                    <?php echo $this->session->flashdata('msg');?>
                                    <?php endif;?>
                                    <form method="post" class="form" action="<?php echo base_url();?>Dishes/saveMenu"
                                                     enctype="multipart/form-data" accept-charset="ISO-8859-1">
                                    <div class="form-body">
                                            <h4 class="form-section">
                                                <i class="ft-flag"></i>Add Menu
                                            </h4>
                                            <div class="row">
                                                
                                                <div class="col-md-6 form-group">
                                                    <label for="">Menu Name</label>
                                                    <input type="text" required value="" class="form-control" placeholder="Menu Name"
                                                        name="menu_name">
                                                </div>

                                                <div class="col-md-6 form-group">
                                                    <label for="">Select Dish</label>
                                                    <select name="" required id="dish_id" class="select2 form-control">
                                                            <option value="">Select Any Dish</option>
                                                        <?php foreach($dishes as $raw){ ?>
                                                            <option value="<?= $raw->id ?>"><?= $raw->name ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                                          

                                                <div class="col-md-10"></div>
                                                <div class="col-md-2"> 
                                                    <a  name="add_raw" style="color:white" onclick="addfunction()" class="btn btn-primary btn-block" >
                                                    <i class="la la-check-square-o"></i> Add Dish </a>
                                                </div>

                                            </div>
                                                            <hr width="90%">
                                                            

                                    </div>
    
                                <div class="row">
                                    <div class="col-12">
                                        <div class="card">
                                        <div class="card-header">
                                            <h4 class="card-title">Menu Dishes</h4>
                                            <div class="heading-elements">
                                                <!-- <a class="btn btn-sm btn-danger box-shadow-2 round btn-min-width pull-right" href="<?= base_url() ?>Vandors/addVandor">Add Vandor</a> -->
                                            </div>
                                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                        </div>
                                        <div class="card-content collapse show">
                                            <div class="table-responsive">
                                            <table id="tbl" class="table table-striped table-bordered">
                                                <thead class="bg-primary white">
                                                <tr>
                                                    <th>#</th>
                                                    <th>Dish Name</th>
                                                    <th>Actions</th>
                                                </tr>
                                                </thead>
                                                 
                                                <tbody id="tbody">
                                                    <!-- <?php  
                                                        if(isset($recipes)){ 
                                                        $c = 0;
                                                        foreach($recipes as $res){
                                                            $c++;
                                                            ?>
                                                        <tr>
                                                        <td><?= $c?></td>
                                                        <td><?= $res->product_name?></td>
                                                        <td><?= $res->qty?> <span><?= ' per/'.$res->raw_unit?></span> </td>
                                                        <td> 
                                                        <a onclick="removeRow(this)" class="a_button"><i class="ft-trash-2 text-danger"></i></a>
                                                        <input type="hidden" value="<?= $res->dish_id?>" name="dish_id[]"/>
                                                        <input type="hidden" value="<?= $res->qty?>" name="qty[]"/>
                                                        <input type="hidden" value="<?= $res->raw_material_id?>" name="raw_material_id[]"/>
                                                        </td>
                                                        </tr>
                                                    <?php }}?>       -->
                                                </tbody>
                                                            
                                            </table>
                                                           
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="update_id" value="<??>">
                                                        <input type="submit" name="submit" value="Save" class="btn btn-primary m-1 mt-3 float-right">
                                                </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

        

        </div>
    </div>
</div>




<script type="text/javascript">

        var i = 0;
        function addfunction(){
           
            i++;
            if($('#dish_id').val() == ''){
                alert('select dish');
            }else{
                var dish_id = $('#dish_id').val();
                var dish_id_text = $('#dish_id option:selected').text();
           
                var newCell = '<tr><td>'+i+'</td><td>'+dish_id_text+'</td>'+
                '<td> <a onclick="removeRow(this)" class="a_button"><i class="ft-trash-2 text-danger"></i></a>'+
                '<input type="hidden" value="'+dish_id+'" name="dish_id[]"/>'+
                '</td></tr>';
                
                $('#tbody').append(newCell);           
            }
        }


        function removeRow(obj){
        var r = $(obj).parent().parent();
        r.remove();
        console.log(r);
    }

    window.addEventListener("load", function(){
        console.log('hello');
        $('body').delegate('#delete_row', 'click', function(event){
            console.log(event);
        });
    });

</script>