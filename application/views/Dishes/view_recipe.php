
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-wrapper-before"></div>

        <div class="content-body">
            <!-- Basic form layout section start -->
            <section id="basic-form-layouts">
                <div class="row match-height">

                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <?php if($this->session->flashdata('msg')):?>
                                    <?php echo $this->session->flashdata('msg');?>
                                    <?php endif;?>
                                     <div class="form-body">
                                            <h4 class="form-section">
                                                <i class="ft-flag"></i>View Recipe
                                            </h4>
                                            <div class="row">
                                                

                                            <div class="col-md-6 form-group">
                                                    <label for="companyName">Select raw_material</label>
                                                    <select name="raw_material_id" required onchange="setVal('<?= base_url()?>Dishes/getUnit')" id="raw_material_id" class="select2 form-control">
                                                            <option value="">Select Raw Material</option>
                                                        <?php foreach($raw_materials as $raw){ ?>
                                                            <option value="<?= $raw->id ?>"><?= $raw->name ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                                            <input type="hidden" name="unit_val" id="unit_val" >
                                                <div class="col-md-6 form-group">
                                                    <label for="">Quantity</label> <span id="unit"></span>
                                                    <input type="number" required value="" id="qty" class="form-control" placeholder="Quantity"
                                                        name="">
                                                </div>

                                                <div class="col-md-10"></div>
                                                <div class="col-md-2"> 
                                                    <a  name="add_raw" onclick="addfunction()" class="btn btn-primary btn-block text-dark">
                                                    <i class="la la-check-square-o"></i> Add </a>
                                                </div>

                                            </div>
                                                            <hr width="90%">

                                            </div>
    
                                    


                                <div class="row">
                                    <div class="col-12">
                                        <div class="card">
                                        <div class="card-header">
                                            <h4 class="card-title">Raw Materials</h4>
                                            <div class="heading-elements">
                                                <!-- <a class="btn btn-sm btn-danger box-shadow-2 round btn-min-width pull-right" href="<?= base_url() ?>Vandors/addVandor">Add Vandor</a> -->
                                            </div>
                                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                        </div>
                                        <div class="card-content collapse show">
                                            <div class="table-responsive">
                                            <form method="post" class="form" action="<?php echo base_url();?>Dishes/saveRecipe"
                                                     enctype="multipart/form-data" accept-charset="ISO-8859-1">
                                            <table id="tbl" class="table table-striped table-bordered">
                                                <thead class="bg-primary white">
                                                <tr>
                                                    <th>#</th>
                                                    <th>Raw Material</th>
                                                    <th>Quantity</th>
                                                    <th>Actions</th>
                                                </tr>
                                                </thead>
                                                 
                                                <tbody id="tbody">
                                                    <?php  
                                                        if(isset($recipes)){ 
                                                        $c = 0;
                                                        foreach($recipes as $res){
                                                            $c++;
                                                            ?>
                                                        <tr>
                                                        <td><?= $c?></td>
                                                        <td><?= $res->product_name?></td>
                                                        <td><?= $res->qty?> <span><?= ' per/'.$res->raw_unit?></span> </td>
                                                        <td> 
                                                        <a onclick="removeRow(this)" class="a_button"><i class="ft-trash-2 text-danger"></i></a>
                                                        <input type="hidden" value="<?= $res->dish_id?>" name="dish_id[]"/>
                                                        <input type="hidden" value="<?= $res->qty?>" name="qty[]"/>
                                                        <input type="hidden" value="<?= $res->raw_material_id?>" name="raw_material_id[]"/>
                                                        </td>
                                                        </tr>
                                                    <?php }}?>      
                                                </tbody>
                                                            
                                            </table>
                                                            <input type="hidden" name="update_id" value="<?= $dish_id?>">
                                                        <input type="submit" name="submit" value="Save" class="btn btn-primary m-1 mt-3 float-right">
                                                </form>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                           
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

        

        </div>
    </div>
</div>






<script type="text/javascript">

    function setVal(url) {
        var raw_material_id = $('#raw_material_id').val();
    //    console.log(raw_material_id);
        $.ajax({
            url: url+'/'+ raw_material_id,
            type: "POST",
            success: function(res){
            // console.log(res);
            $('#unit').html("- "+res);
            $('#unit_val').val(res);
           }
       }); 
    }

        var i = 0;
        function addfunction(){
           
            i++;
            if($('#raw_material_id').val() == '' ||  $('#qty').val() == '' || $('#qty').val() == null ){
                alert("SomeThing Left");
            }else{
                var unitVal = $('#unit_val').val();
                var dish_id = <?= $dish_id?>;
                var raw_material_id = $('#raw_material_id').val();
                var raw_material_text = $('#raw_material_id option:selected').text();
                var qty = $('#qty').val();
                
                var newCell = '<tr><td>'+i+'</td><td>'+raw_material_text+'</td><td>'+qty+' per/'+unitVal+'</td>'+
                '<td> <a onclick="removeRow(this)" class="a_button"><i class="ft-trash-2 text-danger"></i></a>'+
                '<input type="hidden" value="'+dish_id+'" name="dish_id[]"/>'+
                '<input type="hidden" value="'+qty+'" name="qty[]"/>'+
                '<input type="hidden" value="'+raw_material_id+'" name="raw_material_id[]"/>'+
                '</td></tr>';
                
                $('#tbody').append(newCell);
                
                $('#qty').val('');
            }
            
        }


        function removeRow(obj){
        var r = $(obj).parent().parent();
        r.remove();
        console.log(r);
    }

    window.addEventListener("load", function(){
        console.log('hello');
        $('body').delegate('#delete_row', 'click', function(event){
            console.log(event);
        });
    });

</script>