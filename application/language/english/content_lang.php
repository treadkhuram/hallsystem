<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['welcome'] = 'Welcome to our website';

//menu
$lang['dashboard'] = 'Dashboard';
$lang['users'] = 'Users';
$lang['drivers'] = 'Drivers';
$lang['customers'] = 'Customers';
$lang['vehicles'] = 'Vehicles';
$lang['orders'] = 'Orders';
$lang['add_new'] = 'Add New';
$lang['view_all'] = 'View All';
$lang['all'] = 'All';
$lang['ios'] = 'IOS';
$lang['android'] = 'Android';
$lang['feedback'] = 'Feedback';
$lang['reports'] = 'Reports';
$lang['settings'] = 'Settings';
$lang['wallet'] = 'Wallet';


?>