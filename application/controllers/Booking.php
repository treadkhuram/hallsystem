<?php

class Booking extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Db_model', 'dbm');
        $this->load->model('Customer_model', 'cm');
        $this->load->model('Booking_model', 'bm');
        $this->load->model('Dishes_model', 'dm');
        $this->load->model('RawMaterial_model', 'rm');
        if (!($this->session->userdata('admin_logged_in'))) {
            redirect('login');
        }
    }

    public function allBookings()
    {
        $startDate = date('Y-01-01');
        $endDate = date('Y-m-d');
        $activeTab = 'active';
        if(isset($_POST['filter'])){
            $startDate = $this->input->post('start_date');
            $endDate = $this->input->post('end_date');
            $activeTab = 'complete';
        }
        $completed = $this->bm->retrieveBookings(1, $startDate, $endDate);
        $data = array(
            'view' => 'Booking/bookings',
            'active' => 'Booking',
            'pendingBooking' => $this->bm->retrieveBookings(0),
            'completeBooking' => $completed,
            'cancelBooking' => $this->bm->retrieveBookings(-1),
            'activeTab' => $activeTab,
            'start_date' => $startDate,
            'end_date' => $endDate
        );
        $this->load->view('main_layouts/headerFooter', $data);
    }

    public function eventCloseForm($id)
    {
        $amountReceived = $this->bm->getTotalReceivedAmount($id);
        $amountReceived = isset($amountReceived) ? $amountReceived : 0;
        $bookingData = $this->bm->getBookingOrder($id, 1);
        $functionAmount = $bookingData->confirm_persons * $bookingData->per_person_rate;
        $subTotal = ($bookingData->confirm_persons * $bookingData->per_person_rate) - $amountReceived;
        $data = array(
            'active' => 'Booking',
            'view' => 'Booking/event_close_form',
            'bookingData' => $bookingData,
            'amountReceived' => $amountReceived,
            'functionAmount' => $functionAmount,
            'subTotal' => $subTotal
        );
        $this->load->view('main_layouts/headerFooter', $data);
    }

    public function saveEventCloseForm()
    {
        $id = $this->input->post('booking_id');
        $data = array(
            'tax' => $this->input->post('tax'),
            'discount' => $this->input->post('discount'),
            'confirm_persons'=>$this->input->post('total_persons'),
            'total_amount' => $this->input->post('function_amount'), 'status' => 1,
        );
        $amount = $this->input->post('received_amount');
        $amount = isset($amount) ? $amount : 0;
        if($amount > 0) {
            $dataTransaction = array(
                'referance' => $this->input->post('customer_name'), 'amount' => $amount,
                'detail' => 'receive on event close', 'date' => date('Y-m-d'), 'booking_id' => $id,
            );
            $transactionId = $this->dbm->createGetId('booking_transactions', $dataTransaction);

//            $bankData = array('date' => date('Y-m-d'),
//                'detail' => 'Booking Amount ' . $dataTransaction['detail'],
//                'debit' => $dataTransaction['amount'], 'credit' => 0, 'bank_id' => get_default_bank_id(),
//                'booking_transaction_id' => $transactionId);
//            $this->dbm->create('bank_account', $bankData);
            add_amount_in_booking_account($amount, $transactionId, $id);
        }
        $extraProduct = $this->input->post('new_product_name');
        $extraQty = $this->input->post('new_product_qty');
        $extraPrice = $this->input->post('new_product_price');

        if (isset($extraProduct)) {
            foreach ($extraProduct as $key => $ep) {
                $extraData = array('product_name' => $ep, 'qty' => $extraQty[$key],
                    'price' => $extraPrice[$key], 'booking_id' => $id);
                $this->dbm->create('booking_extra', $extraData);
            }
        }

        $this->dbm->update('booking', $id, $data);
        $bookingData = $this->dbm->retriveById('booking', $id);
        $dishes = $this->bm->getMenuItemsOnId($id);
        print_r($dishes);
        foreach ($dishes as $key => $d) {
            $dishRecipe = $this->dm->getDishCost($d->dish_id);
            foreach ($dishRecipe as $dr) {
                $personServing = $d->no_of_persons_serving;
                $recipeQty = $dr->recipe_qty;
                $singlePersonQty = $recipeQty / $personServing;
                $totalPersonQty = $singlePersonQty * $bookingData->confirm_persons;
                $totalPersonQty = $totalPersonQty / $dr->converstion_factor;
                $totalPersonQty = number_format($totalPersonQty, 2);
                $this->rm->subtractRawMaterial($dr->id, $totalPersonQty);
            }
        }

        $this->session->set_flashdata('msg', '<p class="alert alert-success">Booking closed successfully</p>');
        redirect('Booking/allBookings');
    }

    public function addBooking()
    {
        $data = array(
            'active' => 'Booking',
            'view' => 'Booking/add_booking',
            'menus' => $this->dbm->retriveTable('menu', 1),
            'dishes' => $this->dbm->retriveTable('dishes'),
        );
        $this->load->view('main_layouts/headerFooter', $data);
    }

    public function addBookingAmount()
    {
        $data_array = array(
            'referance' => $this->input->post('referance'),
            'amount' => $this->input->post('amount'),
            'detail' => $this->input->post('detail'),
            'date' => date('Y-m-d'),
            'booking_id' => $this->input->post('booking_id'),
        );
        $transactionId = $this->dbm->createGetId('booking_transactions', $data_array);

//        $bankData = array('date' => date('Y-m-d'), 'detail' => 'Booking Amount ' . $data_array['detail'],
//            'debit' => $data_array['amount'], 'credit' => 0, 'bank_id' => get_default_bank_id(),
//            'booking_transaction_id' => $transactionId);
//        $this->dbm->create('bank_account', $bankData);
        add_amount_in_booking_account($data_array['amount'], $transactionId, $data_array['booking_id']);
        redirect('Booking/allBookings');
    }

    public function saveBooking()
    {
        $bookingPerson = $this->input->post('booking_persons');
        $confirmPerson = $this->input->post('confirm_persons');
        $confirmPerson = isset($confirmPerson) ? $confirmPerson : 0;
        if ($bookingPerson > $confirmPerson) {
            $confirmPerson = $bookingPerson;
        }
        $bookingRate = $this->input->post('per_person');
        $amount = $bookingPerson * $bookingRate;
        $data = array(
            'customer_name' => $this->input->post('customer_name'),
            'serial_no' => $this->input->post('serial_no'),
            'booking_date' => $this->input->post('booking_date'),
            'function_date' => $this->input->post('function_date'),
            'fun_st_time' => $this->input->post('fun_st_time'),
            'fun_end_time' => $this->input->post('fun_end_time'),
            'phone' => $this->input->post('phone'),
            'function_name' => $this->input->post('function_name'),
            'booking_persons' => $bookingPerson,
            'confirm_persons' => $confirmPerson,
            'function_cost' => $this->input->post('txt_function_cost'),
            'per_person_rate' => $bookingRate,
            'total_amount' => $amount,
            'hall_charges'=>$this->input->post('hall_charges'),
            'hall_labour_cost'=>$this->input->post('labour_cost'),
            'hall_tax'=>$this->input->post('function_tax'),
            'tax'=>$this->input->post('txt_function_tax_amount'),
            'hall' => $this->input->post('hall'),
            'notes' => $this->input->post('notes'),
            'menu_id' => $this->input->post('function_menu'),
            'active' => 1,
        );
        $booking_id = $this->dbm->createGetId('booking', $data);
        $dishes = $this->input->post('dish_id');
        $costs = $this->input->post('dish_cost');
        foreach ($dishes as $key => $d) {
            if (isset($d)) {
                $checkSelection = $this->input->post('select_dish' . $d);
                if (isset($checkSelection)) {
                    $dish_data = array('dish_id' => $d, 'booking_id' => $booking_id, 'cost' => $costs[$key]);
                    $this->dbm->create('booking_menu', $dish_data);
                }
            }
        }

        $bookingAmount = $this->input->post('booking_advance');
        if(isset($bookingAmount) && $bookingAmount > 0){
            $booking_trans = array(
                'referance' => $this->input->post('customer_name'),
                'amount' => $this->input->post('booking_advance'), 'detail' => 'Booking Advance',
                'date' => date('Y-m-d'), 'booking_id' => $booking_id, 'is_advance' => 0
            );
            $transactionId = $this->dbm->createGetId('booking_transactions', $booking_trans);

//            $bankData = array('date' => date('Y-m-d'), 'detail' => 'Booking Advance Received',
//                'debit' => $booking_trans['amount'], 'credit' => 0, 'bank_id' => get_default_bank_id(),
//                'booking_transaction_id' => $transactionId);
//
//            $this->dbm->create('bank_account', $bankData);
            add_amount_in_booking_account($booking_trans['amount'], $transactionId, $booking_id);
        }
        $this->session->set_flashdata('msg', '<p class="alert alert-success">Booking added successfully</pre>');
        redirect('Booking/addBooking');
    }

    public function updateBooking($id)
    {

        $booking_data = $this->bm->getBookingOrder($id, 1);
        $booking_data->dishMenu = $this->bm->getMenuDishes($booking_data->id);
        $booking_data->allDishes = $this->bm->getMenuItemsOnId($booking_data->id);
        $totalPersons = $booking_data->booking_persons;
        $body = '<tbody id="tbl_menu_dishes">';
        $totalMenuCost = 0;
        foreach ($booking_data->allDishes as $key => &$d) {
            $dishRecipe = $this->dm->getDishCost($d->dish_id);
            $dishCost = 0;
            $dishItems = '<table>';
            $itemCost = 0;
            foreach ($dishRecipe as $dr) {
//                $personServing = $d->no_of_persons_serving;
//                $recipeQty = $dr->recipe_qty;
//                $singlePersonQty = $recipeQty / $personServing;
//                $totalPersonQty = $singlePersonQty * $totalPersons;
//                $dishCost += ($totalPersonQty / $dr->converstion_factor) * $dr->purchase_price;
//                $totalPersonQty = number_format($totalPersonQty, 2);
//                $dishItems .= $dr->name . ' - ' . $totalPersonQty . ' ' . $dr->receipe_unit . '<br>';

                $personServing = $d->no_of_persons_serving;
                $recipeQty = $dr->recipe_qty;
                $singlePersonQty = $recipeQty / $personServing;
                $totalPersonQty = $singlePersonQty * $totalPersons;
                $itemCost = ($totalPersonQty / $dr->converstion_factor) * $dr->purchase_price;
                $dishCost += $itemCost;
                $totalPersonQty = number_format($totalPersonQty, 2);

                $dishItems .='<tr>';
                $dishItems .='<td>'.$dr->name.'</td>';
                $dishItems .='<td>'.$totalPersonQty.'-'.$dr->receipe_unit.'</td>';
                $dishItems .='<td>'.ceil($itemCost).' PK</td>';
                $dishItems .='</tr>';
            }
//            $d->cost = $dishCost;
//            $d->recipe = $dishItems;


            $dishItems .= '</table>';
            $totalMenuCost = $totalMenuCost + ceil($dishCost);
            $body .= '<tr>';
            $body .= '<td>' . ($key + 1) . '</td>';
            $body .= '<td>' . $d->dish_name . '</td>';
            $body .= '<td>' . ceil($dishCost) . ' PK</td>';
            $body .= '<td>' . $dishItems . '</td>';
            $body .= '<td>
                 <input onchange="handleChange(event, '.ceil($dishCost).')" id="chk_select_dish" name="select_dish' . $d->dish_id . '" type="checkbox" checked>&nbsp<label>Select</label>
                 <input type="hidden" name="dish_cost[]" value="' . ceil($dishCost) . '">
                 <input type="hidden" name="dish_id[]" value="' . $d->dish_id . '">
                 </td>';
            $body .= '</tr>';

        }
        $booking_data->lbl_booking_per_person = '('.$booking_data->per_person_rate.'), '.$booking_data->per_person_rate.' X '.$booking_data->booking_persons.' = '.
            ($booking_data->per_person_rate * $booking_data->booking_persons);
        $taxAmount = ($booking_data->hall_tax / 100) * $booking_data->total_amount;
        $booking_data->lbl_tax_amount = $taxAmount;
        $body .= '</tbody>';
        $data = array(
            'view' => 'Booking/edit_booking',
            'active' => 'Booking',
            'update_id' => $id,
            'menus' => $this->dbm->retriveTable('menu', 1),
            'dishes' => $this->dbm->retriveTable('dishes'),
            'order' => $booking_data,
            'table_data'=>$body
        );
        $this->load->view('main_layouts/headerFooter', $data);
    }

    public function saveUpdateBooking()
    {
        $updateId = $this->input->post('update_id');
        $bookingPerson = $this->input->post('booking_persons');
        $bookingRate = $this->input->post('per_person');
        $amount = $bookingPerson * $bookingRate;
        $data = array(
            'customer_name' => $this->input->post('customer_name'),
            'serial_no'=>$this->input->post('serial_no'),
            'booking_date' => $this->input->post('booking_date'),
            'function_date' => $this->input->post('function_date'),
            'fun_st_time' => $this->input->post('fun_st_time'),
            'fun_end_time' => $this->input->post('fun_end_time'),
            'phone' => $this->input->post('phone'),
            'function_name' => $this->input->post('function_name'),
            'booking_persons' => $this->input->post('booking_persons'),
            'confirm_persons' => $this->input->post('confirm_persons'),

            'function_cost' => $this->input->post('txt_function_cost'),
            'per_person_rate' => $bookingRate,
            'total_amount' => $amount,
            'hall_charges'=>$this->input->post('hall_charges'),
            'hall_labour_cost'=>$this->input->post('labour_cost'),
            'hall_tax'=>$this->input->post('function_tax'),
            'tax'=>$this->input->post('txt_function_tax_amount'),

            'hall' => $this->input->post('hall'),
            'notes' => $this->input->post('notes'),
            'menu_id' => $this->input->post('function_menu'),
            'active' => 1,
        );


        $this->dbm->update('booking', $updateId, $data);
        $this->bm->removeMenuDishes($updateId);

        $dishes = $this->input->post('dish_id');
        $costs = $this->input->post('dish_cost');
        foreach ($dishes as $key => $d) {
            if (isset($d)) {
                $checkSelection = $this->input->post('select_dish' . $d);
                if (isset($checkSelection)) {
                    $dish_data = array('dish_id' => $d, 'booking_id' => $updateId, 'cost' => $costs[$key]);
                    $this->dbm->create('booking_menu', $dish_data);
                }
            }
        }

        $this->session->set_flashdata('msg', '<p class="alert alert-success">Dish Updated Successfully</pre>');
        redirect('Booking/allBookings');
    }

    public function deleteBooking($id)
    {
        $data = array('active' => 0);
        $this->dbm->update('booking', $id, $data);
        redirect('Booking/allBookings');
    }

    public function view_booking($id)
    {
        $booking = $this->dbm->retriveById('booking', $id);
        $booking->menu_name = $this->dbm->retriveById('menu', $booking->menu_id)->name;
        $booking->dishes = $this->bm->getMenuDishes($id);
        $bookingExtra = $this->dbm->retriveTableByFKey('booking_extra', 'booking_id', $id);
        $totalExtraAmount = 0;
        foreach ($bookingExtra as $bx) {
            $totalExtraAmount += ($bx->qty * $bx->price);
        }
        $booking->function_amount = ($booking->confirm_persons * $booking->per_person_rate) + $totalExtraAmount;
        $booking->net_total = $booking->function_amount - $booking->discount + $booking->tax;
        $booking->total_Received_amount = $this->bm->getTotalReceivedAmount($id);
        $booking->balance = $booking->net_total - $booking->total_Received_amount;

        $data = array(
            'view' => 'Booking/view_booking',
            'active' => 'Booking',
            'booking_detail' => $booking,
            'booking_transaction' => $this->dbm->retriveTableByFKey('booking_transactions', 'booking_id', $id),
            'booking_extra' => $bookingExtra,
        );

        $this->load->view('main_layouts/headerFooter', $data);
    }

    function getMenuDishes()
    {
        $id = $this->input->post('id');
        $totalPersons = $this->input->post('total_persons');
        $menu_rates = $this->dm->getMenuRatesOnId($id);

        $menuRate = get_menu_rate($menu_rates, $totalPersons);

        $dishes = $this->dm->getMenuItemsOnId($id);
        // $dishRecipe = $this->dm->getDishCost(1);
        // echo personServing;
        $body = '<tbody id="tbl_menu_dishes">';
        $totalMenuCost = 0;
        foreach ($dishes as $key => &$d) {
            $dishRecipe = $this->dm->getDishCost($d->dish_id);
            $dishCost = 0;
            $dishItems = '<table>';
            foreach ($dishRecipe as $dr) {
                $personServing = $d->no_of_persons_serving;
                $recipeQty = $dr->recipe_qty;
                $singlePersonQty = $recipeQty / $personServing;
                $totalPersonQty = $singlePersonQty * $totalPersons;
                $itemCost = ($totalPersonQty / $dr->converstion_factor) * $dr->purchase_price;
                $dishCost += $itemCost;
                $totalPersonQty = number_format($totalPersonQty, 2);

                $dishItems .='<tr>';
                $dishItems .='<td>'.$dr->name.'</td>';
                $dishItems .='<td>'.$totalPersonQty.'-'.$dr->receipe_unit.'</td>';
                $dishItems .='<td>'.ceil($itemCost).' PK</td>';
                $dishItems .='</tr>';
            }
            $dishItems .= '</table>';
            $totalMenuCost = $totalMenuCost + ceil($dishCost);
            $body .= '<tr>';
            $body .= '<td>' . ($key + 1) . '</td>';
            $body .= '<td>' . $d->dish_name . '</td>';
            $body .= '<td>' . ceil($dishCost) . ' PK</td>';
            $body .= '<td>' . $dishItems . '</td>';
            $body .= '<td>
      <input onchange="handleChange(event, '.ceil($dishCost).')" id="chk_select_dish" name="select_dish' . $d->dish_id . '" type="checkbox" checked>&nbsp<label>Select</label>
      <input type="hidden" name="dish_cost[]" value="' . ceil($dishCost) . '">
      <input type="hidden" name="dish_id[]" value="' . $d->dish_id . '">
      </td>';
            $body .= '</tr>';
        }
        $body .= '</tbody>';
        $res['menu_rate'] = $menuRate;
        $res ['total_cost'] = $totalMenuCost;
        $res ['per_person_cost'] = ceil($totalMenuCost/$totalPersons);
        $res['table_body'] = $body;
        echo json_encode($res);
    }


    public function getSingleDishMenu()
    {
        $dishId = $this->input->post('dish_id');
        $totalPersons = $this->input->post('total_person');

        $dishData = $this->dbm->retriveById('dishes', $dishId);
        $dishRecipe = $this->dm->getDishCost($dishId);

        $menu_rates = $this->dm->getMenuRatesOnId($dishId);
//        $menuRate = get_menu_rate($menu_rates, $totalPersons);

        $dishCost = 0;
        $dishItems = '';
        foreach ($dishRecipe as $dr) {
            $personServing = $dishData->no_of_persons_serving;
            $recipeQty = $dr->recipe_qty;
            $singlePersonQty = $recipeQty / $personServing;
            $totalPersonQty = $singlePersonQty * $totalPersons;
            $dishCost += ($totalPersonQty / $dr->converstion_factor) * $dr->purchase_price;
            $totalPersonQty = number_format($totalPersonQty, 2);
            $dishItems .= $dr->name . ' - ' . $totalPersonQty . ' ' . $dr->receipe_unit . '<br>';
        }

        $body = '<tr>';
        $body .= '<td>Extra</td>';
        $body .= '<td>' . $dishData->name . '</td>';
        $body .= '<td>' . $dishCost . '</td>';
        $body .= '<td>' . $dishItems . '</td>';
        $body .= '<td><input onchange="handleChange(event, '.ceil($dishCost).')" id="chk_select_dish" name="select_dish' . $dishId . '" type="checkbox" checked>&nbsp<label>Select</label>
    <input type="hidden" name="dish_cost[]" value="' . $dishCost . '">
    <input type="hidden" name="dish_id[]" value="' . $dishId . '">
    </td>';
        $body .= '</tr>';

        $res['dish_cost'] = $dishCost;
        $res['person_cost'] = ceil($dishCost/$totalPersons);
        $res['table']=$body;
        echo json_encode($res);

    }

    public function deleteTransaction($transactionId, $bookingId)
    {
        $this->dbm->delete('booking_transactions', $transactionId);
        $this->dbm->deleteByFk('bank_account', 'booking_transaction_id', $transactionId);

        return redirect('booking/view_booking/' . $bookingId);
    }

    function printBooking($id)
    {
        $data['booking'] = $this->dbm->retriveById('booking', $id);
        $data['booking_trans'] = $this->dbm->retriveTableByFKey('booking_transactions', 'booking_id', $id);
        $data['booking_menu'] = $this->bm->retriveDishesById($id);
        // echo '<pre>';
        // print_r($data);die();
        $this->load->view('reports/booking_print', $data);
    }

    public function cancelEvent($id)
    {
        $booking = $this->dbm->retriveById('booking', $id);
        $booking->menu_name = $this->dbm->retriveById('menu', $booking->menu_id)->name;
        $booking->dishes = $this->bm->getMenuDishes($id);
        $bookingExtra = $this->dbm->retriveTableByFKey('booking_extra', 'booking_id', $id);
        $totalExtraAmount = 0;
        foreach ($bookingExtra as $bx) {
            $totalExtraAmount += ($bx->qty * $bx->price);
        }
        $booking->function_amount = ($booking->confirm_persons * $booking->per_person_rate) + $totalExtraAmount;
        $booking->net_total = $booking->function_amount - $booking->discount + $booking->tax;
        $booking->total_Received_amount = $this->bm->getTotalReceivedAmount($id);
        $booking->balance = $booking->net_total - $booking->total_Received_amount;


        $data = array(
            'view' => 'Booking/cancel_booking', 'active' => 'Booking',
            'booking_detail' => $booking, 'booking_extra' => $bookingExtra,
            'booking_transaction' => $this->dbm->retriveTableByFKey('booking_transactions', 'booking_id', $id),
        );

        $this->load->view('main_layouts/headerFooter', $data);
    }

    public function saveCancelEvent()
    {
        $bookingId = $this->input->post('booking_id');
        $returnAmount = $this->input->post('txt_return_amount');
        $detail = $this->input->post('txt_detail');

        $bookingData = array('notes' => "Canceled due to " . $detail, 'status' => -1);
        $this->dbm->update('booking', $bookingId, $bookingData);

        $booking_trans = array(
            'referance' => $this->input->post('booking_person_name'),
            'amount' => $returnAmount * -1, 'detail' => 'Booking Advance return', 'date' => date('Y-m-d'),
            'booking_id' => $bookingId, 'is_advance' => 0
        );
        $transactionId = $this->dbm->createGetId('booking_transactions', $booking_trans);

        $bankData = array('date' => date('Y-m-d'), 'detail' => 'Booking return ' . $detail,
            'debit' => 0, 'credit' => $returnAmount, 'bank_id' => get_default_bank_id(),
            'booking_transaction_id' => $transactionId);
        $this->dbm->create('bank_account', $bankData);
        $this->session->set_flashdata('msg', '<p class="alert alert-success">Booking Canceled</pre>');
        redirect('Booking/allBookings');
    }

}