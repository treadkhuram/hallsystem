<?php

class PersonalAccount extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Db_model', 'dbm');
        $this->load->model('Customer_model', 'cm');
        $this->load->model('Booking_model', 'bm');
        $this->load->model('Dishes_model', 'dm');
        $this->load->model('RawMaterial_model', 'rm');
        if (!($this->session->userdata('admin_logged_in'))) {
            redirect('login');
        }
    }


    public function personal_account(){
        
        $data = array(
            'view'=> 'personal_account/personal_account',
            'active'=> 'Personal Account',
            'accounts' => $this->dbm->retriveTable('bank', 1),
        );
        $this->load->view('main_layouts/headerFooter', $data);
    }
    public function add_personal_account(){
        
        $data = array(
            'view'=> 'personal_account/add_personal_account',
            'active'=> 'Personal Account'
        );
        $this->load->view('main_layouts/headerFooter', $data);
    }

    public function savePersonalAccount(){

        $data = array(
            'name' => $this->input->post('name'),
            'detail' => $this->input->post('detail'),
            'active' => 1,
        );
        if($this->dbm->create('bank', $data)){

            $this->session->set_flashdata('msg', '<p class="alert alert-success">Updated Successfully</p>');
            redirect('PersonalAccount/add_personal_account');
        }else{
            $this->session->set_flashdata('msg', '<p class="alert alert-danger">Error Occured</p>');
            redirect('PersonalAccount/add_personal_account');
        }

    }

    public function edit_personal_account($id){
        
        $data = array(
            'view'=> 'personal_account/edit_personal_account',
            'active'=> 'Personal Account',
            'accounts' => $this->dbm->retriveById('bank', $id),
        );
        $this->load->view('main_layouts/headerFooter', $data);
    }

    public function updatePersonalAccount(){

            $id = $this->input->post('id');
        $data = array(
            'name' => $this->input->post('name'),
            'detail' => $this->input->post('detail'),
        );
        if($this->dbm->update('bank', $id, $data)){
            $this->session->set_flashdata('msg', '<p class="alert alert-success">Updated Successfully</p>');
            redirect('PersonalAccount/edit_personal_account/'.$id);
        }else{
            $this->session->set_flashdata('msg', '<p class="alert alert-danger">Error Occured</p>');
            redirect('PersonalAccount/edit_personal_account/'.$id);
        }

    }





    public function del_personal_account(){
        
        $id = $this->input->post('id');
        $data = array(
            'active' => 0,
        );
        $this->dbm->update('bank', $id, $data);
        redirect('PersonalAccount/personal_account');
    }







}
?>