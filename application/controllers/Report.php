<?php
class Report extends CI_Controller
{

  public function __construct(){
        parent::__construct();
        $this->load->model('Db_model','dbm');
        $this->load->model('Customer_model','cm');
        $this->load->model('Booking_model','bm');
        $this->load->model('Dishes_model','dm');
        $this->load->model('Vandor_model','vm');
        $this->load->model('Reports_model','rm');
        $this->load->model('Bank_model','bkm');
        $this->load->model('Cash_model');
        if(!($this->session->userdata('admin_logged_in'))){
            redirect('login');
        }
    }

    public function bookingReport(){

        $data = array(
            'view' => 'reports/booking_report',
            'active' => 'Booking',
        );

        if(isset($_POST['filter'])){
            $status = $this->input->post('booking_status');
            $start_date = $this->input->post('start_date');
            $end_date = $this->input->post('end_date');

            $bookingData = $this->rm->getBookingReport($status, $start_date, $end_date);
            $data['bookingReport'] = $bookingData; 
            $data['start_date'] = $start_date;
            $data['end_date'] = $end_date;
            $data['booking_type'] = $status;
            $total_amount = 0;
            $recieve_amount = 0;
            $single_receive_amount = 0;
            $balance = 0;
            foreach($bookingData as $bd){
               $total_amount += ($bd->total_amount + $bd->tax) - ($bd->discount);
               $single_receive_amount = $this->bm->getTotalReceivedAmount($bd->id);
               $recieve_amount += $this->bm->getTotalReceivedAmount($bd->id);
               $data['single_receive_amount'] = $single_receive_amount;
            }
            $balance = $total_amount - $recieve_amount;
            
            $data['receive_amount'] = $recieve_amount;
            $data['total_amount'] = $total_amount;
            $data['balance'] = $balance;
        }

        $this->load->view('main_layouts/headerFooter', $data);
    }

    public function vendorBalance(){

      $vendors = $this->dbm->retriveTable('vandors', 1);
      $totalBalance = 0;
      foreach ($vendors as $ven){
          $debit = $this->vm->getTotalColumnSum($ven->id, 'debit')->debit;
          $credit = $this->vm->getTotalColumnSum($ven->id, 'credit')->credit;
          $debit = isset($debit) ? $debit : 0;
          $credit = isset($credit) ? $credit : 0;
          $ven->balance = $credit-$debit;
          $totalBalance += $ven->balance;
      }

        $data = array(
            'view' => 'reports/vendor_balance',
            'active' => 'Vandors',
            'vendors' => $vendors,
            'total_balance' => $totalBalance
        );
        $this->load->view('main_layouts/headerFooter', $data);
    }

    public function pendingBookingPayments()
    {   
        $data = array(
            'view' => 'reports/pendingBookingPayments',
            'active' => 'Booking',
        );

        if(isset($_POST['filter'])){

           $strt_date = $this->input->post('start_date');
           $end_date = $this->input->post('end_date');

            $bookings = $this->rm->getBookingReport(1, $strt_date, $end_date);
            $bdata = array();
            foreach($bookings as &$b){
                $rec_amount = $this->bm->getTotalReceivedAmount($b->id);
                
                if($rec_amount < $b->total_amount){
                    $b->receive_amunt = $rec_amount;
                    array_push($bdata, $b);
                }
            }
            $data['booking'] = $bdata;
            $data['start_date'] = $strt_date;
            $data['end_date'] = $end_date;            
        }

        $this->load->view('main_layouts/headerFooter', $data);
    }
    public function allEmployeeSalerySheet()
    {   
        $data = array(
            'view' => 'reports/allEmployeeSalerySheet',
            'active' => 'Employees',
        );

        if(isset($_POST['filter'])){
            $strt_date = $this->input->post('start_date');
            $end_date = $this->input->post('end_date');

            $allEmp = $this->dbm->retriveTable('employees',1);
            $ddata = array();
            foreach($allEmp as $em){
                $paid_amount = 0;
                $trans = $this->rm->getAllSaleryTransaction($em->id, $strt_date, $end_date);
                foreach($trans as $t){
                    $paid_amount += $t->total_amount;
                }
                    // $em->employee = $trans;
                    array_push($ddata, $em); 
                    $em->paid_amount = $paid_amount;
            }
            $data['transactions'] = $ddata;
            $data['start_date'] = $strt_date;
            $data['end_date'] = $end_date;
        }
        
        $this->load->view('main_layouts/headerFooter', $data);
    }
    public function accountBalancereport($bank_id=-1)
    {
        $data = array(
            'view' => 'reports/account_balance_report',
            'active' => 'Personal Account',
            'allAccounts' => $this->dbm->retriveTable('bank', 1),
            'bankId' => '',
        );

        $start_date = date('Y-m-01');
        $end_date = date('Y-m-d', time());

        if(isset($_POST['filter'])){
            $start_date = $this->input->post('start_date');
            $end_date = $this->input->post('end_date');
            $bank_id = $this->input->post('bank_id');
        }
        $debit = $this->bkm->getTotalColumnSum($bank_id, 'debit', $start_date)->debit;
        $credit = $this->bkm->getTotalColumnSum($bank_id, 'credit', $start_date)->credit;

        $data['bankId'] = $bank_id;
        $data['start_date'] = $start_date;
        $data['end_date'] = $end_date;
        $data['previous_balance'] = $debit - $credit;

        $accountTransactions = $this->rm->getBankTransactions($bank_id, $start_date, $end_date);
        $data['transactions'] = $accountTransactions;
        
        $this->load->view('main_layouts/headerFooter', $data);
    }
    
    public function dailyCashRegister()
    {   
        $data = array(
            'view' => 'reports/dailyCashReport',
            'active' => 'Cash',
            'BankId' => '',
        );
        $start_date = date('Y-m-01');
        $end_date = date('Y-m-d', time());
        if(isset($_POST['filter'])) {
            $start_date = $this->input->post('start_date');
            $end_date = $this->input->post('end_date');
        }
            $data['start_date'] = $start_date;
            $data['end_date'] = $end_date;


            $cashReceive = $this->Cash_model->getCashWithAccountDetail($start_date, $end_date, 1);//receive
            $bookingReceiving = $this->rm->getBookingTransaction($start_date, $end_date);
            foreach ($bookingReceiving as $br){
                $res['date'] = $br->date;
                $res['detail'] = 'Booking amount from '.$br->customer_name.' ('.$br->serial_no.')'.' -- '.$br->detail;
                $res['amount'] = $br->amount;
                array_push($cashReceive, (object)$res);
            }
            usort($cashReceive, function($a, $b) {
                return strtotime($a->date) - strtotime($b->date);
            });

            $cashPay = $this->Cash_model->getCashWithAccountDetail($start_date, $end_date, 0);//pay

            $data['cash_pay'] = $cashPay;
            $data['cash_receive'] = $cashReceive;
        
        $this->load->view('main_layouts/headerFooter', $data);
    }
    public function totalAccountBalance(){

        $vendors = $this->dbm->retriveTable('bank', 1);
        $totalBalance = 0;
        foreach ($vendors as $ven){
            if($ven->id != get_default_bank_id()){
            
                $debit = $this->rm->getTotalColumnSum($ven->id, 'debit')->debit;
                $credit = $this->rm->getTotalColumnSum($ven->id, 'credit')->credit;
                $debit = isset($debit) ? $debit : 0;
                $credit = isset($credit) ? $credit : 0;
                
                $ven->balance = $debit-$credit;
                $totalBalance += $ven->balance;
            }
        }
        // echo '<pre>';
        // print_r($vendors);die();
  
          $data = array(
              'view' => 'reports/total_account_balance',
              'active' => 'Personal Account',
              'vendors' => $vendors,
              'total_balance' => $totalBalance
          );
          $this->load->view('main_layouts/headerFooter', $data);
      }


}
?>