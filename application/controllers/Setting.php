<?php
class Setting extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Db_model', 'dbm');
        $this->load->model('Setting_model', 'sm');
        if (!($this->session->userdata('admin_logged_in'))) {
            redirect('login');
        }
    }

    public function cashInHand(){

        $id = get_default_bank_id();
        $data = array(
            'view' => 'Setting/cash_in_hand',
            'active' => 'Setting',
        );

        if(isset($_POST['submit'])){
            $balance = $this->input->post('balance');
            $balance = isset($balance) ? $balance : 0;
            $bankData = array('date'=>date('Y-m-d'), 'detail'=>'Cash in hand updated',
                'debit'=>$balance, 'credit'=>0, 'bank_id'=>get_default_bank_id());
            $this->dbm->create('bank_account',$bankData);
        }

        $debit = $this->sm->getTotalBankColumnSum($id, 'debit')->debit;
        $credit = $this->sm->getTotalBankColumnSum($id, 'credit')->credit;
        $debit = isset($debit) ? $debit : 0;
        $credit = isset($credit) ? $credit : 0;
        $data['balance'] = $debit - $credit;

        $this->load->view('main_layouts/headerFooter', $data);
    }

    public function updateProfile(){
        $user  = $this->session->userdata('user');
        $data = array(
            'view' => 'Setting/profile_setting',
            'active' => 'Setting',
            'user'=>$user,
        );

        if(isset($_POST['btn_submit'])){
            $previousPassword = $this->input->post('p_password');
            $newPassword = $this->input->post('n_password');
            $confirmPassword = $this->input->post('c_password');

            $data = array(
                'name'=>$this->input->post('name'),
//                'email'=>$this->input->post('email'),
            );
            $user  = $this->session->userdata('user');
            $password = $this->input->post('p_password');
            if($previousPassword=="" && $newPassword == "" && $confirmPassword == ""){
                $this->dbm->update('users', $user->id, $data);
                $user = $this->dbm->retriveById('users',$user->id);
                $data2 = array(
                    'user' => $user,
                    'admin_logged_in' => TRUE,
                );
                $this->session->set_userdata($data2);
                $this->session->set_flashdata('msg','<p class="alert alert-success">Profile Updated</p>');
                redirect('setting/updateProfile');
            }else{
                if($password != $user->password){
                    $this->session->set_flashdata('msg','<p class="alert alert-danger">Please enter valid current Password</p>');
                    redirect('setting/updateProfile');
                }else{
                    $data['password'] = $this->input->post('n_password');
                    $this->dbm->update('users', $user->id, $data);
                    $user = $this->dbm->retriveById('users',$user->id);
                    $data2 = array(
                        'user' => $user,
                        'admin_logged_in' => TRUE,
                    );
                    $this->session->set_userdata($data2);
                    $this->session->set_flashdata('msg','<p class="alert alert-success">Profile Updated</p>');
                    redirect('setting/updateProfile');

                }


            }
        }

        $this->load->view('main_layouts/headerFooter', $data);
    }


    public function bookingAccount(){
        if(isset($_POST['submit'])){
            $accountId = $this->input->post('booking_account');
            $setting = array('table_id'=> $accountId);
            $this->dbm->update('settings', get_booking_Account()->id, $setting);
            $this->session->set_flashdata('msg','<p class="alert alert-success">Account Updated...</p>');
            redirect('setting/bookingAccount');
        }
        $data = array(
            'view' => 'Setting/booking_account',
            'active' => 'Setting',
            'accounts'=>$this->dbm->retriveTable('bank', 1),
            'booking_account'=>get_booking_Account()
        );
        $this->load->view('main_layouts/headerFooter', $data);
    }



}

?>