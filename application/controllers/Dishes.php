<?php
class Dishes extends CI_Controller
{

  public function __construct(){
        parent::__construct();
        $this->load->model('Db_model','dbm');
        $this->load->model('Dishes_model','dm');
        if(!($this->session->userdata('admin_logged_in'))){
            redirect('login');
        }
    }

  public function allDishes(){
      
    $data = array(
          'view' => 'Dishes/dishes',
          'active' => 'Dishes',
          'allCustomer' => $this->dbm->retriveTable('dishes',1)
        );
        $this->load->view('main_layouts/headerFooter', $data);
  }

  public function addDishes(){
    $data = array(
       'active' => 'Dishes',
       'view' => 'Dishes/add_dishes'
      );
      $this->load->view('main_layouts/headerFooter', $data);
}

  public function saveDishes(){
    $data = array(
        'name' => $this->input->post('name'),
        'no_of_persons_serving' => $this->input->post('no_of_persons_serving'),
        'active' => 1,
    );

    $this->dbm->create('dishes',$data);

    $this->session->set_flashdata('msg','<p class="alert alert-success">Dish Added Successfully</p>');
    redirect('Dishes/addDishes');
  }

  public function updateDishes($id){
    $data = array(
        'view' => 'Dishes/edit_dishes',
        'active' => 'Dishes',
        'update_id' => $id,
        'customer'=>$this->dbm->retriveById('dishes',$id),
      );
      $this->load->view('main_layouts/headerFooter', $data);
  }

  public function saveUpdateDishes(){
    $updateId = $this->input->post('update_id');
    $data = array(
      'name' => $this->input->post('name'),
      'no_of_persons_serving' => $this->input->post('no_of_persons_serving'),
      'active' => 1,
    );
    
    $this->dbm->update('dishes', $updateId, $data);

    $this->session->set_flashdata('msg','<p class="alert alert-success">Dish Updated Successfully</p>');
    redirect('Dishes/allDishes');
  }
  
  public function viewRecipe($id){
   $orderData = $this->dm->retrieveCurrentRecipes($id);
    $data = array(
      'view' => 'Dishes/view_recipe',
      'active' => 'Dishes',
      'dish_id' => $id,
      'raw_materials' => $this->dbm->retriveTable('raw_material',1),
      'recipes' => $orderData,
      // 'orderData'=>$orderData
    );

    $this->load->view('main_layouts/headerFooter', $data);
  }
  public function getUnit($id)
  {
     echo $this->dm->retrieveRawMaterial($id)->receipe_unit;
  }
  public function saveRecipe(){

    $updateId = $this->input->post('update_id');
    $dish_id = $this->input->post('dish_id');
    $qty = $this->input->post('qty');
    $raw_material_id = $this->input->post('raw_material_id');
    // print_r($raw_material_id);

    $this->dm->delete('recipe', $updateId);

    for($i=0; $i< count($this->input->post('raw_material_id')); $i++){
      $newData = array(
        'dish_id' => $dish_id[$i],
        'qty' => $qty[$i],
        'raw_material_id'=> $raw_material_id[$i],
      );
      // print_r($newData);
      $this->dbm->create('recipe',$newData);
    }
    $this->session->set_flashdata('msg','<p class="alert alert-success">Updated Successfully</p>');
    redirect('Dishes/allDishes');
  }
  
  public function deleteDishes($id){
    $data = array('active'=>0);
    $this->dbm->update('dishes', $id, $data);
    redirect('Dishes/allDishes');
  }

  
  public function view_menu(){

      $allMenu = $this->dm->retrieveMenu();
    
    $data = array(
      'active' => 'Dishes',
      'view' => 'Dishes/view_menu',
      'Menu' => $allMenu,
    );
    
    foreach($allMenu as $menu){
      
      $menu->dishes = $this->dm->getMenuItemsOnId($menu->id);
    }
    // echo '<pre>';
    // print_r($data);
    // die();
    $this->load->view('main_layouts/headerFooter', $data);
  }
  public function addMenu(){

    $data = array(
      'active' => 'Dishes',
      'view' => 'Dishes/add_menu',
      'dishes' => $this->dbm->retriveTable('dishes'),
    );
    
    $this->load->view('main_layouts/headerFooter', $data);
  }

  public function saveMenu(){

    $dish_id = $this->input->post('dish_id');
    $menu_name = $this->input->post('menu_name');
    // print_r($raw_material_id);

    
    $menuData = array(
      'name' => $menu_name,
    );
    $menu_id = $this->dbm->createGetId('menu',$menuData);
    
    // $this->dm->deleteMenuItems('menu_items', $menu_id);

    for($i=0; $i< count($dish_id); $i++){
      $menuItemsData = array(
        'dish_id' => $dish_id[$i],
        'menu_id' => $menu_id,
      );
      // print_r($dish_id);
      // print_r($menu_name);
      $this->dbm->create('menu_items',$menuItemsData);
    }
    $this->session->set_flashdata('msg','<p class="alert alert-success">Updated Successfully</p>');
    redirect('Dishes/view_menu');
  }

  function editMenu($id){

    $allMenu = $this->dbm->retriveById('menu',$id);

    $data = array(
      'active' => 'Dishes',
      'view' => 'Dishes/edit_menu',
      'update_id' => $id,
      'dishes' => $this->dbm->retriveTable('dishes'),
      'Menu' => $allMenu,
    );

      $allMenu->dishes = $this->dm->getMenuItemsOnId($allMenu->id);
    
      // echo '<pre>';
      // print_r($data);
      // die();
    $this->load->view('main_layouts/headerFooter', $data);
  } 

  public function updateMenu(){
    $update_id = $this->input->post('update_id');
    $dish_id = $this->input->post('dish_id');
    $menu_name = $this->input->post('menu_name');
    // print_r($raw_material_id);

    
    $menuData = array(
      'name' => $menu_name,
    );
    $this->dbm->update('menu',$update_id,$menuData);
    
    $this->dm->deleteMenuItems('menu_items', $update_id);

    for($i=0; $i< count($dish_id); $i++){
      $menuItemsData = array(
        'dish_id' => $dish_id[$i],
        'menu_id' => $update_id,
      );
      // print_r($dish_id);
      // print_r($menu_name);
      $this->dbm->create('menu_items',$menuItemsData);
    }
    $this->session->set_flashdata('msg','<p class="alert alert-success">Updated Successfully</p>');
    redirect('Dishes/view_menu');
  }

  public function menu_rates(){
    $data = array(
      'active' => 'Dishes',
      'view' => 'Dishes/menu_rates',
      'menuRates' => $this->dm->getMenuRates(),
    );
    $this->load->view('main_layouts/headerFooter', $data);

  }
  public function addMenuRates(){
    $data = array(
      'active' => 'Dishes',
      'view' => 'Dishes/add_menu_rates',
      'Menues' => $this->dbm->retriveTable('menu'),
    );
    $this->load->view('main_layouts/headerFooter', $data);

  }

  public function saveMenuRates(){

    $persons = $this->input->post('persons');
    $rate = $this->input->post('rate');

    $extra = array();
    for($i=0; $i< count($persons); $i++){
      $d = array(
        'persons' => $persons[$i],
        'rate' => $rate[$i],
      );
        array_push($extra, $d);
      }
      $extra = json_encode($extra);
      // echo '<pre>';
      // print_r($extra);die();

    $data = array(
      'menu_id' => $this->input->post('menu_id'),
      'p100' => $this->input->post('p100'),
      'p200' => $this->input->post('p200'),
      'p300' => $this->input->post('p300'),
      'p400' => $this->input->post('p400'),
      'p500' => $this->input->post('p500'),
      'p600' => $this->input->post('p600'),
      'p700' => $this->input->post('p700'),
      'extra_rate' => $extra,
    );

    $this->dbm->create('menu_rate',$data);
    $this->session->set_flashdata('msg','<p class="alert alert-success">Created Successfully</p>');
    redirect('Dishes/menu_rates');

  }
  public function editMenuRates($id){
    $data = array(
      'active' => 'Dishes',
      'view' => 'Dishes/edit_menu_rates',
      'Menu' => $this->dbm->retriveTable('menu'),
      'oldData' => $this->dm->getMenuRatesOnId($id),
    );
    
    $this->load->view('main_layouts/headerFooter', $data);
  }

  public function updateMenuRates(){

    $persons = $this->input->post('persons');
    $rate = $this->input->post('rate');

    $extra = array();
    for($i=0; $i< count($persons); $i++){
      $d = array(
        'persons' => $persons[$i],
        'rate' => $rate[$i],
      );
        array_push($extra, $d);
      }
      $extra = json_encode($extra);


    $update_id = $this->input->post('update_id'); 
    $data = array(
      'menu_id' => $this->input->post('menu_id'),
      'p100' => $this->input->post('p100'),
      'p200' => $this->input->post('p200'),
      'p300' => $this->input->post('p300'),
      'p400' => $this->input->post('p400'),
      'p500' => $this->input->post('p500'),
      'p600' => $this->input->post('p600'),
      'p700' => $this->input->post('p700'),
      'extra_rate' => $extra,
    );

    $this->dbm->update('menu_rate',$update_id,$data);
    $this->session->set_flashdata('msg','<p class="alert alert-success">Updated Successfully</p>');
    redirect('Dishes/menu_rates');

  }

  public function deleteMenu($id){
      $data = array('active' => 0);
      $this->dbm->update('menu', $id, $data);
      redirect('Dishes/view_menu');
  }








}