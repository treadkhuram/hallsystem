<?php

class Employees extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Vandor_model', 'vm');
        $this->load->model('Db_model', 'dbm');
        if (!($this->session->userdata('admin_logged_in'))) {
            redirect('login');
        }
    }


    public function addEmployees()
    {
        $data = array(
            'active' => 'Employees',
            'view' => 'Employees/add_employees'
        );
        $this->load->view('main_layouts/headerFooter', $data);
    }

    public function Employees()
    {
        $data = array(
            'active' => 'Employees',
            'view' => 'Employees/view_employees',
            'employees' => $this->dbm->retriveTable('employees', 1)

        );
        $this->load->view('main_layouts/headerFooter', $data);
    }

    public function employeesDetail()
    {
        $data = array(
            'active' => 'Employees',
            'view' => 'Employees/employee_detail',
            'employees' => $this->dbm->retriveTable('employees', 1)

        );

        if (isset($_POST['filter'])) {
            $employee_id = $this->input->post('employee_id');
            $start_date = $this->input->post('start_date');
            $end_date = $this->input->post('end_date');

            $newData = $this->vm->getSupplyDataEmployee($start_date, $end_date, $employee_id);
            $data['employeeData'] = $newData;
            $data['employee'] = $this->dbm->retriveById('employees', $employee_id);
            $data['employee_id']=$employee_id;
            $data['start_date']=$start_date;
            $data['end_date']=$end_date;

        }
        $this->load->view('main_layouts/headerFooter', $data);
    }


    public function saveEmployees()
    {
        $data = array(
            'name' => $this->input->post('name'),
            'phone' => $this->input->post('phone'),
            'city' => $this->input->post('city'),
            'address' => $this->input->post('address'),
            'designation' => $this->input->post('designation'),
            'salary' => $this->input->post('salary'),
            'allow_holyday' => $this->input->post('holiday'),
            'fine_per_day' => $this->input->post('fine'),
            'active' => 1,
        );

        $this->dbm->create('employees', $data);

        $this->session->set_flashdata('msg', '<p class="alert alert-success">Employee Added Successfully</p>');
        redirect('Employees/addEmployees');
    }


    public function deleteEmployee($id)
    {
        $data = array('active' => 0);
        $this->dbm->update('employees', $id, $data);
        redirect('Employees/Employees');
    }

    public function updateEmployees($id)
    {
        $data = array(
            'view' => 'Employees/edit_employees',
            'active' => 'Employees',
            'update_id' => $id,
            'customer' => $this->dbm->retriveById('employees', $id),
        );
        $this->load->view('main_layouts/headerFooter', $data);
    }

    public function saveUpdateEmployees()
    {
        $updateId = $this->input->post('update_id');
        $data = array(
            'name' => $this->input->post('name'),
            'phone' => $this->input->post('phone'),
            'city' => $this->input->post('city'),
            'address' => $this->input->post('address'),
            'designation' => $this->input->post('designation'),
            'salary' => $this->input->post('salary'),
            'allow_holyday' => $this->input->post('holiday'),
            'fine_per_day' => $this->input->post('fine'),
            'active' => 1,
        );

        $this->dbm->update('employees', $updateId, $data);

        $this->session->set_flashdata('msg', '<p class="alert alert-success">Employee Updated Successfully</p>');
        redirect('Employees/Employees');
    }

}