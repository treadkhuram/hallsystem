<?php

class Cash extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Cash_model', 'cm');
        $this->load->model('Bank_model', 'bm');
        $this->load->model('Db_model', 'dbm');
        if (!($this->session->userdata('admin_logged_in'))) {
            redirect('login');
        }
    }

    public function addCash()
    {
        $data = array(
            'active' => 'Cash',
            'view' => 'Cash/add_cash'
        );
        $this->load->view('main_layouts/headerFooter', $data);
    }

    public function viewCash()
    {
        $start_date = date('Y-m-01');
        $end_date = date('Y-m-d');
        if(isset($_POST['filter'])){
            $start_date = $this->input->post('start_date');
            $end_date = $this->input->post('end_date');
        }
        $data = array(
            'active' => 'Cash',
            'view' => 'Cash/view_cash',
            'cash' => $this->cm->get_cash($start_date, $end_date),
            'start_date'=>$start_date,
            'end_date'=>$end_date
        );
        $this->load->view('main_layouts/headerFooter', $data);
    }


    public function getAccounts(){
        extract($_POST);

        if ($account == "vendor") {
            $accountsData = $this->dbm->retriveTable('vandors', 1);
        } else if($account == 'employee') {
            $accountsData = $this->dbm->retriveTable('employees', 1);
        }else if($account == 'bank'){
            $temp = $this->dbm->retriveTable('bank', 1);
            $accountsData = array();
            foreach ($temp as $t){
                if($t->id != get_default_bank_id())
                    array_push($accountsData, $t);
            }
        }
        if (!empty($accountsData)) {
            echo(json_encode($accountsData));
        }
    }

    function add_cash()
    {
        $cashType = $this->input->post('update_type');
        $accountTypes = $this->input->post('accountType');
        $details = $this->input->post('detail');
        $amounts = $this->input->post('amount');
        $vendors = $this->input->post('customer_id');
        $referances = $this->input->post('referance');
        $dates = $this->input->post('date');

        foreach ($accountTypes as $key => $at) {
            $detail = $details[$key];
            $amount = $amounts[$key];
            $vendor = $vendors[$key];
            $date = $dates[$key];
            $voucherNo = $referances[$key];
            $cashData = array('cash_type' => $cashType, 'account_type' => $at, 'amount' => $amount,
                'date' => $date, 'detail' => $detail, 'voucher_no' => $voucherNo, 'bank_account_id' => get_default_bank_id(),
                'paid_person_id' => $vendor);
            $cashId = $this->dbm->createGetId('cash', $cashData);
//            $this->cm->insertDefaultBankAccount($date, $detail, $amount, get_default_bank_id(), $cashId, $cashType);

            if ($at == 'vendor') {
                $vendorData = array('bill_book_no' => $voucherNo, 'detail' => $detail, 'date' => $date,
                    'vendor_id'=>$vendor, 'total_amount' =>$amount, 'amount_type' => 1, 'cash_id' => $cashId);
                if ($cashType == 0) {//pay
                    $vendorData['credit'] = 0;
                    $vendorData['debit'] = $amount;
                } else { //receive
                    $vendorData['credit'] = $amount;
                    $vendorData['debit'] = 0;
                }
                $this->dbm->create('vendors_supply', $vendorData);
            } else if ($at == 'employee') {
                $vendorData = array('detail' => $detail, 'date' => $date, 'total_amount' => $amount,
                    'cash_id' => $cashId, 'employee_id' => $vendor);
                $this->dbm->create('employee_account', $vendorData);
            }else if ($at == 'bank') {
                if ($cashType == 0) {//pay
                     $newBankData = array('date'=>$date, 'detail'=>$detail,
                         'debit'=>$amount, 'credit'=>0, 'bank_id'=>$this->input->post('vendor_id'), 'cash_id'=>$cashId);
                     $this->dbm->create('bank_account',$newBankData);
                } else { //receive
                     $newBankData = array('date'=>$date, 'detail'=>$detail,
                         'debit'=>0, 'credit'=>$amount, 'bank_id'=>$this->input->post('vendor_id'), 'cash_id'=>$cashId);
                     $this->dbm->create('bank_account',$newBankData);
                }
            }
        }
        $this->session->set_flashdata('msg', '<p class="alert alert-success">Cash Transaction Successfully</p>');
        redirect('Cash/addCash');

    }


    public function cashDelete($id)
    {
        $cash = $this->dbm->retriveById('cash', $id);
        if($cash->account_type == 'vendor'){
            $this->dbm->deleteByFk('vendors_supply','cash_id', $id);
        }else if($cash->account_type == 'employee'){
            $this->dbm->deleteByFk('employee_account','cash_id', $id);
        }else if($cash->account_type == 'bank'){
            $this->dbm->deleteByFk('bank_account','cash_id', $id);
        }
        $this->dbm->delete('cash',$id);
        $this->session->set_flashdata('msg', '<p class="alert alert-success">Cash has been deleted successfully</p>');

        redirect('Cash/viewCash');
    }

    public function updateCash($id)
    {
        $cash = $this->dbm->retriveById('cash', $id);
        $vendorData = $this->cm->getCashById($id, $cash->account_type);
        if ($vendorData->account_type == "vendor") {
            $accounts = $this->dbm->retriveTable('vandors', 1);
        } else if($vendorData->account_type == "employee") {
            $accounts = $this->dbm->retriveTable('employees', 1);
        }
        else if($vendorData->account_type == "bank") {
            $accounts = $this->dbm->retriveTable('bank', 1);
        }
        $data = array(
            'view' => 'Cash/update_cash',
            'active' => 'Cash',
            'vendor' => $vendorData,
            'accounts'=>$accounts,
        );
        $this->load->view('main_layouts/headerFooter', $data);
    }

    public function saveUpdateCash()
    {
        $updateCashId = $this->input->post('update_id');
        $accountType = $this->input->post('account_type');
        $previousCash = $this->dbm->retriveById('cash', $updateCashId);

        $accountId = $this->input->post('vendor_id');
        $totalAmount = $this->input->post('amount');
        $newDate = $this->input->post('current_date');
        $newDetail = $this->input->post('det');
        $cashType = $this->input->post('update_type');

        $data = array('date' => $newDate, 'detail' => $newDetail, 'amount' => $totalAmount, 'paid_person_id'=>$accountId);
        $this->dbm->update('cash', $updateCashId, $data);

        if($accountType == 'vendor'){
            $this->cm->updateBankAccountCash($updateCashId, $totalAmount, $newDate, $newDetail, $cashType);
            $newData = array('total_amount' => $totalAmount, 'debit' => $totalAmount, 'vendor_id'=>$accountId);
            if ($cashType == 1) { // receive
                $newData = array('total_amount' => $totalAmount,'credit' => $totalAmount, 'vendor_id'=>$accountId);
            }
            $this->dbm->updateByFk('vendors_supply', 'cash_id', $updateCashId, $newData);
        }else if($accountType == 'employee'){
            $this->cm->updateBankAccountCash($updateCashId, $totalAmount, $newDate, $newDetail, $cashType);
            $newData = array('total_amount' => $totalAmount, 'vendor_id'=>$accountId);
            $this->dbm->updateByFk('employee_account','cash_id', $updateCashId, $newData);
        }else if ($accountType == 'bank') {
            $this->cm->updateBankAccountCash($updateCashId, $totalAmount, $newDate, $newDetail, $cashType, get_default_bank_id());
            $previousBankAccount = $this->bm->getBankAccountByCashId($accountId, $updateCashId);

            if ($cashType == 0) {//pay
                $newBankData = array('date'=>$newDate, 'detail'=>$newDetail,
                    'debit'=>$totalAmount, 'credit'=>0, 'bank_id'=>$accountId);
                $this->dbm->update('bank_account', $previousBankAccount->id, $newBankData);
            } else { //receive
                $newBankData = array('date'=>$newDate, 'detail'=>$newDetail,
                    'debit'=>0, 'credit'=>$totalAmount, 'bank_id'=>$accountId);
                $this->dbm->update('bank_account', $previousBankAccount->id, $newBankData);
            }
        }

        $this->session->set_flashdata('msg', '<p class="alert alert-success">Updated Successfully</p>');
        redirect('Cash/ViewCash');
    }


}