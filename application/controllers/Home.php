<?php
class Home extends CI_Controller
{

  public function __construct(){
        parent::__construct();
        $this->load->model('Db_model','dbm');
        $this->load->model('Driver_model','dm');
        $this->load->model('Cash_model');
        $this->load->model('Reports_model','rm');
        if(!($this->session->userdata('admin_logged_in'))){
            redirect('login');
        }
    }

     public function index(){
      $pending = $this->db->select('*')->from('booking')->where('status', 0)->where('active', 1)->count_all_results();
      $completed = $this->db->select('*')->from('booking')->where('status', 1)->where('active', 1)->count_all_results();
      $canceled = $this->db->select('*')->from('booking')->where('status', -1)->where('active', 1)->count_all_results();
      $todayBooking = $this->db->select('*')->from('booking')->where('status', 0)->where('function_date', date('Y-m-d'))->where('active', 1)->count_all_results();

      $totalVendors = $this->db->select('*')->from('vandors')->where('active',1)->count_all_results();
      $totalDishes = $this->db->select('*')->from('dishes')->where('active',1)->count_all_results();
      $totalMenus = $this->db->select('*')->from('menu')->where('active',1)->count_all_results();
      $totalRawMaterial = $this->db->select('*')->from('raw_material')->where('active',1)->count_all_results();

      $accounts = $this->dbm->retriveTable('bank', 1);

         $month_start_date = date('Y-m-01');
         $today_date = date('Y-m-d', time());

         $currentMonthAmountReceive = 0;
         $currentMonthAmountPay = 0;
         $cashReceive = $this->Cash_model->getCashWithAccountDetail($month_start_date, $today_date, 1);//receive
         $bookingReceiving = $this->rm->getBookingTransaction($month_start_date, $today_date);

         foreach ($cashReceive as $cr){
             $currentMonthAmountReceive += $cr->amount;
         }
         foreach ($bookingReceiving as $br){
             $currentMonthAmountReceive += $br->amount;
         }

         $cashPay = $this->Cash_model->getCashWithAccountDetail($month_start_date, $today_date, 0);//pay
         foreach ($cashPay as $cp){
             $currentMonthAmountPay += $cp->amount;
         }

         $lastSevenDayDate = date('Y-m-d', strtotime('-7 days'));
         $sevenDayAmountReceive = 0;
         $sevenDayAmountPay = 0;
         $cashReceive = $this->Cash_model->getCashWithAccountDetail($lastSevenDayDate, $today_date, 1);//receive
         $bookingReceiving = $this->rm->getBookingTransaction($lastSevenDayDate, $today_date);
         foreach ($cashReceive as $cr){
             $sevenDayAmountReceive += $cr->amount;
         }
         foreach ($bookingReceiving as $br){
             $sevenDayAmountReceive += $br->amount;
         }
         $cashPay = $this->Cash_model->getCashWithAccountDetail($lastSevenDayDate, $today_date, 0);//pay
         foreach ($cashPay as $cp){
             $sevenDayAmountPay += $cp->amount;
         }

         $todayAmountReceived = 0;
         $todayAmountPay = 0;
         $cashReceive = $this->Cash_model->getCashWithAccountDetail($today_date, $today_date, 1);//receive
         $bookingReceiving = $this->rm->getBookingTransaction($today_date, $today_date);
         foreach ($cashReceive as $cr){
             $todayAmountReceived += $cr->amount;
         }
         foreach ($bookingReceiving as $br){
             $todayAmountReceived += $br->amount;
         }
         $cashPay = $this->Cash_model->getCashWithAccountDetail($today_date, $today_date, 0);//pay
         foreach ($cashPay as $cp){
             $todayAmountPay += $cp->amount;
         }


         $totalSalary = 0;
         $paidSalary = 0;
         $allEmp = $this->dbm->retriveTable('employees',1);
         foreach($allEmp as $em){
             $paid_amount = 0;
             $trans = $this->rm->getAllSaleryTransaction($em->id, $month_start_date, $today_date);
             foreach($trans as $t){
                 $paid_amount += $t->total_amount;
             }
             $totalSalary += $em->salary;
             $paidSalary += $paid_amount;
         }

        $data = array(
           'pending'=>$pending,
           'completed'=>$completed,
           'canceled'=>$canceled,
           'todayBooking'=>$todayBooking,
           'totalVendors'=> $totalVendors,
           'totalDishes'=> $totalDishes,
           'totalMenus'=> $totalMenus,
           'totalRawMaterial'=> $totalRawMaterial,
           'todayAmountReceive'=>$todayAmountReceived,
           'todayAmountPay'=>$todayAmountPay,
           'active' => 'dashboard',
           'view' => 'dashboard_view',
            'accounts' => $accounts,
            'month_amount_receive'=>$currentMonthAmountReceive,
            'month_amount_pay'=>$currentMonthAmountPay,
            'seven_day_amount_receive'=>$sevenDayAmountReceive,
            'seven_day_amount_pay'=>$sevenDayAmountPay,
            'total_employee_salary'=>$totalSalary,
            'paid_employee_salary'=>$paidSalary
        );
          $this->load->view('main_layouts/headerFooter', $data);
  }


}

?>