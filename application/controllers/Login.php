<?php


class Login extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Auth_model','am');

    }

    public function index()
    {
        $this->session->set_userdata('site_lang', "english");
        $this->load->view('login_view');
    }

    public function authentication()
    {

        $this->form_validation->set_rules('username','Username','required|trim');
        $this->form_validation->set_rules('password','Password','required|trim');

        if ($this->form_validation->run() == FALSE)
        {
            $data = array(
                'msg' => validation_errors('<p class="alert alert-danger">','</p>')
            );

            $this->session->set_flashdata($data);

            redirect('login');
        }

        else{
            $username = $this->input->post('username');
            $password = $this->input->post('password');

            if($this->am->loginUser($username, $password)){
                $user = $this->am->loginUser($username, $password);
                $data = array(
                    'user' => $user,
                    'admin_logged_in' => TRUE,
                );
//               echo '<pre>';
//               print_r($data);
//               die();
                $this->session->set_userdata('admin_logged_in', TRUE);
                $this->session->set_userdata('user', $user);
                $this->session->set_userdata($data);
                $this->session->set_flashdata('msg','<p class="alert alert-success">Login Success</p>');
                redirect('home');
            }else{
                $data = array('msg' => '<p class="alert alert-danger">Incorrect Username or Password</p>');
                $this->session->set_flashdata($data);
                redirect('login');
            }
        }

    }

    public function logout()
    {
        $this->session->unset_userdata('admin_logged_in');
        redirect('login');
    }
}
