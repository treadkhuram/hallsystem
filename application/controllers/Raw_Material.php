<?php
class Raw_Material extends CI_Controller
{

  public function __construct(){
        parent::__construct();
        $this->load->model('Db_model','dbm');
        $this->load->model('Customer_model','cm');
        $this->load->model('RawMaterial_model', 'rm');
        if(!($this->session->userdata('admin_logged_in'))){
            redirect('login');
        }
    }

  public function allRawMaterials(){

    $data = array(
          'view' => 'Raw_Material/rawMaterials',
          'active' => 'Raw_Materials',
          'allCustomer' => $this->rm->retriveTable('raw_material',1),
        );
        $this->load->view('main_layouts/headerFooter', $data);
  }

  public function addRawMaterial(){
    $data = array(
       'active' => 'Raw_Materials',
       'view' => 'Raw_Material/add_rawMaterial',
        'Units' => $this->dbm->retriveTable('unit',1),
      );
      $this->load->view('main_layouts/headerFooter', $data);
}

  public function saveRawMaterial(){
    $data = array(
        'name' => $this->input->post('name'),
        'unit' => $this->input->post('unit'),
        'purchase_price' => $this->input->post('purchase_price'),
        'qty' => $this->input->post('qty'),
        'alert_qty' => $this->input->post('alert_qty'),
        'active' => 1,
    );

    $this->dbm->create('raw_material',$data);

    $this->session->set_flashdata('msg','<p class="alert alert-success">Raw Material Added Successfully</p>');
    redirect('Raw_Material/addRawMaterial');
  }

  public function updateRawMaterial($id){
    $data = array(
        'view' => 'Raw_Material/edit_rawMaterial',
        'active' => 'Raw_Material',
        'Units' => $this->dbm->retriveTable('unit',1),
        'update_id' => $id,
        'customer'=>$this->dbm->retriveById('raw_material',$id),
      );
      $this->load->view('main_layouts/headerFooter', $data);
  }

  public function saveUpdateRawMaterial(){
    $updateId = $this->input->post('update_id');
    $data = array(
      'name' => $this->input->post('name'),
        'unit' => $this->input->post('unit'),
        'purchase_price' => $this->input->post('purchase_price'),
        'qty' => $this->input->post('qty'),
        'alert_qty' => $this->input->post('alert_qty'),
        'active' => 1,
    );
    
    $this->dbm->update('raw_material', $updateId, $data);

    $this->session->set_flashdata('msg','<p class="alert alert-success">Raw Material Updated Successfully</p>');
    redirect('Raw_Material/allRawMaterials');
  }
  
  // public function customerDetail($id){
  //   $orderData = $this->cm->getCustomerOrder($id);
  //   $data = array(
  //     'view' => 'Vandors/customer_detail_view',
  //     'active' => 'driver',
  //     'customer'=>$this->dbm->retriveById('customers',$id),
  //     'orderData'=>$orderData
  //   );
  //   $this->load->view('main_layouts/headerFooter', $data);
  // }
  
  public function deleteRawMaterial($id){
    $data = array('active'=>0);
    $this->dbm->update('raw_material', $id, $data);
    redirect('Raw_Material/allRawMaterials');
  }

  

}