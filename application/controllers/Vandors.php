<?php

class Vandors extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Db_model', 'dbm');
        $this->load->model('Vandor_model', 'vm');
        $this->load->model('RawMaterial_model', 'rm');
        if (!($this->session->userdata('admin_logged_in'))) {
            redirect('login');
        }
    }

    public function allVandors()
    {

        $data = array(
            'view' => 'Vandors/vandors',
            'active' => 'Vandors',
            'allCustomer' => $this->dbm->retriveTable('vandors', 1)
        );
        $this->load->view('main_layouts/headerFooter', $data);
    }


    public function viewSupply()
    {

        $data = array(
            'view' => 'Vandors/view_supply',
            'active' => 'Vandors',
            'allVandors' => $this->dbm->retriveTable('vandors', 1),
        );

        if (isset($_POST['filter'])) {
            $vandor_id = $this->input->post('vandor_id');
            $start_date = $this->input->post('start_date');
            $end_date = $this->input->post('end_date');

            $newData = $this->vm->getSupplyData($start_date, $end_date, $vandor_id);
            $debit = $this->vm->getTotalColumnSum($vandor_id, 'debit', $start_date)->debit;
            $credit = $this->vm->getTotalColumnSum($vandor_id, 'credit', $start_date)->credit;
            $previousBalance =$credit - $debit;
            $data['previous_balance'] = $previousBalance;
            $data['SupplyData'] = $newData;
            $data['start_date'] = $start_date;
            $data['end_date'] = $end_date;
            $data['vendor_id'] = $vandor_id;
        }

        $this->load->view('main_layouts/headerFooter', $data);
    }

    public function deleteSupply($id)
    {
        $this->dbm->delete('vendors_supply', $id);
        $this->dbm->deleteByFk('vendor_supply_data', 'vendors_supply_id', $id);
        redirect('Vandors/viewSupply');

    }

    public function addSupply()
    {

        $data = array(
            'view' => 'Vandors/add_supply',
            'active' => 'Vandors',
            'allVandors' => $this->dbm->retriveTable('vandors', 1),
            'allProducts' => $this->dbm->retriveTable('raw_material', 1),

        );
        $this->load->view('main_layouts/headerFooter', $data);
    }

    public function addVandor()
    {
        $data = array(
            'active' => 'Vandors',
            'view' => 'Vandors/add_vandors'
        );
        $this->load->view('main_layouts/headerFooter', $data);
    }

    public function saveVandor()
    {
        $data = array(
            'name' => $this->input->post('name'),
            'phone' => $this->input->post('phone'),
            'city' => $this->input->post('city'),
            'address' => $this->input->post('address'),
            'active' => 1,
        );

        $this->dbm->create('vandors', $data);

        $this->session->set_flashdata('msg', '<p class="alert alert-success">Vandor Added Successfully</p>');
        redirect('Vandors/addVandor');
    }

    public function updateVandors($id)
    {
        $data = array(
            'view' => 'Vandors/edit_vandors',
            'active' => 'Vandors',
            'update_id' => $id,
            'customer' => $this->dbm->retriveById('vandors', $id),
        );
        $this->load->view('main_layouts/headerFooter', $data);
    }

    public function saveUpdateVandor()
    {
        $updateId = $this->input->post('update_id');
        $data = array(
            'name' => $this->input->post('name'),
            'phone' => $this->input->post('phone'),
            'city' => $this->input->post('city'),
            'address' => $this->input->post('address'),
            'active' => 1,
        );

        $this->dbm->update('vandors', $updateId, $data);

        $this->session->set_flashdata('msg', '<p class="alert alert-success">Vandor Updated Successfully</p>');
        redirect('Vandors/allVandors');
    }

    // public function customerDetail($id){
    //   $orderData = $this->cm->getCustomerOrder($id);
    //   $data = array(
    //     'view' => 'Vandors/customer_detail_view',
    //     'active' => 'driver',
    //     'customer'=>$this->dbm->retriveById('customers',$id),
    //     'orderData'=>$orderData
    //   );
    //   $this->load->view('main_layouts/headerFooter', $data);
    // }

    public function deleteVandor($id)
    {
        $data = array('active' => 0);
        $this->dbm->update('vandors', $id, $data);
        redirect('Vandors/allVandors');
    }

    function selectData()
    {
        $id = $this->input->post('id');
        $data = $this->dbm->retriveById('raw_material', $id);
        //  $data->purchase_price;
        $newArray = array(
            'name' => $data->name,
            'price' => $data->purchase_price,
            'qty' => $data->qty,
        );
        echo $array = json_encode($newArray);
    }

    function add_supplyData()
    {

        $data = array(
            'bill_book_no' => $this->input->post('bill_book_number'),
            'detail' => $this->input->post('detail'),
            'date' => $this->input->post('date'),
            'credit' => $this->input->post('total_amount'),
            'debit' => 0,
            'total_amount' => $this->input->post('total_amount'),
            'vendor_id' => $this->input->post('vandor_id'),
        );
        $vender_supply_id = $this->dbm->createGetId('vendors_supply', $data);

        $prod_id = $this->input->post('product_id');
        $prod_qty = $this->input->post('product_qty');
        $prod_price = $this->input->post('product_price');

        for ($i = 0; $i < count($prod_id); $i++) {
            $ProdData = array(
                'product_id' => $prod_id[$i],
                'qty' => $prod_qty[$i],
                'price' => $prod_price[$i],
                'vendors_supply_id' => $vender_supply_id,
            );
            $this->dbm->create('vendor_supply_data', $ProdData);
            $this->rm->addRawMaterial($prod_id[$i], $prod_qty[$i], $prod_price[$i]);


        }

        $this->session->set_flashdata('msg', '<p class="alert alert-success">Updated Successfully</p>');
        redirect('Vandors/allVandors');


    }


}