<?php
class Unit extends CI_Controller
{

  public function __construct(){
        parent::__construct();
        $this->load->model('Db_model','dbm');
        $this->load->model('Customer_model','cm');
        if(!($this->session->userdata('admin_logged_in'))){
            redirect('login');
        }
    }

  public function allUnit(){

    $data = array(
          'view' => 'Setting/Units/units',
          'active' => 'Setting',
          'allCustomer' => $this->dbm->retriveTable('unit',1)
        );
        $this->load->view('main_layouts/headerFooter', $data);
  }

  public function addUnit(){
    $data = array(
       'active' => 'Setting',
       'view' => 'Setting/Units/add_unit'
      );
      $this->load->view('main_layouts/headerFooter', $data);
}

  public function saveUnit(){
    $data = array(
        'measure_unit' => $this->input->post('measure_unit'),
        'receipe_unit' => $this->input->post('receipe_unit'),
        'converstion_factor' => $this->input->post('converstion_factor'),
        'active' => 1,
    );

    $this->dbm->create('unit',$data);

    $this->session->set_flashdata('msg','<p class="alert alert-success">Unit Added Successfully</p>');
    redirect('Unit/addUnit');
  }

  public function updateUnit($id){
    $data = array(
        'view' => 'Setting/Units/edit_unit',
        'active' => 'Setting',
        'update_id' => $id,
        'customer'=>$this->dbm->retriveById('unit',$id),
      );
      $this->load->view('main_layouts/headerFooter', $data);
  }

  public function saveUpdateUnit(){
    $updateId = $this->input->post('update_id');
    $data = array(
        'measure_unit' => $this->input->post('measure_unit'),
        'receipe_unit' => $this->input->post('receipe_unit'),
        'converstion_factor' => $this->input->post('converstion_factor'),
        'active' => 1,
    );
    
    $this->dbm->update('unit', $updateId, $data);

    $this->session->set_flashdata('msg','<p class="alert alert-success">Unit Updated Successfully</p>');
    redirect('Unit/allUnit');
  }
  
  // public function customerDetail($id){
  //   $orderData = $this->cm->getCustomerOrder($id);
  //   $data = array(
  //     'view' => 'Vandors/customer_detail_view',
  //     'active' => 'driver',
  //     'customer'=>$this->dbm->retriveById('customers',$id),
  //     'orderData'=>$orderData
  //   );
  //   $this->load->view('main_layouts/headerFooter', $data);
  // }
  
  public function deleteUnit($id){
    $data = array('active'=>0);
    $this->dbm->update('unit', $id, $data);
    redirect('Unit/allUnit');
  }

  

}