<?php

class Dishes_model extends CI_Model{

    public function retrieveRawMaterial($id){
    
        $this->db->select('b.receipe_unit');
        $this->db->from('raw_material a');
        $this->db->join('unit b', 'b.id = a.unit');
        $this->db->where('a.id',$id);
        return $this->db->get()->row();
    }
    public function retrieveMenu(){
    
        $this->db->select('a.*');
        $this->db->from('menu a');
        $this->db->order_by('a.name', 'asc');
        $this->db->where('a.active',1);

        // $this->db->join('menu b', 'b.id = a.menu_id');
        // $this->db->join('dishes c', 'c.id = a.dish_id');
        return $this->db->get()->result();
    }
    public function retrieveMenuOnId($id){
    
        $this->db->select('a.*');
        $this->db->from('menu a');
        $this->db->where('a.id',$id);
        return $this->db->get()->result();
    }

    public function getMenuItemsOnId($id){

        $this->db->select('a.*, b.name as dish_name, b.no_of_persons_serving');
        $this->db->from('menu_items a');
        $this->db->join('dishes b','b.id = a.dish_id');
        $this->db->where('a.menu_id', $id);
        return $this->db->get()->result();
    }


    public function retrieveCurrentRecipes($id){

        $this->db->select('a.*, b.name as product_name, c.name as dish_name, d.receipe_unit as raw_unit');
        $this->db->from('recipe a');
        $this->db->join('raw_material b','b.id = a.raw_material_id');
        $this->db->join('dishes c','c.id = a.dish_id');
        $this->db->join('unit d','d.id = b.unit');
        $this->db->where('c.id',$id);
        return $this->db->get()->result();
    }

    public function delete($table, $id){
        $this->db->where("dish_id",$id);
        $this->db->delete($table); 
      }
    public function deleteMenuItems($table, $id){
        $this->db->where("menu_id",$id);
        $this->db->delete($table); 
      }

      function getMenuRates(){

        $this->db->select('a.*, b.name as menu_name');
        $this->db->from('menu_rate a');
        $this->db->join('menu b',' b.id = a.menu_id');
        return $this->db->get()->result();
      }
     
      function getMenuRatesOnId($id){

        $this->db->select('a.*, b.name as menu_name');
        $this->db->from('menu_rate a');
        $this->db->join('menu b',' b.id = a.menu_id');
        $this->db->where('b.id', $id);
        $res = $this->db->get();
//        if($res->num_rows() > 0)
            return $res->row();
//        return 0;
      }

      public function getDishCost($dishId){
        $this->db->select('rm.*, r.qty as recipe_qty, u.converstion_factor, u.receipe_unit');
        $this->db->from('recipe r');
        $this->db->join('raw_material rm','r.raw_material_id=rm.id');
        $this->db->join('unit u', 'rm.unit=u.id');
        $this->db->where('r.dish_id',$dishId);
        return $this->db->get()->result();
      }






}?>