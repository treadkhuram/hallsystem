<?php

class Cash_model extends CI_Model
{

    function insertCash($TableName, $values)
    {
        $this->db->insert($TableName, $values);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }

    public function get_cash($startDate, $endDate, $cashType = -1)
    {
        $cashType;
        $this->db->select('*');
            $this->db->from('cash');
            $this->db->where('date>=', $startDate);
            $this->db->where('date<=', $endDate);
        if($cashType !=-1) {

            $this->db->where('cash_type', $cashType);
        }
        return $this->db->get()->result();
    }

    public function cashDelBySupply($id)
    {
        $this->db->where('cash_id', $id);
        $this->db->delete('vendors_supply');

    }

    public function getCashById($id, $accountType=''){
        $this->db->select('a.*, b.name as vendor_name, b.id as vendor_id');
        $this->db->from('cash a');
        if($accountType == 'vendor')
            $this->db->join('vandors b', 'b.id = a.paid_person_id');
        else if($accountType == 'employee')
            $this->db->join('employees b', 'b.id = a.paid_person_id');
        else if($accountType == 'bank')
            $this->db->join('bank b', 'b.id = a.paid_person_id');
        $this->db->where('a.id', $id);
        return $this->db->get()->row();
    }

    public function updateBankAccountCash($cashId, $totalAmount, $date, $detail, $cashType, $bankId=''){
        $newBankAccount = array('credit'=>$totalAmount, 'debit'=>0, 'date'=>$date, 'detail'=>'(Pay) '.$detail);
        if ($cashType == 1) { // receive
            $newBankAccount = array('credit'=>0, 'debit'=>$totalAmount,
                'date'=>$date, 'detail'=>'(Receive) '.$detail);
        }
        $this->db->where('cash_id', $cashId);
        if($bankId !='')
            $this->db->where('bank_id', $bankId);
        return $this->db->update('bank_account', $newBankAccount);
    }

    public function insertDefaultBankAccount($date, $detail, $amount, $bankId, $cashId, $cashType){
        $bankData = array('date'=>$date, 'detail'=>'(Receive) '.$detail,
            'debit'=>$amount, 'credit'=>0, 'bank_id'=>get_default_bank_id(), 'cash_id'=>$cashId);
        if ($cashType == 0) {//pay
            $bankData = array('date'=>$date, 'detail'=>'(Pay) '.$detail,
                'debit'=>0, 'credit'=>$amount, 'bank_id'=>get_default_bank_id(), 'cash_id'=>$cashId);
        }
        $this->db->insert('bank_account', $bankData);
    }

    public function getCashWithAccountDetail($startDate, $endDate, $cashType){
        $cash = $this->get_cash($startDate, $endDate, $cashType);
        $this->db->last_query();
        $cashArray = array();
        foreach ($cash as $ca){
            $detail = $cashType == 0 ? 'Cash Pay ' : 'Cash Receive ';
            if($ca->account_type == 'vendor'){
                $vendor = $this->db->select('*')->from('vandors')
                    ->where('id', $ca->paid_person_id)->get()->row();
                $detail .=  $vendor->name .' (Vendor)';
            }else if($ca->account_type == 'employee'){
                $employee = $this->db->select('*')->from('employees')
                    ->where('id', $ca->paid_person_id)->get()->row();
                $detail .= $employee->name.' (Employee)';
            }else if($ca->account_type == 'bank'){
                $bank  = $this->db->select('*')->from('bank')
                    ->where('id', $ca->paid_person_id)->get()->row();
                $detail .= $bank->name.' (Account)';
            }
            $res['date'] = $ca->date;
            $res['detail'] = $detail. ' - '.$ca->detail;
            $res['amount'] = $ca->amount;
            array_push($cashArray, (object)$res);
        }
        return $cashArray;
    }



}

?>