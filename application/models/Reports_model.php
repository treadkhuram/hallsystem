<?php

class Reports_model extends CI_Model{

 function getBookingReport($stats, $strDate, $endDate){

    $this->db->select('*');
    $this->db->from('booking');
    $this->db->where('booking.status',$stats);
    $this->db->where('booking.function_date >= ',$strDate);
    $this->db->where('booking.function_date <= ',$endDate);
    return $this->db->get()->result();
   }
   
   function getEmployees($active=0){
      
      $this->db->select('*');
      $this->db->from('employees');
      if($active == 1){
         $this->db->where('active',$active);
      }
      return $this->db->get()->result();
 }

   function getAllSaleryTransaction($id, $stDate, $endDate){

      $this->db->select('date, detail, total_amount');
      $this->db->from('employee_account');
      $this->db->where('employee_id', $id);
      $this->db->where('date >=', $stDate);
      $this->db->where('date <=', $endDate);
      return $this->db->get()->result();
   }

   public function getBankTransactions($id, $st, $en){

      $this->db->select('ba.*');
      $this->db->from('bank_account ba');
      $this->db->join('bank', 'bank.id = ba.bank_id');
      $this->db->where('bank.id', $id);
      $this->db->where('ba.date >=', $st);
      $this->db->where('ba.date <=', $en);
      return $this->db->get()->result();
      
   }
   public function getBankTransactionsNotById($st, $en){

      $this->db->select('ba.*');
      $this->db->from('bank_account ba');
      $this->db->join('bank', 'bank.id = ba.bank_id');
      $this->db->where('ba.date >=', $st);
      $this->db->where('ba.date <=', $en);
      return $this->db->get()->result();
      
   }

   public function getTotalColumnSum($vendorId, $column){
      $this->db->select_sum($column);
      $this->db->from('bank_account');
      $this->db->where('bank_id', $vendorId);
      return $this->db->get()->row();
  }

  public function getBookingTransaction($startDate, $endDate){
      $this->db->select('bt.*, b.customer_name, b.serial_no');
      $this->db->from('booking_transactions bt');
      $this->db->join('booking b','bt.booking_id = b.id');
      $this->db->where('bt.date >= ',$startDate);
      $this->db->where('bt.date <= ',$endDate);
      return $this->db->get()->result();
  }


}
?>