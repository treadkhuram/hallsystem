<?php

class RawMaterial_model extends CI_Model{

    public function retriveTable($table, $active = 0){
        $this->db->select($table.'.*, unit.measure_unit as measure_unit');
        $this->db->from($table);
        $this->db->join('unit','unit.id = '.$table.'.unit');
        if($active == 1){
          $this->db->where('raw_material.active', 1);
        }
        return $this->db->get()->result();
      }

      public function addRawMaterial($id, $qty, $price){
        $this->db->set('qty','qty+'.$qty, false)
            ->set('purchase_price', $price)
            ->where('id', $id)
            ->update('raw_material');
      }

      public function subtractRawMaterial($id, $qty){
          $this->db->set('qty','qty-'.$qty, false)
              ->where('id', $id)
              ->update('raw_material');
      }

}
?>