<?php

class Db_model extends CI_Model{

    public function retriveTable($table, $active = 0){
      $this->db->select('*');
      $this->db->from($table);
      if($active == 1){
        $this->db->where('active', 1);
      }
      return $this->db->get()->result();
    }

    public function retriveById($table, $id){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where('id', $id);
        return $this->db->get()->row();
    }


    public function retriveTableByFKey($table, $columnName, $val){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($columnName, $val);
        return $this->db->get()->result();
    }
  
  public function retriveRowByFKey($table, $columnName, $val){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($columnName, $val);
        return $this->db->get()->row();
    }

    public function create($tableName, $data){
        return $this->db->insert($tableName, $data);
    }

    public function createGetId($tableName, $data){
        $this->db->insert($tableName, $data);
        return $this->db->insert_id();
    }

  
  public function delete($table, $id){
    $this->db->where("id",$id);
    $this->db->delete($table); 
  }

    public function deleteByFk($table, $column, $id){
        $this->db->where($column,$id);
        $this->db->delete($table);
    }
  
  public function remove($table, $data){
    $this->db->where($data);
    $this->db->delete($table); 
  }
  
  public function update($table, $id, $data){
    $this->db->where('id', $id);
    return $this->db->update($table, $data);
  }

    public function updateByFk($table, $column, $id, $data){
        $this->db->where($column, $id);
        return $this->db->update($table, $data);
    }


    public function checkValueExist($table, $data){
        $q = $this->db->where($data)->count_all_results($table);
        return $q;
    }
  
  public function tableRowsCount($table){
        $q = $this->db->count_all_results($table);
        return $q;
    }
  
  public function tableRowsCountByKey($table,$columnName, $val){
        $this->db->select('*')->from($table)->where($columnName, $val);
        $q = $this->db->count_all_results();
        return $q;
    }
 


}





?>