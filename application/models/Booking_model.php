<?php

class Booking_model extends CI_Model{

    public function getBookingOrder($id, $active = 0){
        $this->db->select('booking.*');
        $this->db->from('booking');
        if($active == 1){
            $this->db->where('booking.active', 1);
        }
        $this->db->where('booking.id',$id);
        return $this->db->get()->row();
    }
  
    public function getMenuDishes($id){
        $this->db->select('booking_menu.*, dishes.name as dish_name');
        $this->db->from('booking_menu');
        $this->db->join('dishes','dishes.id = booking_menu.dish_id');
        $this->db->where('booking_menu.booking_id',$id);
        return $this->db->get()->result();
    }
    
    public function removeMenuDishes($id){
        return $this->db->where('booking_id',$id)
                    ->delete('booking_menu');
    }

    public function retrieveBookings($status, $startDate='', $endDate=''){
        $this->db->select('*');
        $this->db->from('booking');
        $this->db->where('status', $status);
        $this->db->where('active', 1);
        if($startDate != '' && $endDate != ''){
            $this->db->where('function_date >=', $startDate);
            $this->db->where('function_date <=', $endDate);
        }
        return $this->db->get()->result();
    }

    public function getTotalReceivedAmount($bookingId){
        $this->db->select('sum(amount) as amount');
        $this->db->from('booking_transactions');
        $this->db->where('booking_id', $bookingId);
        $res =  $this->db->get();
        if($res->num_rows()>0){
            return $res->row()->amount;
        }else 
            return 0;
    }

    public function getMenuItemsOnId($id){
        $this->db->select('a.*, b.name as dish_name, b.no_of_persons_serving');
        $this->db->from('booking_menu a');
        $this->db->join('dishes b','b.id = a.dish_id');
        $this->db->where('a.booking_id', $id);
        return $this->db->get()->result();
    }

    public function retriveDishesById($id){
        $this->db->select('bm.*, d.name as dish_name');
        $this->db->from('booking_menu bm');
        $this->db->join('dishes d', 'd.id = bm.dish_id');
        $this->db->where('bm.booking_id', $id);
        return $this->db->get()->result();
    }
    




    
}
?>