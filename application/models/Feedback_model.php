<?php

class Feedback_model extends CI_Model{

    public function getAllFeedback($approveStatus){
        $this->db->select('feedback.*, drivers.full_name driver_name, customers.full_name customer_name, items_type.type as order_type')
                ->from('feedback')
                ->join('drivers', 'feedback.driver_id = drivers.id')
                ->join('customers', 'feedback.customer_id = customers.id')
                ->join('orders', 'feedback.order_id = orders.id')
                ->join('items_type', 'orders.items_type_id = items_type.id')
                ->where('feedback.approval_status',$approveStatus);
        return $this->db->get()->result();
    }


}