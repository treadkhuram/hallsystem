<?php

class Driver_model extends CI_Model{

    public function getAllDrivers($status, $active = ""){
        $this->db->select('drivers.*, vehicles.name as vehicle_name, vehicles.number_plate ')
                ->from('drivers')
                ->join('vehicles', 'drivers.vehicle_id = vehicles.id')
                ->where('drivers.account_status',$status);
        if($active != ""){
            $this->db->where('active', $active);
        }
        return $this->db->get()->result();
    }

    public function driversDetail($id){
        $q = $this->db->select('drivers.*, vehicles.name as vehicle_name, vehicles.number_plate ')
                ->from('drivers')
                ->join('vehicles', 'drivers.vehicle_id = vehicles.id')
                ->where('drivers.id',$id)
                ->get();
        return $q->row();
    }

    public function getDriverVehicle($driverId){
        return $this->db->select('vehicles.*, vehicles_assigned_history.assigned_start_time')
        ->from('vehicles')
        ->join('vehicles_assigned_history', 'vehicles.id = vehicles_assigned_history.vehicle_id')
        ->where('vehicles_assigned_history.driver_id', $driverId)
        ->group_by(array("vehicles_assigned_history.vehicle_id", "vehicles_assigned_history.driver_id"))
        ->get()->result();
    }
    public function getDriverTrips($driverId, $vehicleId){
        return $this->db->select('vehicles.*, vehicles_assigned_history.assigned_start_time')
        ->from('vehicles')
        ->join('vehicles_assigned_history', 'vehicles.id = vehicles_assigned_history.vehicle_id')
        ->where('vehicles_assigned_history.driver_id', $driverId)
        ->where('vehicles_assigned_history.vehicle_id', $vehicleId)
        ->get()->result();
    }

    public function getDriverAssignHistory($driverId){
        return $this->db->select('orders.*, items_type.type')
        ->from('orders')
        ->join('order_assigned_history', 'orders.id = order_assigned_history.order_id')
        ->join('items_type', 'orders.items_type_id = items_type.id')
        ->where('order_assigned_history.driver_id', $driverId)
        ->get()->result();
    }




}





?>