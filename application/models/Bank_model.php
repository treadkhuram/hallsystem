<?php
/**
 * Created by PhpStorm.
 * User: Omen
 * Date: 3/9/2021
 * Time: 1:31 PM
 */

class Bank_model extends CI_Model{
    public function getBankAccountByCashId($bankId, $cashId){
        $this->db->select('*');
        $this->db->from('bank_account');
        $this->db->where('bank_id', $bankId);
        $this->db->where('cash_id', $cashId);
        return $this->db->get()->row();
    }

    public function getTotalColumnSum($bankId, $column, $endDate=''){
        $this->db->select_sum($column);
        $this->db->from('bank_account');
        $this->db->where('bank_id', $bankId);
        if($endDate !=''){
            $this->db->where('date <', $endDate);
        }
        return $this->db->get()->row();
    }

//    public function updateBankAccount($bank)
}