<?php
class Auth_model extends CI_Model{

    public function loginUser($username, $password){
        $q = $this->db->where(array('email' =>$username, 'password' =>$password))->get('users');
        if ($q->num_rows() == 1){
            return $q->row();
        }
        else{
            return FALSE;
        }
    }

    public function checkTokenExists($userId, $userType, $token){
        $q = $this->db->where(array('user_id' =>$userId, 'user_type' =>$userType, 'token' =>$token))->get('device_tokens');
        if ($q->num_rows() == 1){
            return $q->row();
        }
        else{
            return FALSE;
        }
    }
}