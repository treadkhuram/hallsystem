<?php

class Vandor_model extends CI_Model{

    public function getSupplyData($start, $end, $vendor){

        $this->db->select('v.*');
        $this->db->from('vendors_supply v');
        $this->db->where('v.vendor_id',$vendor);
        $this->db->where('v.date >=',$start);
        $this->db->where('v.date <=',$end);
        $this->db->order_by('id', 'asc');
        return $this->db->get()->result();
    }



    public function getSupplyDataEmployee($start, $end, $employee_id){

        $q = $this->db->order_by('employee_account.id','desc')
                    ->select('employee_account.*, employees.name as name , employees.salary as salary')
                    ->join('employees', 'employee_account.employee_id = employees.id')
                    ->where('employee_id' , $employee_id)
                    ->where('date>=' , $start)
                    ->where('date<=' , $end)
                    ->get('employee_account');
        return $q->result();

    }

    public function getTotalColumnSum($vendorId, $column, $endDate=''){
        $this->db->select_sum($column);
        $this->db->from('vendors_supply');
        $this->db->where('vendor_id', $vendorId);
        if($endDate !=''){
            $this->db->where('date <', $endDate);
        }
        return $this->db->get()->row();
    }


}
?>